package com.example.darewreck.timelinesample;

import org.parceler.Parcel;

/**
 * Created by darewreck_PC on 4/15/2017.
 */
@Parcel(analyze={TravelLog.class})
public class TravelLog {
    public String title;
}
