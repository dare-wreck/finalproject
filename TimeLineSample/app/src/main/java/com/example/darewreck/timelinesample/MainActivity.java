package com.example.darewreck.timelinesample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements TimeLineFragment.TimelineFragmentListener, MapTimeLineFragment.MapTimeLineFragmentListener, ProfileFragment.ProfileFragmentListener {
    private MapTimeLineFragment mMapTimeLineFragment;
    private List<TravelLogEntry> mTravelLogEntry;
    private ProfileFragment profileFragment;
    private static final int PICK_IMAGE_ID = 234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //mMapTimeLineFragment =  MapTimeLineFragment.newInstance(mTravelLogEntry);

        User user = new User();

        profileFragment = ProfileFragment.newInstance(user);
        ft.replace(R.id.flContainer, profileFragment);
        ft.commit();
    }
    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */
    @Override
    public void onTimelineDetailClicked(TravelLogEntry entry) {
        Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        TravelLogEntryDetailFragment timelineDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
        ft.replace(R.id.flContainer, timelineDetailFragment);

        // ADD THIS LINE
        ft.addToBackStack("mapTimeLineFragment"); // name can be null
        ft.commit();

    }

    @Override
    public void onStartAdventureClick(TravelLog entry) {
        Intent intent = new Intent(this, StartTravelActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTravelLogClicked(TravelLog entry) {
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        MapTimeLineFragment mapTimelineFragment = MapTimeLineFragment.newInstance(entry);
        ft.replace(R.id.flContainer, mapTimelineFragment);

        // ADD THIS LINE
        ft.addToBackStack("profileFragment"); // name can be null
        ft.commit();
    }

    @Override
    public void onEditTravelLogProfilePic(User user) {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                profileFragment.updateProfilePic(bitmap);
                // TODO use bitmap
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
