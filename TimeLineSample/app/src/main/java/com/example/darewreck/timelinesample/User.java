package com.example.darewreck.timelinesample;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by darewreck_PC on 4/15/2017.
 */
@Parcel(analyze={User.class})
public class User {
    public List<TravelLog> travelLogs;

    public User(){
        travelLogs = new ArrayList<>();
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
        travelLogs.add(new TravelLog());
    }
}
