package com.example.darewreck.timelinesample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileFragment extends Fragment {
    private User mUser;
    private Context mContext;
    private LayoutInflater inflater;
    private static final String TAG = ProfileFragment.class.getName();
    private ViewHolder viewHolder;
    private GridLayoutManager gridLayoutManager;
    @BindView(R.id.rvTravelLogs)
    protected RecyclerView rvTravelLogs;
    protected TravelLogRecycleAdapter mAdapter;
    private static final int PICK_IMAGE_ID = 234; // the number doesn't matter
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvUserName;

        @BindView(R.id.ivProfilePic)
        ImageView ivProfilePic;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private ProfileFragment.ProfileFragmentListener mListener;

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileFragment.ProfileFragmentListener) {
            mListener = (ProfileFragment.ProfileFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ProfileFragment.ProfileFragmentListener");
        }
    }

    public interface ProfileFragmentListener {
        void onTravelLogClicked(TravelLog entry);
        void onEditTravelLogProfilePic(User user);
    }

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(User user) {

        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();

        Parcelable listParcelable = Parcels.wrap(user);
        args.putParcelable("user", listParcelable);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mUser = Parcels.unwrap(getArguments().getParcelable("user"));
            mContext = getContext();


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        mAdapter = new TravelLogRecycleAdapter(getContext(), mUser.travelLogs);
        rvTravelLogs.setAdapter(mAdapter);

        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rvTravelLogs.setLayoutManager(gridLayoutManager);
        rvTravelLogs.setHasFixedSize(true);
        ItemClickSupport.addTo(rvTravelLogs).setOnItemClickListener(
            new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    TravelLog log = mUser.travelLogs.get(position);
                    mListener.onTravelLogClicked(log);
                }
            }
        );

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onEditTravelLogProfilePic(mUser);
            }
        });
        return view;
    }

    public void updateProfilePic(Bitmap image) {
        Toast.makeText(getContext(), "updatePicTriggered", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.profile_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home: {
                getFragmentManager().popBackStack();
                return true;
            }
            case R.id.action_edit: {

                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}

