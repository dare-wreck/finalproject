package com.example.darewreck.timelinesample;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapTimeLineFragment extends Fragment {
    private TimeLineFragment timeLineFragment;
    private MapFragment mapFragment;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MapTimeLineFragment.MapTimeLineFragmentListener mListener;

    public MapTimeLineFragment() {
    }

    public static MapTimeLineFragment newInstance(TravelLog mTravelLogEntry) {
        MapTimeLineFragment fragment = new MapTimeLineFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MapTimeLineFragment.MapTimeLineFragmentListener) {
            mListener = (MapTimeLineFragment.MapTimeLineFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }

    public interface MapTimeLineFragmentListener {
        void onStartAdventureClick(TravelLog entry);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_time_line, container, false);
        ButterKnife.bind(this, view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();

        mapFragment = MapFragment.newInstance(null);
        ft.replace(R.id.flContainer1, mapFragment);

        timeLineFragment = TimeLineFragment.newInstance(null);
        ft.replace(R.id.flContainer2, timeLineFragment);

        ft.commit();
    }

    public void onMarkerClick(int markerIndex) {
        timeLineFragment.scrollToPosition(markerIndex);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.travellog_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home: {
                getFragmentManager().popBackStack();
                return true;
            }
            case R.id.action_start_adventrue: {
                mListener.onStartAdventureClick(null);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
