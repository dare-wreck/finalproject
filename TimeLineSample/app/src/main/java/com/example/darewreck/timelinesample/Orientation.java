package com.example.darewreck.timelinesample;

/**
 * Created by darewreck on 4/9/17.
 */

public enum Orientation {
    VERTICAL,
    HORIZONTAL
}
