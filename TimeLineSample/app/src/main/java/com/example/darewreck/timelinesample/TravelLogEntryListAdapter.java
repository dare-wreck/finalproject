package com.example.darewreck.timelinesample;

        import android.content.Context;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import java.util.ArrayList;
        import java.util.List;

        import butterknife.BindView;
        import butterknife.ButterKnife;

/**
 * Created by darewreck_PC on 4/14/2017.
 */

public class TravelLogEntryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder > {

    private final List<TravelLogEntry> mList = new ArrayList<>();
    private ViewHolder viewHolder;
    private Context mContext;
    private LayoutInflater inflater;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public TravelLogEntryListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_view, parent, false);
        RecyclerView.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, int position) {
        TravelLogEntry entry = mList.get(position);
        viewHolder = (ViewHolder) holder;
        viewHolder.tvTitle.setText(entry.title.toString());
    }

    @Override public int getItemCount() {
        return mList.size();
    }

    public boolean add(TravelLogEntry object) {
        int lastIndex = mList.size();
        if (mList.add(object)) {
            notifyItemInserted(lastIndex);
            return true;
        } else {
            return false;
        }
    }
}