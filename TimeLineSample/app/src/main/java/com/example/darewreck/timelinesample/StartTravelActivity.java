package com.example.darewreck.timelinesample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

public class StartTravelActivity extends AppCompatActivity {
    private StartTravelActivityFragment startTravelActivityFragment;
    private static String TAG = StartTravelActivity.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_travel);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        startTravelActivityFragment =  StartTravelActivityFragment.newInstance(null);
        ft.replace(R.id.flContainer, startTravelActivityFragment);

        ft.addToBackStack(TAG); // name can be null
        ft.commit();
    }

}
