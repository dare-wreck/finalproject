package com.example.darewreck.timelinesample;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class TravelLogEntryDetailFragment extends Fragment {
    private final String TAG = TravelLogEntryDetailFragment.class.getName();
    private static final String ARG_TRAVEL_LOG_ENTRY= "travelLogEntryParam";
    private TimeLineModel mTimeLineModel;

    @BindView(R.id.tvTitle)
    public TextView title;

    @BindView(R.id.tvDetails)
    public TextView details;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.indicator)
    CircleIndicator indicator;

    public TravelLogEntryDetailFragment() {
        // Required empty public constructor
    }

    public static TravelLogEntryDetailFragment newInstance(TravelLogEntry travelLogEntry) {
        TravelLogEntryDetailFragment fragment = new TravelLogEntryDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TRAVEL_LOG_ENTRY, Parcels.wrap(travelLogEntry));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTimeLineModel = (TimeLineModel) Parcels.unwrap(getArguments().getParcelable(ARG_TRAVEL_LOG_ENTRY));
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_travel_log_entry_detail, container, false);
        ButterKnife.bind(this,view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);



        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TravelLogEntryListAdapter adapter = new TravelLogEntryListAdapter(getContext());

        TravelImagePagerAdapter pageAdapter = new TravelImagePagerAdapter();
        CustomPagerAdapter customPageAdapter = new CustomPagerAdapter(getContext());

        viewpager.setAdapter(customPageAdapter);
        indicator.setViewPager(viewpager);

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
