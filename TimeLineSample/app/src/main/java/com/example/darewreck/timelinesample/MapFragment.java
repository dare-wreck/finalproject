package com.example.darewreck.timelinesample;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapFragment extends Fragment implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    private GoogleMapOptions options;
    private SupportMapFragment mapFragment;
    private int containerWidth;
    private int containerHeight;

    @BindView(R.id.flContainer)
    FrameLayout flContainer;

    public MapFragment() {
    }

    public static MapFragment newInstance(List<TravelLogEntry> mTravelLogEntry) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (TextUtils.isEmpty(getResources().getString(R.string.google_maps_api_key))) {
            throw new IllegalStateException("You forgot to supply a Google Maps API key");
        }
        if (getArguments() != null) {

        }

        options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL);
        options.compassEnabled(false);

        mapFragment = SupportMapFragment.newInstance(options);
        mapFragment.getMapAsync(this);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            Toast.makeText(getContext(), "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.flContainer, mapFragment);
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);

        this.containerHeight= view.getMeasuredHeight();
        this.containerWidth = view.getMeasuredWidth();

        return view;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        // Retrieve the data from the marker.
        Integer markerIndex = (Integer) marker.getTag();
        if (markerIndex != null) {
            MapTimeLineFragment mapTimeLineFragment = (MapTimeLineFragment)getParentFragment();
            mapTimeLineFragment.onMarkerClick(markerIndex);
            //mListener.onMarkerClick(markerIndex);
        }

        return true;
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        List<MarkerOptions> markers = new ArrayList<MarkerOptions>();
        MarkerOptions marker1 = new MarkerOptions()
                .position(new LatLng(47.620506, -122.349277))
                .title("Space Needle");
        markers.add(marker1);

        MarkerOptions marker2 = new MarkerOptions()
                .position(new LatLng(47.610136, -122.342057))
                .title("Pikes Market");
        markers.add(marker2);

        MarkerOptions marker3 = new MarkerOptions()
                .position(new LatLng(47.629487, -122.359912))
                .title("Kerry Park");
        markers.add(marker3);

        map.addMarker(marker1).setTag(0);
        map.addMarker(marker2).setTag(1);
        map.addMarker(marker3).setTag(2);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (MarkerOptions marker : markers) {
            builder.include(marker.getPosition());
        }
        final  LatLngBounds bounds = builder.build();


        Polyline polyline = map.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(47.620506, -122.349277), new LatLng(47.610136, -122.342057), new LatLng(47.629487, -122.359912))                );

        final int height = this.containerHeight;
        final int width = this.containerWidth;

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                final int width = flContainer.getMeasuredWidth();
                final int height = flContainer.getMeasuredHeight();
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width,height, 100));
                map.getUiSettings().setZoomControlsEnabled(false);
                map.getUiSettings().setZoomGesturesEnabled(false);
                map.getUiSettings().setScrollGesturesEnabled(false);
                map.getUiSettings().setAllGesturesEnabled(false);
            }
        });

        map.setOnMarkerClickListener(this);
    }
}
