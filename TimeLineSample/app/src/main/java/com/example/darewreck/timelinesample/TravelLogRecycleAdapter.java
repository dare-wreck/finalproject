package com.example.darewreck.timelinesample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by darewreck_PC on 4/15/2017.
 */

public class TravelLogRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private final List<TravelLog> mList;
    private TravelLogEntryListAdapter.ViewHolder viewHolder;
    private Context mContext;
    private LayoutInflater inflater;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView tvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public TravelLogRecycleAdapter(Context context, List<TravelLog> travelLogs) {
        mList = travelLogs;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_view, parent, false);
        RecyclerView.ViewHolder viewHolder = new TravelLogEntryListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, int position) {
        TravelLog entry = mList.get(position);
        viewHolder = (TravelLogEntryListAdapter.ViewHolder) holder;
//        viewHolder.tvTitle.setText(entry.title.toString());
    }

    @Override public int getItemCount() {
        return mList.size();
    }

    public boolean add(TravelLog object) {
        int lastIndex = mList.size();
        if (mList.add(object)) {
            notifyItemInserted(lastIndex);
            return true;
        } else {
            return false;
        }
    }
}
