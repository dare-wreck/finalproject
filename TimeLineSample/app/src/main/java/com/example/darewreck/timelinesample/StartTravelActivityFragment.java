package com.example.darewreck.timelinesample;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StartTravelActivityFragment extends Fragment {
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    private static String TAG = StartTravelActivityFragment.class.getName();
    private Menu menu;
    private MenuItem menuBackButton;
    private MenuItem menuNextButton;

    public StartTravelActivityFragment() {
        // Required empty public constructor
    }

    public static StartTravelActivityFragment newInstance(TravelLog travelLog) {
        StartTravelActivityFragment fragment = new StartTravelActivityFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_travel_activity, container, false);
        ButterKnife.bind(this, view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();

        StartTravelLogEntryDetailFragment travelLogEntryDetailFragment = StartTravelLogEntryDetailFragment.newInstance(null);
        ft.replace(R.id.flContainer, travelLogEntryDetailFragment);

        ft.addToBackStack(TAG); // name can be null
        ft.commit();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.start_travel_menu, menu);
        this.menu = menu;
        this.menuBackButton = menu.getItem(0);
        this.menuNextButton = menu.getItem(1);
        menuBackButton.setVisible(false);

        if(getChildFragmentManager().getBackStackEntryCount() > 1)
        {
            this.menuBackButton.setVisible(true);
        } else {
            this.menuBackButton.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home: {
                getActivity().finish();
                return true;
            }
            case R.id.action_back: {
                getChildFragmentManager().popBackStack();

                return true;
            }
            case R.id.action_next: {
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();

                StartTravelLogEntryDetailFragment startTravelLogEntryDetailFragment = StartTravelLogEntryDetailFragment.newInstance(null);
                ft.replace(R.id.flContainer, startTravelLogEntryDetailFragment);

                ft.addToBackStack(TAG + getChildFragmentManager().getBackStackEntryCount()); // name can be null
                ft.commit();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
