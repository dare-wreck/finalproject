package com.codepath.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by darewreck_PC on 4/20/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements DraggableItemAdapter<MyAdapter.MyViewHolder> {
    private static final String TAG = MyAdapter.class.getName();
    protected List<MyItem> mItems;
    private LayoutInflater inflater;
    public MyAdapter() {
        setHasStableIds(true);
        mItems = new ArrayList<>();
        mItems.add(new MyItem("1"));
        mItems.add(new MyItem("2"));
        mItems.add(new MyItem("3"));
        mItems.add(new MyItem("4"));
        mItems.add(new MyItem("5"));
        mItems.add(new MyItem("6"));

    }

    @Override
    public boolean onCheckCanStartDrag(MyViewHolder holder, int position, int x, int y) {
        View itemView = holder.itemView;
        View dragHandle = holder.dragHandle;

        int handleWidth = dragHandle.getWidth();
        int handleHeight = dragHandle.getHeight();
        int handleLeft = dragHandle.getLeft();
        int handleTop = dragHandle.getTop();

        return (x >= handleLeft) && (x < handleLeft + handleWidth) &&
                (x >= handleTop) && (x < handleTop + handleHeight);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(MyViewHolder holder, int position) {
        return null;
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        Log.d(TAG, "onMoveItem(fromPosition = " + fromPosition + ", toPosition = " + toPosition + ")");

        MyItem removed = mItems.remove(fromPosition);
        mItems.add(toPosition, removed);

        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
        return true;
    }

    static class MyViewHolder extends AbstractDraggableItemViewHolder {
        TextView textView;
        View dragHandle;

        MyViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(android.R.id.text1);
            dragHandle = v.findViewById(R.id.drag_handle);
        }
    }
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_view, parent, false);
        MyAdapter.MyViewHolder viewHolder = new MyAdapter.MyViewHolder(view);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {
            MyItem myItem = mItems.get(position);
            holder.textView.setText(myItem.text);

    }

    @Override
    public long getItemId(int position) {
        // requires static value, it means need to keep the same value
        // even if the item position has been changed.
        return mItems.get(position).id;
    }

    @Override
    public int getItemCount() {
        // requires static value, it means need to keep the same value
        // even if the item position has been changed.
        return mItems.size();
    }

}
