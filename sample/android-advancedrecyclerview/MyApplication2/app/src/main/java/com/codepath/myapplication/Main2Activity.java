package com.codepath.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;

import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        RecyclerViewDragDropManager dragDropManager = new RecyclerViewDragDropManager();

        MyAdapter adapter = new MyAdapter();
        RecyclerView.Adapter wrappedAdapter = dragDropManager.createWrappedAdapter(adapter);

        mRecyclerView.setAdapter(wrappedAdapter);
        LinearLayoutManager layoutMgr = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutMgr);

        // disable change animations
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), layoutMgr.getOrientation());
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        // [OPTIONAL]
        // dragDropManager.setInitiateOnTouch(true);
        // dragDropManager.setInitiateOnLongPress(true);
        // dragDropManager.setInitiateOnMove(true);

        dragDropManager.attachRecyclerView(mRecyclerView);
    }
}
