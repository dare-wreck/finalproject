package com.codepath.myapplication;

/**
 * Created by darewreck_PC on 4/20/2017.
 */

public class MyItem {
    public int id;
    public String text;
    public MyItem() {

    }
    public MyItem(String text) {
        this.text = text;
    }
}
