package com.raywenderlich.android.arewethereyet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.software.shell.fab.ActionButton;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class AllGeofencesFragment extends Fragment implements AddGeofenceFragment.AddGeofenceFragmentListener {
    private ViewHolder viewHolder;
    private ViewHolder getViewHolder() {
        return viewHolder;
    }
    private AllGeofencesAdapter allGeofencesAdapter;
    private AllGeofencesFragment.AllGeofencesFragmentListener listener;
    private List<NamedGeofence> namedGeofences;

    public interface AllGeofencesFragmentListener {
        void onAddGeoFence( NamedGeofence geofence);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AllGeofencesFragmentListener) {
            listener = (AllGeofencesFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static AllGeofencesFragment newInstance() {
        AllGeofencesFragment fragment = new AllGeofencesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        namedGeofences = new ArrayList<>();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_geofences, container, false);
        viewHolder = new ViewHolder();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getViewHolder().populate(view);

        viewHolder.geofenceRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        viewHolder.geofenceRecyclerView.setLayoutManager(layoutManager);

        allGeofencesAdapter = new AllGeofencesAdapter(this.namedGeofences);
        viewHolder.geofenceRecyclerView.setAdapter(allGeofencesAdapter);
        /*
        allGeofencesAdapter.setListener(new AllGeofencesAdapter.AllGeofencesAdapterListener() {
            @Override
            public void onDeleteTapped(NamedGeofence namedGeofence) {
                List<NamedGeofence> namedGeofences = new ArrayList<>();
                namedGeofences.add(namedGeofence);
                GeofenceController.getInstance().removeGeofences(namedGeofences, geofenceControllerListener);
            }
        });
        */
        viewHolder.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddGeofenceFragment dialogFragment = new AddGeofenceFragment();
                dialogFragment.setListener(AllGeofencesFragment.this);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "AddGeofenceFragment");
            }
        });

        refresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        /*
        if (id == R.id.action_fake) {
            Intent startIntent = new Intent();
            startIntent.setAction("com.lexa.fakegps.START");
            startIntent.setPackage("com.lexa.fakegps");
            startIntent.putExtra("lat", 47.6101);
            startIntent.putExtra("long", -122.342057);
            getActivity().startService(startIntent);

            Intent stopIntent = new Intent();
            stopIntent.setAction("com.lexa.fakegps.STOP");
            stopIntent.setPackage("com.lexa.fakegps");

            getActivity().startService(stopIntent);
            return true;
        }
    */

        return super.onOptionsItemSelected(item);
    }

    /*

    private GeofenceController.GeofenceControllerListener geofenceControllerListener = new GeofenceController.GeofenceControllerListener() {
        @Override
        public void onGeofencesUpdated() {
            refresh();
        }

        @Override
        public void onError() {
            showErrorToast();
        }
    };

     private void showErrorToast() {
        Toast.makeText(getActivity(), getActivity().getString(R.string.Toast_Error), Toast.LENGTH_SHORT).show();
    }
    */

    private void refresh() {
        allGeofencesAdapter.notifyDataSetChanged();

        if (allGeofencesAdapter.getItemCount() > 0) {
            getViewHolder().emptyState.setVisibility(View.INVISIBLE);
        } else {
            getViewHolder().emptyState.setVisibility(View.VISIBLE);

        }

        getActivity().invalidateOptionsMenu();

    }


    @Override
    public void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog, NamedGeofence geofence) {
        //GeofenceController.getInstance().addGeofence(geofence, geofenceControllerListener);
        this.listener.onAddGeoFence(geofence);
    }

    @Override
    public void onDialogNegativeClick(android.support.v4.app.DialogFragment dialog) {
        // Do nothing
    }


    static class ViewHolder {
        ViewGroup container;
        ViewGroup emptyState;
        RecyclerView geofenceRecyclerView;
        ActionButton actionButton;

        public void populate(View v) {
            container = (ViewGroup) v.findViewById(R.id.fragment_all_geofences_container);
            emptyState = (ViewGroup) v.findViewById(R.id.fragment_all_geofences_emptyState);
            geofenceRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_all_geofences_geofenceRecyclerView);
            actionButton = (ActionButton) v.findViewById(R.id.fragment_all_geofences_actionButton);

            actionButton.setImageResource(R.drawable.fab_plus_icon);
        }
    }

    public List<NamedGeofence> getNamedGeofences() {
        return this.namedGeofences;
    }

    public void addNamedGeofences(NamedGeofence namedGeofence) {
        allGeofencesAdapter.add(namedGeofence);
    }
}
