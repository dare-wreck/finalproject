package com.raywenderlich.android.arewethereyet;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AreWeThereIntentService extends IntentService {

  private final String TAG = AreWeThereIntentService.class.getName();
 // private SharedPreferences prefs;
//  private Gson gson;

  public AreWeThereIntentService() {
    super("AreWeThereIntentService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    //prefs = getApplicationContext().getSharedPreferences(Constants.SharedPrefs.Geofences, Context.MODE_PRIVATE);
   // gson = new Gson();

    GeofencingEvent event = GeofencingEvent.fromIntent(intent);
      
    if (event != null) {
      if (event.hasError()) {
        onError(event.getErrorCode());
      } else {
        int transition = event.getGeofenceTransition();
        if (transition == Geofence.GEOFENCE_TRANSITION_ENTER || transition == Geofence.GEOFENCE_TRANSITION_DWELL || transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
          List<String> geofenceIds = new ArrayList<>();
          for (Geofence geofence : event.getTriggeringGeofences()) {
            geofenceIds.add(geofence.getRequestId());
          }
          if (transition == Geofence.GEOFENCE_TRANSITION_ENTER || transition == Geofence.GEOFENCE_TRANSITION_DWELL) {
            int geoFenceTransition = event.getGeofenceTransition();
            List<Geofence> triggeringGeofences = event.getTriggeringGeofences();
            // Create a detail message with Geofences received
            String geofenceTransitionDetails = getGeofenceTrasitionDetails(geoFenceTransition, triggeringGeofences );
            // Send notification details as a String
            sendNotification( geofenceTransitionDetails );
          }
        }
      }
    }
  }

  // Create a detail message with Geofences received
  private String getGeofenceTrasitionDetails(int geoFenceTransition, List<Geofence> triggeringGeofences) {
    // get the ID of each geofence triggered
    ArrayList<String> triggeringGeofencesList = new ArrayList<>();
    for ( Geofence geofence : triggeringGeofences ) {
      triggeringGeofencesList.add( geofence.getRequestId() );
    }

    String status = null;
    if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER )
      status = "Entering ";
    else if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT )
      status = "Exiting ";
    return status + TextUtils.join( ", ", triggeringGeofencesList);
  }

  public static final int GEOFENCE_NOTIFICATION_ID = 0;

  private void sendNotification(String msg) {
    Log.i(TAG, "sendNotification: " + msg );

    // Intent to start the main Activity
    Intent notificationIntent = AllGeofencesActivity.makeNotificationIntent(
            getApplicationContext(), msg
    );

    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
    stackBuilder.addParentStack(AllGeofencesActivity.class);
    stackBuilder.addNextIntent(notificationIntent);
    PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

    // Creating and sending Notification
    NotificationManager notificatioMng =
            (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
    notificatioMng.notify(
            GEOFENCE_NOTIFICATION_ID,
            createNotification(msg, notificationPendingIntent));
  }

  // Create a notification
  private Notification createNotification(String msg, PendingIntent notificationPendingIntent) {
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
    notificationBuilder
            .setSmallIcon(R.mipmap.ic_launcher)
            .setColor(Color.RED)
            .setContentTitle(msg)
            .setContentText("Geofence Notification!")
            .setContentIntent(notificationPendingIntent)
            .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
            .setAutoCancel(true);
    return notificationBuilder.build();
  }

  /*
  private void onEnteredGeofences(List<String> geofenceIds) {
    for (String geofenceId : geofenceIds) {
      String geofenceName = "";

      // Loop over all geofence keys in prefs and retrieve NamedGeofence from SharedPreference
      Map<String, ?> keys = prefs.getAll();
      for (Map.Entry<String, ?> entry : keys.entrySet()) {
        String jsonString = prefs.getString(entry.getKey(), null);
        NamedGeofence namedGeofence = gson.fromJson(jsonString, NamedGeofence.class);
        if (namedGeofence.id.equals(geofenceId)) {
          geofenceName = namedGeofence.name;
          break;
        }
      }

      // Set the notification text and send the notification
      String contextText = String.format(this.getResources().getString(R.string.Notification_Text), geofenceName);

      NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
      Intent intent = new Intent(this, AllGeofencesActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

      Notification notification = new NotificationCompat.Builder(this)
              .setSmallIcon(R.mipmap.ic_launcher)
              .setContentTitle(this.getResources().getString(R.string.Notification_Title))
              .setContentText(contextText)
              .setContentIntent(pendingNotificationIntent)
              .setStyle(new NotificationCompat.BigTextStyle().bigText(contextText))
              .setPriority(NotificationCompat.PRIORITY_HIGH)
              .setDefaults(Notification.DEFAULT_SOUND)
              .setAutoCancel(true)
              .build();
      notificationManager.notify(0, notification);

    }
  }
  */
  private void onError(int i) {
    Log.e(TAG, "Geofencing Error: " + i);
  }

}

