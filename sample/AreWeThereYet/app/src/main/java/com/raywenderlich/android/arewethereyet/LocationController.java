package com.raywenderlich.android.arewethereyet;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by darewreck_PC on 5/2/2017.
 */

public class LocationController {
    private final String TAG = LocationController.class.getSimpleName();
    private Context context;
    private GoogleApiClient googleApiClient;

    private static LocationController INSTANCE;

    private Location lastLocation;

    private LocationRequest locationRequest;
    // Defined in mili seconds.
    // This number in extremely low, and should be used only for debug
    private final int UPDATE_INTERVAL =  1000;
    private final int FASTEST_INTERVAL = 900;

    public static LocationController getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocationController();
        }
        return INSTANCE;
    }

    public void init(Context context, GoogleApiClient googleApiClient) {
        this.context = context.getApplicationContext();
        this.googleApiClient = googleApiClient;;
    }

}

