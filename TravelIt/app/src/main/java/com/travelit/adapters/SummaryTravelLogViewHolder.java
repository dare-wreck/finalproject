package com.travelit.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelit.R;
import com.travelit.activities.TravelEntriesActivity;
import com.travelit.models.TravelLog;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.AnimationHelper;
import com.travelit.util.LogUtil;
import com.travelit.views.DynamicHeightImageView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * View holder class for Travel log summary view.
 * @author Sai Muppa
 */
class SummaryTravelLogViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
    private static final int REQUEST_CODE = 200;
    private static final String LOG_TAG = "TravelLogViewHolder";
    List<TravelLog> mTravelLogs;
    Context mContext;
    FirebaseApiHelper mFirebaseApiHelper;
    TravelLogAdapter.FavoriteListener mFavoriteListener;
    UserProfile mUserProfile;

    @BindView(R.id.tvTripName)
    TextView mTvTripName;

    @BindView(R.id.ivProfilePicture)
    DynamicHeightImageView mIvProfilePicture;

    @BindView(R.id.tvUserName)
    TextView mTvUserName;

    @BindView(R.id.tvScreenName)
    TextView mTvScreenName;

    @BindView(R.id.ivTripImage)
    ImageView mIvTripImage;

    @BindView(R.id.tvTripDescription)
    TextView mTvTripDescription;

    @BindView(R.id.ivComment)
    ImageView mIvComments;

    @BindView(R.id.ivVerified)
    ImageView mIvVerified;

    @BindView(R.id.ivShare)
    ImageView mIvShare;

    @BindView(R.id.ivFavorite)
    ImageView mIvFavorite;

    @BindView(R.id.tvFavorites)
    TextView mTvFavorites;

    public SummaryTravelLogViewHolder(Context context, View itemView, List<TravelLog> travelLogs, UserProfile mUserProfile, TravelLogAdapter.FavoriteListener favoriteListener) {
        super(itemView);

        mFirebaseApiHelper = FirebaseApiHelper.getInstance();
        itemView.setOnClickListener(this);
        ButterKnife.bind(this, itemView);

        mFavoriteListener = favoriteListener;

        itemView.setOnClickListener(this);
        mIvProfilePicture.setOnClickListener(this);
        mIvComments.setOnClickListener(this);
        mIvShare.setOnClickListener(this);
        mIvFavorite.setOnClickListener(this);

        this.mTravelLogs = travelLogs;
        this.mUserProfile = mUserProfile;
        mContext = context;
    }

    @Override
    public void onClick(View v) {
        int position = getLayoutPosition();
        final TravelLog travelLog = mTravelLogs.get(position);

        if (v instanceof ImageView) {
            if(v.getId() == R.id.ivProfilePicture) {
                /*Intent intent = UserActivity.newIntent(mContext);
                intent.putExtra(UserActivity.EXTRA_USER, Parcels.wrap(travelLog.getCurrentUserProfile()));
                mContext.startActivity(intent);*/

                LogUtil.logD(LOG_TAG, "Profile Image Selected, for user: " + travelLog.getUserId());
            }
            else if(v.getId() == R.id.ivFavorite){
                if(travelLog.isFavorited()){
                    AnimationHelper.updateButtonStatus((ImageView)v, true, R.drawable.ic_heart);
                }
                else{
                    AnimationHelper.updateButtonStatus((ImageView)v, true, R.drawable.ic_heart_full);
                }

                FirebaseApiHelper.getInstance().updateFavorite(travelLog, new FirebaseApiHelper.FirebaseRetrieverCallback<TravelLog>() {
                    @Override
                    public void callback(TravelLog updatedTravelLog) {

                        if(updatedTravelLog == null){
                            updatedTravelLog = travelLog;
                            updatedTravelLog.favoritesCount = travelLog.favoritesCount - 1;
                        }

                        mTvFavorites.setText(Long.toString(updatedTravelLog.getFavoritesCount()));
                        mFavoriteListener.onFavorite(position, updatedTravelLog);
                    }
                });
            }
        }
        else if(v instanceof TextView){
            if(v.getId() == R.id.ivComment) {
                // TODO: open a dialog fragment with comments and ability to add new comment
            }
            else if(v.getId() == R.id.ivShare){
                //TODO: share the travel log with entries
            }
        }
        else {
            Intent intent = new Intent(mContext, TravelEntriesActivity.class);
            intent.putExtra(TravelEntriesActivity.EXTRA_TRAVEL_LOG, Parcels.wrap(travelLog));
            intent.putExtra(TravelEntriesActivity.EXTRA_USER_PROFILE, Parcels.wrap(mUserProfile));
            ((Activity) mContext).startActivityForResult(intent, REQUEST_CODE, new Bundle());

            LogUtil.logD(LOG_TAG, "Travel log selected: " + travelLog);
        }
    }
}
