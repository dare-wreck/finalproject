package com.travelit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.TravelLog;
import com.travelit.util.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

public class TravelLogRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<TravelLog> mList;
    private TravelLogViewHolder viewHolder;
    private Context mContext;
    private LayoutInflater inflater;

    public static class TravelLogViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.tvLocation)
        TextView tvLocation;

        @BindView(R.id.tvDaysTillTrip)
        TextView tvDaysTillTrip;

        @BindView(R.id.ivTravelAlbumPic)
        ImageView ivTravelAlbumPic;

        public TravelLogViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public TravelLogRecycleAdapter(Context context, List<TravelLog> travelLogs) {
        mList = (travelLogs == null) ? new ArrayList<>():travelLogs;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.travel_log_item_view, parent, false);
        ButterKnife.bind(this,view);
        RecyclerView.ViewHolder viewHolder = new TravelLogViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TravelLog entry = mList.get(position);
        viewHolder = (TravelLogViewHolder) holder;

        viewHolder.tvTitle.setText(entry.title);
        if(entry.date != null) {
            //get current time
            long dataMillies = Long.valueOf(entry.date);
            long cur = Long.valueOf(DateTimeUtils.getUTCMillisAsString());

            long delta = dataMillies - cur;
            if(delta <= 0) {
                viewHolder.tvDaysTillTrip.setText(DateTimeUtils.parseDateTimeFromMillis(entry.date));
            } else {
                viewHolder.tvDaysTillTrip.setText(DateTimeUtils.parseRelativeTime(String.valueOf(delta)) + " days left");
            }

        }
        viewHolder.tvLocation.setText(entry.getDestination());
        if(entry.defaultImageUrl == null){
        } else {
            // viewHolder.ivTravelAlbumPic.setImageBitmap();
            Picasso.with(mContext)
                    .load(entry.getDefaultImageUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(viewHolder.ivTravelAlbumPic);
        }
       // viewHolder.ivTravelAlbumPic.setImageBitmap();
//        viewHolder.tvTitle.setText(entry.title.toString());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public boolean add(TravelLog object) {
        int lastIndex = mList.size();
        if (mList.add(object)) {
            notifyItemInserted(lastIndex);
            return true;
        } else {
            return false;
        }
    }
}