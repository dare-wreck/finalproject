package com.travelit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.LocationInfo;
import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.TravelLogEntryAssociation;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.DateTimeUtils;
import com.travelit.util.LocationUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class TravelLogEntryRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<TravelLogEntry> mList;
    private TravelLogEntryViewHolder viewHolder;
    private Context mContext;
    private LayoutInflater inflater;
    private boolean mEnableCheckBox;
    private FirebaseApiHelper mFirebaseHelper;

    public static class TravelLogEntryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.tvLocation)
        TextView tvLocation;

        @BindView(R.id.tvDaysTillTrip)
        TextView tvDaysTillTrip;

        @BindView(R.id.ivTravelAlbumPic)
        ImageView ivTravelAlbumPic;

        @BindView(R.id.cbSave)
        CheckBox cbSave;

        @BindView(R.id.ivClock)
        ImageView ivClock;

        public TravelLogEntryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public TravelLogEntryRecycleAdapter(Context context, List<TravelLogEntry> entries, boolean enableCheckBox) {
        mList = (entries == null) ? new ArrayList<>():entries;
        mContext = context;
        mEnableCheckBox = enableCheckBox;
        mFirebaseHelper = FirebaseApiHelper.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.travel_log_entry_item_view, parent, false);
        ButterKnife.bind(this,view);

        RecyclerView.ViewHolder viewHolder = new TravelLogEntryViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TravelLogEntry entry = mList.get(position);
        viewHolder = (TravelLogEntryViewHolder) holder;

        viewHolder.tvTitle.setText(entry.travelLatLong.name);

        if(entry.checkedIn) {
            viewHolder.tvDaysTillTrip.setVisibility(View.VISIBLE);
            viewHolder.ivClock.setVisibility(View.VISIBLE);
            viewHolder.tvDaysTillTrip.setText(DateTimeUtils.parseDateTimeFromMillis(entry.date));
        } else {
            viewHolder.tvDaysTillTrip.setVisibility(View.GONE);
            viewHolder.ivClock.setVisibility(View.GONE);
        }

        viewHolder.tvLocation.setText(entry.travelLatLong.address);

        if(!mEnableCheckBox) {
            viewHolder.cbSave.setVisibility(View.GONE);
        } else {
            if(entry.isSelected) {
                viewHolder.cbSave.setChecked(true);
                viewHolder.cbSave.setEnabled(false);
            } else {
                viewHolder.cbSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        TravelLogEntry entry = mList.get(position);
                        //Toast.makeText(mContext, "Need to implement save click", Toast.LENGTH_LONG).show();

                        if (isChecked){
                            //get the locality (city name from the Geo Coder API)
                            LocationInfo locationInfo = LocationUtil.getLocationInfoByLatLong(mContext, entry.travelLatLong);

                            //search the travel logs of current user by the locality on destination field to see if there is a matching travel log
                            //search currently works if the destination starts with the locality name i.e. if the localcity is san francisco & destination is
                            // 'san franciso blah blah', it works
                            mFirebaseHelper.getTravelLogsByLexicalSearchForCurrentUser(locationInfo.locality, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
                                @Override
                                public void callback(List<TravelLog> travelLogs) {
                                    if (travelLogs == null || travelLogs.size() <= 0) {
                                        createInitialTravelLogWithEntry(locationInfo, entry, new FirebaseApiHelper.FirebaseRetrieverCallback<TravelLogEntryAssociation>() {
                                            @Override
                                            public void callback(TravelLogEntryAssociation travelLogEntryAssociation) {
                                                if(travelLogEntryAssociation!= null) {
                                                    mFirebaseHelper.associateTravelLogEntry(entry.id, travelLogEntryAssociation, new FirebaseApiHelper.FirebaseRetrieverCallback<Boolean>() {
                                                        @Override
                                                        public void callback(Boolean data) {
                                                            entry.isSelected = true;
                                                            notifyItemChanged(position);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    } else {
                                        TravelLog travelLog = travelLogs.get(0); //get the first log for now & associate the entry with it
                                        TravelLogEntry travelLogEntry = getNewTravelLogEntry(entry);

                                        mFirebaseHelper.associateTravelLogEntry(entry.id, travelLog.getId(), travelLogEntry, new FirebaseApiHelper.FirebaseRetrieverCallback<Boolean>() {
                                            @Override
                                            public void callback(Boolean data) {
                                                entry.isSelected = true;
                                                notifyItemChanged(position);
                                            }
                                        });


                                    }
                                }
                            });
                        } //end of if
                    }
                });
            }


        }
        if(entry.images != null && entry.images.size() > 0){
            Picasso.with(mContext)
                    .load(entry.images.get(0).getUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(viewHolder.ivTravelAlbumPic);
        }
    }

    /**
     * this method helps to associate a travel log entry by creating a new TravelLog to the current user
     * when there is no matching travel log found in the user's Travel Logs
     */
    private void createInitialTravelLogWithEntry(final LocationInfo locationInfo, final TravelLogEntry travelLogEntry, FirebaseApiHelper.FirebaseRetrieverCallback<TravelLogEntryAssociation> finishedCallBack) {
        mFirebaseHelper.getCurrentUserProfile(new FirebaseApiHelper.FirebaseRetrieverCallback<UserProfile>() {
            @Override
            public void callback(UserProfile userProfile) {
                TravelLog travelLog = getNewTravelLog(locationInfo, userProfile, travelLogEntry);

                mFirebaseHelper.addTravelLogWithAnEntry(travelLog, finishedCallBack);
            }
        });
    }

    /**
     * Copy the existing travel log fields and return a newly created travel log object
     * @param entry
     * @return
     */
    private TravelLogEntry getNewTravelLogEntry(TravelLogEntry entry) {
        TravelLogEntry travelLogEntry = new TravelLogEntry();
        travelLogEntry.title = entry.title;
        travelLogEntry.travelLatLong = entry.travelLatLong;
        travelLogEntry.dateCreated = DateTimeUtils.getUTCMillisAsString();
        travelLogEntry.notify = entry.notify;
        travelLogEntry.description = entry.description;
        travelLogEntry.notes = entry.notes;
        travelLogEntry.favorited = false;
        travelLogEntry.favoritesCount = 0;
        travelLogEntry.checkedIn = false;
        travelLogEntry.commentsCount = 0;
        travelLogEntry.privacy = PrivacyEnum.PRIVATE;
        travelLogEntry.status = 2;

        return travelLogEntry;
    }

    private TravelLog getNewTravelLog(final LocationInfo locationInfo, final UserProfile userProfile, final TravelLogEntry travelLogEntry){
        TravelLog travelLog = new TravelLog();

        travelLog.title = String.format("Your newly added %s trip.", locationInfo.locality);
        travelLog.destination = locationInfo.cityState + " " + locationInfo.country;
        travelLog.dateCreated = DateTimeUtils.getUTCMillisAsString();
        travelLog.privacy = PrivacyEnum.PRIVATE;
        travelLog.notify = false;
        travelLog.userId = mFirebaseHelper.getFirebaseUser().getUid();
        travelLog.userProfile = userProfile;
        travelLog.description = String.format("Your newly added %s trip.", locationInfo.locality);
        travelLog.defaultImageUrl = "https://photos.smugmug.com/Trips/Scotland-2014-National-Geograp/i-GtBtqLv/0/S/5D3_0627-Edit-S.jpg";

        travelLog.travelLogEntries = new ArrayList<>();
        travelLog.travelLogEntries.add(getNewTravelLogEntry(travelLogEntry));

        return  travelLog;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }
    public boolean add(TravelLogEntry object) {
        int lastIndex = mList.size();
        if (mList.add(object)) {
            notifyItemInserted(lastIndex);
            return true;
        } else {
            return false;
        }
    }
}