package com.travelit.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.LocationInfo;
import com.travelit.models.Orientation;
import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.TravelLogEntryAssociation;
import com.travelit.models.TravelStatus;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.DateTimeUtils;
import com.travelit.util.LocationUtil;
import com.travelit.util.VectorDrawableUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class TimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<TravelLogEntry> mFeedList;
    private Context mContext;
    private Orientation mOrientation;
    private LayoutInflater mLayoutInflater;
    private int selectedIndex;
    private int minViewableIndex;
    private int maxViewableIndex;
    private boolean mEnableCheckBox;
    private boolean mIsTravelLogComplete;
    private FirebaseApiHelper mFirebaseHelper;

    public static class TimelineViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.tvName)
        TextView tvName;

        @Nullable
        @BindView(R.id.tvAddress)
        TextView tvAddress;

        @BindView(R.id.time_marker)
        TimelineView mTimelineView;

        @BindView(R.id.timeline_card)
        CardView cardView;

        @Nullable
        @BindView(R.id.checkBox)
        CheckBox checkbox;

        @Nullable
        @BindView(R.id.ivPic)
        ImageView ivPic;

        public TimelineViewHolder(View itemView, int viewType) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            mTimelineView.initLine(viewType);
        }
    }

    public TimelineAdapter(List<TravelLogEntry> feedList, Orientation orientation, boolean enableCheckBox, boolean isTravelLogComplete) {

        mFeedList = (feedList == null) ? new ArrayList<>():feedList;
        mOrientation = orientation;
        selectedIndex = 0;
        minViewableIndex = Integer.MAX_VALUE;
        maxViewableIndex = Integer.MIN_VALUE;
        mEnableCheckBox = enableCheckBox;
        mFirebaseHelper = FirebaseApiHelper.getInstance();
        mIsTravelLogComplete = isTravelLogComplete;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        if(mOrientation == Orientation.HORIZONTAL) {
            view = mLayoutInflater.inflate(R.layout.item_timeline_horizontal_line_padding, parent, false);
        } else {
            view = mLayoutInflater.inflate(R.layout.item_timeline_line_padding, parent, false);
        }

        return new TimelineViewHolder(view, viewType);
    }

    public void updateValue(TravelLogEntry entry) {

        for(int i = 0; i < mFeedList.size(); i++) {
            if(mFeedList.get(i).id == entry.id){
                mFeedList.set(i,entry);
                notifyItemChanged(i);
            }
        }
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        minViewableIndex = Math.min(minViewableIndex, position);
        maxViewableIndex = Math.max(maxViewableIndex, position);

        TravelLogEntry entry = mFeedList.get(position);
        TimelineViewHolder timelineHolder = (TimelineViewHolder) holder;

        if(mIsTravelLogComplete){
            //timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
            switch(entry.categoryType) {
                case 0: { //sight see
                    timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_account_balance_black));
                    break;
                }
                case 1: { //food
                    timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_local_dining_black));
                    break;
                }
                case 2: { //night life
                    timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_night_club));
                    break;
                }
                case 3: { //active

                    timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_directions_bike_black));
                    break;
                }
                default : {
                    timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_terrain_black));

                }
            }
        } else {
            //Icons on timeline
            if(entry.status == TravelStatus.INACTIVE.ordinal() ) {
                //timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker_inactive), ContextCompat.getColor(mContext, R.color.colorAccent));
                timelineHolder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, R.color.colorPrimary));
            } else if(entry.status == TravelStatus.ACTIVE.ordinal()) {
                timelineHolder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
            } else {
                timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
        }

        if(entry.checkedIn) {
            timelineHolder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
        }




        if(mOrientation == Orientation.HORIZONTAL) {
            timelineHolder.tvName.setText(entry.travelLatLong.name);
            if( position == selectedIndex) {
                timelineHolder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.timeline_pressed));
            } else {
                timelineHolder.cardView.setCardBackgroundColor(Color.WHITE);
            }
        } else {
            if(entry.travelLatLong != null && entry.travelLatLong.address != null && !entry.travelLatLong.address.isEmpty()) {
                timelineHolder.tvAddress.setVisibility(View.VISIBLE);
                //timelineHolder.tvAddress.setText(DateTimeUtils.parseDateTime(entry.date, "yyyy-MM-dd HH:mm", "hh:mm a, dd-MMM-yyyy"));
                timelineHolder.tvAddress.setText(entry.travelLatLong.address);
            }
            else {
                timelineHolder.tvAddress.setVisibility(View.GONE);
            }
            if(!mEnableCheckBox) {
                timelineHolder.checkbox.setVisibility(View.GONE);
            } else {
                if(entry.isSelected) {
                    timelineHolder.checkbox.setVisibility(View.GONE);
                } else {
                    timelineHolder.checkbox.setChecked(entry.isSelected);
                    timelineHolder.checkbox.setVisibility(View.VISIBLE);
                    timelineHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            TravelLogEntry entry = mFeedList.get(position);
                            //Toast.makeText(mContext, "Need to implement save click", Toast.LENGTH_LONG).show();

                            if (isChecked){
                                //get the locality (city name from the Geo Coder API)
                                LocationInfo locationInfo = LocationUtil.getLocationInfoByLatLong(mContext, entry.travelLatLong);

                                //search the travel logs of current user by the locality on destination field to see if there is a matching travel log
                                //search currently works if the destination starts with the locality name i.e. if the localcity is san francisco & destination is
                                // 'san franciso blah blah', it works
                                mFirebaseHelper.getTravelLogsByLexicalSearchForCurrentUser(locationInfo.locality, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
                                    @Override
                                    public void callback(List<TravelLog> travelLogs) {
                                        if (travelLogs == null || travelLogs.size() <= 0) {
                                            createInitialTravelLogWithEntry(locationInfo, entry, new FirebaseApiHelper.FirebaseRetrieverCallback<TravelLogEntryAssociation>() {
                                                @Override
                                                public void callback(TravelLogEntryAssociation travelLogEntryAssociation) {
                                                    if(travelLogEntryAssociation!= null) {
                                                        mFirebaseHelper.associateTravelLogEntry(entry.id, travelLogEntryAssociation, new FirebaseApiHelper.FirebaseRetrieverCallback<Boolean>() {
                                                            @Override
                                                            public void callback(Boolean data) {
                                                                entry.isSelected = true;
                                                                notifyItemChanged(position);
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        } else {
                                            TravelLog travelLog = travelLogs.get(0); //get the first log for now & associate the entry with it
                                            TravelLogEntry travelLogEntry = getNewTravelLogEntry(entry);

                                            mFirebaseHelper.associateTravelLogEntry(entry.id, travelLog.getId(), travelLogEntry, new FirebaseApiHelper.FirebaseRetrieverCallback<Boolean>() {
                                                @Override
                                                public void callback(Boolean data) {
                                                    entry.isSelected = true;
                                                    notifyItemChanged(position);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
            if(entry.images != null && entry.images.size() > 0) {
                timelineHolder.ivPic.setVisibility(View.VISIBLE);
                Picasso.with(mContext)
                        .load(entry.images.get(0).getUrl())
                        .transform(new RoundedCornersTransformation(0,0))
                        .resize(1100,850)
                        .placeholder(R.drawable.ic_photo)
                        .error(R.drawable.ic_camera)
                        .into(timelineHolder.ivPic);
            } else {
                timelineHolder.ivPic.setVisibility(View.GONE);
            }
            /*
             timelineHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    TravelLogEntry entry = mFeedList.get(position);
                    entry.favorited = isChecked;
                    //  notify(position);
                    //Toast.makeText(mContext, "Update to database of value change: " + entry.title, Toast.LENGTH_SHORT).show();
                }
            });
*/
            timelineHolder.tvName.setText(entry.travelLatLong.name);
        }
    }
    /**
     * this method helps to associate a travel log entry by creating a new TravelLog to the current user
     * when there is no matching travel log found in the user's Travel Logs
     */
    private void createInitialTravelLogWithEntry(final LocationInfo locationInfo, final TravelLogEntry travelLogEntry, FirebaseApiHelper.FirebaseRetrieverCallback<TravelLogEntryAssociation> finishedCallBack) {
        mFirebaseHelper.getCurrentUserProfile(new FirebaseApiHelper.FirebaseRetrieverCallback<UserProfile>() {
            @Override
            public void callback(UserProfile userProfile) {
                TravelLog travelLog = getNewTravelLog(locationInfo, userProfile, travelLogEntry);

                mFirebaseHelper.addTravelLogWithAnEntry(travelLog, finishedCallBack);
            }
        });
    }


    private TravelLog getNewTravelLog(final LocationInfo locationInfo, final UserProfile userProfile, final TravelLogEntry travelLogEntry){
        TravelLog travelLog = new TravelLog();

        travelLog.title = String.format("Your newly added %s trip.", locationInfo.locality);
        travelLog.destination = locationInfo.cityState + " " + locationInfo.country;
        travelLog.dateCreated = DateTimeUtils.getUTCMillisAsString();
        travelLog.privacy = PrivacyEnum.PRIVATE;
        travelLog.notify = false;
        travelLog.userId = mFirebaseHelper.getFirebaseUser().getUid();
        travelLog.userProfile = userProfile;
        travelLog.description = String.format("Your newly added %s trip.", locationInfo.locality);
        travelLog.defaultImageUrl = "https://photos.smugmug.com/Trips/Scotland-2014-National-Geograp/i-GtBtqLv/0/S/5D3_0627-Edit-S.jpg";

        travelLog.isComplete = false;
        travelLog.travelLogEntries = new ArrayList<>();
        travelLog.travelLogEntries.add(getNewTravelLogEntry(travelLogEntry));

        return  travelLog;
    }

    /**
     * Copy the existing travel log fields and return a newly created travel log object
     * @param entry
     * @return
     */
    private TravelLogEntry getNewTravelLogEntry(TravelLogEntry entry) {
        TravelLogEntry travelLogEntry = new TravelLogEntry();
        travelLogEntry.title = entry.title;
        travelLogEntry.travelLatLong = entry.travelLatLong;
        travelLogEntry.dateCreated = DateTimeUtils.getUTCMillisAsString();
        travelLogEntry.notify = entry.notify;
        travelLogEntry.description = entry.description;
        travelLogEntry.notes = entry.notes;
        travelLogEntry.favorited = false;
        travelLogEntry.favoritesCount = 0;
        travelLogEntry.checkedIn = false;
        travelLogEntry.commentsCount = 0;
        travelLogEntry.status = 2;
        travelLogEntry.privacy = PrivacyEnum.PRIVATE;

        return travelLogEntry;
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

    public int getMinViewableIndex() {
        return minViewableIndex;
    }

    public int getMaxViewableIndex() {
        return maxViewableIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }
}