package com.travelit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.Review;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by darewreck_PC on 5/5/2017.
 */

public class ReviewRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<Review> mReviews;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivProfilePic)
        public ImageView ivProfilePic;

        @BindView(R.id.tvName)
        public TextView tvName;

        @BindView(R.id.tvTime)
        public TextView tvTime;

        @BindView(R.id.rbRating)
        public RatingBar rbRating;

        @BindView(R.id.tvText)
        public TextView tvText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public ReviewRecyclerAdapter(Context context, List<Review> reviews){
        this.mReviews = reviews;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View reviewView = inflater.inflate(R.layout.item_review, parent, false);

        ViewHolder viewHolder = new ViewHolder(reviewView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Review review = mReviews.get(position);

        ReviewRecyclerAdapter.ViewHolder viewHolder = (ReviewRecyclerAdapter.ViewHolder) holder;
        viewHolder.tvText.setText(review.getText().toString());
        viewHolder.tvName.setText(review.getAuthorName().toString());
        viewHolder.tvTime.setText(review.getRelativeTimeDescription().toString());
        viewHolder.rbRating.setRating(review.getRating());

        Picasso.with(mContext)
                .load(review.getProfilePhotoUrl())
                .transform(new RoundedCornersTransformation(0,0))
                .resize(50,50)
                .placeholder(R.drawable.ic_photo)
                .error(R.drawable.ic_camera)
                .into(viewHolder.ivProfilePic);
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    public void add(List<Review> reviews) {
        for(Review review: reviews) {
            this.mReviews.add(0, review);
            notifyItemInserted(0);
        }
    }

}