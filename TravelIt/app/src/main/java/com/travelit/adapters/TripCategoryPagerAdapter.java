package com.travelit.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.travelit.fragments.CurrentTripListFragment;
import com.travelit.fragments.PastTripListFragment;
import com.travelit.models.User;
import com.travelit.models.UserProfile;

public class TripCategoryPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private String tabTitles[] = new String[] { "Upcoming", "Past"};
    private UserProfile mUserProfile;

    public TripCategoryPagerAdapter(UserProfile userProfile, FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        this.mUserProfile = userProfile;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if(position == 0) {
            fragment =  CurrentTripListFragment.newInstance(this.mUserProfile);
        } else if(position == 1) {
            fragment = PastTripListFragment.newInstance(this.mUserProfile);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
