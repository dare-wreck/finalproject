package com.travelit.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.LayoutType;
import com.travelit.models.MediaRecyclerViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Adapter that handles recycling of media types such as images and videos
 * @author Sai Muppa
 * TODO: later expand to handle video types
 */
public class MediaRecyclerAdapter extends RecyclerView.Adapter<MediaRecyclerAdapter.ImageViewHolder> {
    private List<MediaRecyclerViewModel> mMedias;
    Context mContext;
    LayoutType mLayoutType;
    public MediaRecyclerAdapter(Context context, LayoutType layoutType, List<MediaRecyclerViewModel> mediaUrls) {
        mContext = context;
        this.mMedias = mediaUrls;
        this.mLayoutType = layoutType;
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.ivTripImage)
        public ImageView mImageView;

        //@BindView(R.id.tvTitle)
        //public TextView mTvTitle;

        public View rootView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rootView = itemView;
        }
    }

    @Override
    public int getItemCount() {
        return mMedias.size();
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        String imageUrl = mMedias.get(position).url;

        Picasso.with(mContext)
                .load(imageUrl)
                .transform(new RoundedCornersTransformation(5, 5))
                .resize(1000, 1000)
                .placeholder(R.drawable.ic_photo)
                .error(R.drawable.ic_camera)
                .into(holder.mImageView);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.item_image, parent, false);
        return new ImageViewHolder(convertView);
    }

}
