package com.travelit.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.Media;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

public class TravelLogEntryImagePagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    /*
    int[] mResources = {
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image4,
            R.drawable.image5,
            R.drawable.image6
    };
    */
    private List<Media> imageList;

    public TravelLogEntryImagePagerAdapter(){
        imageList = new ArrayList<>();
    }

    public TravelLogEntryImagePagerAdapter(Context context, List<Media> media) {
        super();
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        imageList = (media != null) ? media:new ArrayList<>();
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        Media media = imageList.get(position);

        if(media.getUrl() != null){
            Picasso.with(mContext)
                    .load(media.getUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(imageView);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
