package com.travelit.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.travelit.R;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;

import java.util.ArrayList;

public class SpinTravelLogEntryAdapter extends ArrayAdapter<TravelLog> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private TravelLog[] values;

    public SpinTravelLogEntryAdapter(Context context, int textViewResourceId,
                                     TravelLog[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.length;
    }

    public TravelLog getItem(int position){
        return values[position];
    }

    public long getItemId(int position){
        return position;
    }

    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(values[position].title);
        return view;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(values[position].title);
        //View view =
        //TextView label = new TextView(context);
        //label.setTextColor(Color.BLACK);
        //label.setText(values[position].title);

        return view;
    }
}
