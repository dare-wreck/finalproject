/**
 * This package contains various adapters of the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.adapters;