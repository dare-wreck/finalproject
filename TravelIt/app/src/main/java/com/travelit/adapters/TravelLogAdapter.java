package com.travelit.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.TravelLog;
import com.travelit.models.UserProfile;
import com.travelit.util.CircleTransform;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * @author Sai Muppa
 */
public class TravelLogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<TravelLog> mTravelLogs;
    Context mContext;
    UserProfile mUserProfile;

    static final int SUMMARY_VIEW = 1;
    static final int IMAGE_ONLY_VIEW = 2;
    static final int VIDEO_ONLY_VIEW = 3;

    static final String IMAGE_MEDIA_TYPE = "photo";
    static final String VIDEO_MEDIA_TYPE = "video";

    public TravelLogAdapter(Context context, UserProfile userProfile, List<TravelLog> travelLogs){
        this.mContext = context;
        mTravelLogs = (travelLogs == null) ? new ArrayList<>():travelLogs;
        this.mUserProfile = userProfile;
    }

    interface FavoriteListener{
        void onFavorite(int position, TravelLog newTravelLog);
    }

    /**
     * Get the travel log count
     * @return
     */
    @Override
    public int getItemCount(){
        return this.mTravelLogs.size();
    }

    /**
     * Get the view type based on the media type
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position){
        TravelLog travelLog = mTravelLogs.get(position);

        //TODO: build out the logic for view figuring out the view type

        return SUMMARY_VIEW;
    }

    /**
     * Inflate the view holder based on the view type
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType){
            //TODO: enable these views later
            /*case IMAGE_ONLY_VIEW:

                break;

            case VIDEO_ONLY_VIEW:
                break;*/

            case SUMMARY_VIEW:
            default:
                View summaryView = inflater.inflate(R.layout.item_travel_log_summary, parent, false);
                viewHolder = new SummaryTravelLogViewHolder(mContext, summaryView, mTravelLogs, mUserProfile, new FavoriteListener() {
                    @Override
                    public void onFavorite(int position, TravelLog newTravelLog) {
                        mTravelLogs.remove(position);
                        //notifyItemRemoved(position);
                        mTravelLogs.add(position, newTravelLog);
                        //notifyItemInserted(position);
                    }
                });
                break;
        }

        return viewHolder;
    }

    /**
     * Bind the view holder based on the view type
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            //TODO: enable these views later
            /*case IMAGE_ONLY_VIEW:

                break;

            case VIDEO_ONLY_VIEW:
                break;*/

            case SUMMARY_VIEW:
            default:
                SummaryTravelLogViewHolder summaryTravelLogViewHolder = (SummaryTravelLogViewHolder) holder;
                bindSummaryTravelLogViewHolder(summaryTravelLogViewHolder, position);
                break;
        }

    }

    /**
     * Binds the summary travel log
     * @param viewHolder
     * @param position
     */
    private void bindSummaryTravelLogViewHolder(SummaryTravelLogViewHolder viewHolder, int position) {
        TravelLog travelLog = mTravelLogs.get(position);

        if(travelLog != null){
            viewHolder.mTvTripName.setText(travelLog.getTitle());

            //UserProfile profile  = TestServiceUtil.getUserProfile();
            viewHolder.mTvUserName.setText(travelLog.userProfile.getName());
            viewHolder.mTvScreenName.setText("@" + travelLog.userProfile.getScreenName());

            Picasso.with(mContext).load(travelLog.userProfile.getProfileImageUrl())
                    .transform(new CircleTransform())
                    .resize(48, 48)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(viewHolder.mIvProfilePicture);

            viewHolder.mTvTripDescription.setText(travelLog.getDescription());
            viewHolder.mTvFavorites.setText(Long.toString(travelLog.getFavoritesCount()));
            viewHolder.mIvFavorite.setTag(travelLog);

            if(travelLog.isFavorited()){
                viewHolder.mIvFavorite.setImageResource(R.drawable.ic_heart_full);
            }else {
                viewHolder.mIvFavorite.setImageResource(R.drawable.ic_heart);
            }

            Picasso.with(mContext)
                    .load(travelLog.getDefaultImageUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(viewHolder.mIvTripImage);
        }
    }

    /**
     * Clear all the travelLogs
     */
    public void clear() {
        mTravelLogs.clear();
        notifyDataSetChanged();
    }

    /***
     * Adds a list of travel logs
     * @param travelLogs
     */
    public void addAll(List<TravelLog> travelLogs) {
        mTravelLogs.addAll(travelLogs);
        notifyDataSetChanged();
    }
}
