package com.travelit.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;

import com.travelit.R;
import com.travelit.fragments.StartTravelLogEntryFragment;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;

public class StartTravelFragmentPagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private TravelLog mTravelLog;

    public StartTravelFragmentPagerAdapter(FragmentManager fm, TravelLog travelLog) {
        super(fm);
        mTravelLog = travelLog;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return mTravelLog.travelLogEntries.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        TravelLogEntry entry = mTravelLog.travelLogEntries.get(position);
        StartTravelLogEntryFragment fragEntry = StartTravelLogEntryFragment.newInstance(entry);
        return fragEntry;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public float getPageWidth (int position) {
        return 0.93f;
    }
}
