package com.travelit.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelit.R;
import com.travelit.eventlisteners.EndlessRecyclerViewScrollListener;
import com.travelit.models.LayoutType;
import com.travelit.models.Media;
import com.travelit.models.MediaRecyclerViewModel;
import com.travelit.models.TravelLogEntry;
import com.travelit.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TravelLogEntryAdapter extends RecyclerView.Adapter<TravelLogEntryAdapter.ViewHolder> {

    public static final String TAG ="TravelLogEntryAdapter";

    private List<TravelLogEntry> mTravelLogEntries;

    Context mContext;

    public TravelLogEntryAdapter(Context context, List<TravelLogEntry> travelLogEntries) {
        mContext = context;
        mTravelLogEntries = (travelLogEntries == null) ? new ArrayList<>():travelLogEntries;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.rvTravelMedia)
        public RecyclerView mRecyclerViewRow;

        @BindView(R.id.tvTitle)
        public TextView mTvTitle;

        @BindView(R.id.ivFavorite)
        public ImageView mIvFavorite;

        @BindView(R.id.tvFavorites)
        public TextView mTvFavorites;

        @BindView(R.id.tvDescription)
        public TextView mTvDescription;

        SnapHelper mSnapHelper;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mSnapHelper = new LinearSnapHelper();
        }
    }

    @Override
    public int getItemCount() {
        return mTravelLogEntries.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        TravelLogEntry travelLogEntry = mTravelLogEntries.get(position);

        List<MediaRecyclerViewModel> rowMediaItems = new ArrayList<>();
        if(travelLogEntry.images != null){
            MediaRecyclerViewModel mediaRecyclerViewModel;
            for (Media media: travelLogEntry.images) {
                mediaRecyclerViewModel = new MediaRecyclerViewModel(media.url, media.title, media.description, travelLogEntry);
                rowMediaItems.add(mediaRecyclerViewModel);
            }
        }

        if(travelLogEntry != null && rowMediaItems != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            holder.mRecyclerViewRow.setLayoutManager(layoutManager);
            holder.mRecyclerViewRow.setHasFixedSize(true);
            MediaRecyclerAdapter rowsRecyclerAdapter = new MediaRecyclerAdapter(mContext, LayoutType.LINEAR_HORIZONTAL, rowMediaItems);
            holder.mRecyclerViewRow.setAdapter(rowsRecyclerAdapter);

            holder.mSnapHelper.attachToRecyclerView(holder.mRecyclerViewRow);

            final RecyclerView finalRecyclerView = holder.mRecyclerViewRow;

            finalRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    LogUtil.logD(TAG, "Horizontal scroll triggered on carousel adapter.");
                }
            });

            holder.mIvFavorite.setImageResource(R.drawable.ic_heart); //TODO: conditionally updated based on favorite or not
            holder.mTvFavorites.setText(String.format("%s %s", travelLogEntry.favoritesCount, "Likes"));
            holder.mTvTitle.setText(travelLogEntry.title);
            holder.mTvDescription.setText(travelLogEntry.description);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.item_travel_log_entry, parent, false);
        return new ViewHolder(convertView);
    }

    /**
     * Clear all the travel log entries
     */
    public void clear() {
        mTravelLogEntries.clear();
        notifyDataSetChanged();
    }

    /***
     * Adds a list of travel log entries
     * @param travelLogEntries
     */
    public void addAll(List<TravelLogEntry> travelLogEntries) {
        mTravelLogEntries.addAll(travelLogEntries);
        notifyDataSetChanged();
    }
}
