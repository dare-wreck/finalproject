package com.travelit.util;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

public class DateTimeUtils {
    public static String parseDateTimeFromMillis(String millis) {
        try {
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(Long.valueOf(millis));
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
            formatter.setCalendar(calendar);
            return formatter.format(calendar.getTime());

        } catch(Exception e) {
            e.printStackTrace();
            return "";
        }


    }

    public static String parseRelativeTime(String millis) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(Long.valueOf(millis));
      //  DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        //formatter.setCalendar(calendar);
        int day = calendar.get(Calendar.DAY_OF_YEAR);

        return String.valueOf(day);
    }

    public static String parseDateTimeFromMillisToDate(String millis) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(Long.valueOf(millis));
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter.setCalendar(calendar);

        return formatter.format(calendar.getTime());
    }

    public static String parseDateTime(String dateString, String originalFormat, String outputFromat){

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            SimpleDateFormat dateFormat=new SimpleDateFormat(outputFromat, new Locale("US"));

            return dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getRelativeTimeSpan(String dateString, String originalFormat){

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            return DateUtils.getRelativeTimeSpanString(date.getTime()).toString();

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getRelativeTimeSpan(long timeStamp){
        try {
            return DateUtils.getRelativeTimeSpanString(timeStamp).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getUTCMillisAsString(){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        long time = cal.getTimeInMillis();
        return Long.toString(time);
    }

    public static String getUTCMillisAsString( int year, int month, int dayOfMonth) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(year, month, dayOfMonth);
        return String.valueOf(cal.getTimeInMillis());

    }
}
