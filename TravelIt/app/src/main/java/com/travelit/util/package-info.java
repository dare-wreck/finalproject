/**
 * This package contains the utilities used by the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.util;