package com.travelit.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Helper for JSONObjects using GSON
 */
public class JsonHelper {
    @SuppressWarnings("unchecked")
    public static <T> T GetResponseObject(String responseString, Class<T> responseClass) {
        GsonBuilder gsonBuilder = new GsonBuilder();

        Gson gson = gsonBuilder.create();
        T responseObject = gson.fromJson(responseString, responseClass);
        return responseObject;
    }
}
