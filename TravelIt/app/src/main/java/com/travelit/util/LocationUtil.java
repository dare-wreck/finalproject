package com.travelit.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.travelit.models.LocationInfo;
import com.travelit.models.TravelLatLong;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by sm032858 on 4/24/17.
 */

public class LocationUtil {
    public static LocationInfo getLocationInfoByLatLong(Context context, TravelLatLong travelLatLong){
        return getLocationInfoByLatLong(context, travelLatLong.latitude, travelLatLong.longitude);
    }

    public static LocationInfo getLocationInfoByLatLong(Context context, double latitude, double longitude){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        LocationInfo locationInfo = new LocationInfo();
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            locationInfo.location = addresses.get(0).getAddressLine(0);
            locationInfo.cityState = addresses.get(0).getAddressLine(1);
            locationInfo.country = addresses.get(0).getAddressLine(2);
            locationInfo.locality = addresses.get(0).getLocality();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locationInfo;
    }
}
