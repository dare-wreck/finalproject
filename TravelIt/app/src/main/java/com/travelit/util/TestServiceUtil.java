package com.travelit.util;

import com.travelit.models.Media;
import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.UserProfile;
import com.travelit.models.dto.TravelLogDto;
import com.travelit.networks.FirebaseApiHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  @author Sai Muppa
 */
public class TestServiceUtil {

    public TestServiceUtil() {
    }

    /*
    public static List<TravelLog> getTravelLogs() {
        List<TravelLog> travelLogs = new ArrayList<>();

        TravelLog travelLog;
        UserProfile userProfile;
        for(int index = 0; index < 100 ; index ++){
            userProfile = new UserProfile("ABCDXEFGHIJKLMNOPQSTUVWYZ", "John Doe", "@jdoe", "http://www.gravatar.com/avatar/67af57a8547180a427d2547c599d7887?d=identicon", "San Francisco, USA",
                    "Traveller in search of nirvana.", 100000, 100, "Sun Apr 02 15:16:07 +0000 2017", 1000, true, "");

            TravelLogDto dto = new TravelLogDto("1-Day Seattle", "All you can see in Seattle in a day.", "Seattle, WA", 0, null,
                    0, "https://i.imgur.com/ob6cPVf.jpg", null, getTravelLogEntriesMap(), FirebaseApiHelper.getInstance().getFirebaseUser().getUid());

            TravelLog convertedTravelLog = new TravelLog();
            convertedTravelLog.title = dto.getTitle();
            convertedTravelLog.description = dto.getDescription();
            convertedTravelLog.destination = dto.getDestination();
            convertedTravelLog.commentsCount = dto.getCommentsCount();
            convertedTravelLog.favoritesCount = dto.getFavoritesCount();
            convertedTravelLog.defaultImageUrl = dto.getDefaultImageUrl();
            convertedTravelLog.favorited = false;

            travelLogs.add(convertedTravelLog);
        }

        return travelLogs;
    }
    */
    /*
    public static List<TravelLog> getTravelLogsInput() {
        List<TravelLog> travelLogs = new ArrayList<>();

        TravelLog travelLog;
        UserProfile userProfile;
        for(int index = 0; index < 100 ; index ++){
            userProfile = new UserProfile("ABCDXEFGHIJKLMNOPQSTUVWYZ", "John Doe", "@jdoe", "http://www.gravatar.com/avatar/67af57a8547180a427d2547c599d7887?d=identicon", "San Francisco, USA",
                    "Traveller in search of nirvana.", 100000, 100, "Sun Apr 02 15:16:07 +0000 2017", 1000, true, "");

            TravelLog log = new TravelLog("1-Day Seattle", "All you can see in Seattle in a day.", "Seattle, WA", 1000,
                    1000, "https://i.imgur.com/ob6cPVf.jpg", true, getTravelLogEntries(), FirebaseApiHelper.getInstance().getFirebaseUser().getUid());

            travelLogs.add(log);
        }

        return travelLogs;
    }
    */
    public static UserProfile getUserProfile() {
        return new UserProfile("1", "Joe Doe", "@JDoe", "https://i.imgur.com/ob6cPVf.jpg", "Seattle",
                "Desc", 1000, 1000, "Random Time", 1000, true, "https://i.imgur.com/ob6cPVf.jpg");
    }


    public static Map<String, TravelLogEntry> getTravelLogEntriesMap() {
        Map<String, TravelLogEntry> travelLogEntries = new HashMap();
        TravelLogEntry travelLogEntry;

        for(int index = 0; index < 10 ; index ++){
            travelLogEntry = new TravelLogEntry("SeattlePikePlaceId", "Pike place market - Seattle", "This is of my favorite spots. This location has fresh fish, first Starbucks, nice baked goods.",
                    PrivacyEnum.PUBLIC, true, 47.608013, -122.335167, 1000, 1000, true,
                    new ArrayList<>(Arrays.asList(new Media("1", "https://feel-planet.com/wp-content/uploads/2015/10/lake-tekapo-front.jpg", "New Zealand", null),
                            new Media("1", "https://i.ytimg.com/vi/NekC0JEfVBw/hqdefault.jpg", "iceland", null),
                            new Media("1", "http://images.nationalgeographic.com/wpf/media-live/photos/000/333/cache/iceland-seljalandsfoss-travel-pictures_33392_990x742.jpg", "iceland", null),
                            new Media("1", "https://feel-planet.com/wp-content/uploads/2015/10/lake-tekapo-front.jpg", "New Zealand", null))), null,
                    "somerandomid"
            );

            //("ABCDXEFGHIJKLMNOPQSTUVWYZ", "John Doe", "@jdoe", "http://www.gravatar.com/avatar/67af57a8547180a427d2547c599d7887?d=identicon", "San Francisco, USA",
            //"Traveller in search of nirvana.", 100000, 100, "Sun Apr 02 15:16:07 +0000 2017", 1000, true, "");
            travelLogEntries.put("" + index + "", travelLogEntry);
        }

        return travelLogEntries;
    }

    public static List<TravelLogEntry> getTravelLogEntries() {
        List<TravelLogEntry> travelLogEntries = new ArrayList<>();
        TravelLogEntry travelLogEntry;

        for(int index = 0; index < 10 ; index ++){
            travelLogEntry = new TravelLogEntry("SeattlePikePlaceId", "Pike place market - Seattle", "This is of my favorite spots. This location has fresh fish, first Starbucks, nice baked goods.",
                    PrivacyEnum.PUBLIC, true, 47.608013, -122.335167, 1000, 1000, true,
                    new ArrayList<>(Arrays.asList(new Media("1", "https://feel-planet.com/wp-content/uploads/2015/10/lake-tekapo-front.jpg", "New Zealand", null),
                            new Media("1", "https://i.ytimg.com/vi/NekC0JEfVBw/hqdefault.jpg", "iceland", null),
                            new Media("1", "http://images.nationalgeographic.com/wpf/media-live/photos/000/333/cache/iceland-seljalandsfoss-travel-pictures_33392_990x742.jpg", "iceland", null),
                            new Media("1", "https://feel-planet.com/wp-content/uploads/2015/10/lake-tekapo-front.jpg", "New Zealand", null))), null,
                    "somerandomid"
            );

            //("ABCDXEFGHIJKLMNOPQSTUVWYZ", "John Doe", "@jdoe", "http://www.gravatar.com/avatar/67af57a8547180a427d2547c599d7887?d=identicon", "San Francisco, USA",
            //"Traveller in search of nirvana.", 100000, 100, "Sun Apr 02 15:16:07 +0000 2017", 1000, true, "");
            travelLogEntries.add(travelLogEntry);
        }

        return travelLogEntries;
    }
}
