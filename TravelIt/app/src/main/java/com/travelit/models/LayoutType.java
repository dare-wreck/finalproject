package com.travelit.models;

/**
 * Created by sm032858 on 4/15/17.
 */

public enum LayoutType {
    LINEAR_HORIZONTAL,
    LINEAR_VERTICAL,
    STAGGERED_GRID,
}
