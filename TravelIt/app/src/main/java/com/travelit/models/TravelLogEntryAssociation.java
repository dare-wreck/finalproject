package com.travelit.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sm032858 on 4/24/17.
 */
@IgnoreExtraProperties
@Parcel(analyze={TravelLogEntryAssociation.class})
public class TravelLogEntryAssociation {
    public String travelEntryId;
    public String travelLogId;

    public TravelLogEntryAssociation(){}

    @Exclude
    public Map<String,Object> toMap() {
        Map<String, Object> result = new HashMap<>();

        result.put("travelEntryId", travelEntryId);
        result.put("travelLogId", travelLogId);

        return result;
    }
}
