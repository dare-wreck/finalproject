package com.travelit.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by darewreck on 4/8/17.
 * Edited by Sai Muppa
 */
@IgnoreExtraProperties
@Parcel(analyze={TravelLog.class})
public class TravelLog {
    public String id;
    public String title;
    public String description;
    public String destination;
    public long commentsCount;
    public long favoritesCount;
    public String defaultImageUrl;
    public String userId;
    public boolean favorited;
    public String date;
    public String dateCreated;
    public PrivacyEnum privacy;
    public boolean notify;
    public UserProfile userProfile;


    public boolean isComplete;

    public List<TravelLogEntry> travelLogEntries;


    public TravelLog() {
        this.isComplete = false;
    }

    /*
    public TravelLog(String title, String description, String destination, long commentsCount,
                     long favoritesCount, String defaultImageUrl, boolean favorited, List<TravelLogEntry> travelLogEntries,
                     String userId) {
        this.title = title;
        this.description = description;
        this.destination = destination;
        this.commentsCount = commentsCount;
        this.favoritesCount = favoritesCount;
        this.defaultImageUrl = defaultImageUrl;
        this.favorited = favorited;
        this.userId = userId;
        this.travelLogEntries = travelLogEntries;
    }
    */
    public String getId(){
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDestination() {
        return destination;
    }

    public long getCommentsCount() {
        return commentsCount;
    }

    public long getFavoritesCount() {
        return favoritesCount;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public boolean isFavorited() {
        return favorited;
    }

    public String getUserId() {
        return userId;
    }

    @Exclude
    public UserProfile getUserProfile(){ return userProfile; }

    public List<TravelLogEntry> getTravelLogEntries() {
        return travelLogEntries;
    }

    @Exclude
    public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<>();

        result.put("title", title);
        result.put("description", description);
        result.put("destination", destination);
        result.put("commentsCount", commentsCount);
        result.put("favoritesCount", 0);
        result.put("defaultImageUrl", defaultImageUrl);
        //result.put("travelLogEntries", travelLogEntries);
        result.put("userId", userId);
        result.put("date", date);
        result.put("dateCreated", dateCreated);
        result.put("privacy", privacy);
        result.put("notify", notify);
        result.put("isComplete", isComplete);
        return result;

    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
