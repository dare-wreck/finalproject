/**
 * This package contains the models of the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.models;