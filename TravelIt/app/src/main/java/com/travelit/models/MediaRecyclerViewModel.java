package com.travelit.models;

import org.parceler.Parcel;

/**
 * This is a view model class used for populating the media recycler view
 * @author Sai Muppa
 */
@Parcel(analyze={MediaRecyclerViewModel.class})
public class MediaRecyclerViewModel {

    public String url;

    public String title;

    public String description;

    public TravelLogEntry travelLogEntry;

    public MediaRecyclerViewModel() { }

    public MediaRecyclerViewModel(String url, String title, String description, TravelLogEntry travelLogEntry) {
        this.url = url;
        this.title = title;
        this.description = description;
        this.travelLogEntry = travelLogEntry;
    }
}
