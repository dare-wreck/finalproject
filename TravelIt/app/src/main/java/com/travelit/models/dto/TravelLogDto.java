package com.travelit.models.dto;

import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLogEntry;

import java.util.Map;

public class TravelLogDto {
    public String id;
    public String title;
    public String description;
    public String destination;
    public long commentsCount;
    //public Map<String, com.travelit.models.Comment> comments;
    public long favoritesCount;
    public Map<String, Boolean> favorites;
    public String defaultImageUrl;
    public String userId;
    public String date;
    public String dateCreated;
    public PrivacyEnum privacy;
    public boolean notify;
    public boolean isComplete;
    public boolean status;
    public Map<String, TravelLogEntry> travelLogEntries;

    public TravelLogDto() {

    }

    /*
    public TravelLogDto(String title, String description, String destination, int commentsCount, Map<String, com.travelit.models.Comment> comments, int favoritesCount,
                        String defaultImageUrl, Map<String, Boolean> favorites, Map<String, TravelLogEntry> travelLogEntries, String userId) {
        this.title = title;
        this.description = description;
        this.destination = destination;
        this.commentsCount = commentsCount;
        //this.comments = comments;
        this.favoritesCount = favoritesCount;
        this.favorites = favorites;
        this.defaultImageUrl = defaultImageUrl;
        this.userId = userId;
        this.travelLogEntries = travelLogEntries;
    }
    */
    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDestination() {
        return destination;
    }

    public long getCommentsCount() {
        return commentsCount;
    }

    public long getFavoritesCount() {
        return favoritesCount;
    }

    public Map<String, Boolean> getFavorites(){
        return favorites;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public Map<String, TravelLogEntry> getTravelLogEntries() {
        return travelLogEntries;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

