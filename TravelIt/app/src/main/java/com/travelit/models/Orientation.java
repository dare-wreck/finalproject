package com.travelit.models;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

public enum Orientation {
    VERTICAL,
    HORIZONTAL
}
