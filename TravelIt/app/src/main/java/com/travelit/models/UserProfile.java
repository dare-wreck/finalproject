package com.travelit.models;


import org.parceler.Parcel;

/**
 *
 * @author Sai Muppa
 */
@Parcel(analyze={UserProfile.class})
public class UserProfile {
    public String id;

    public String name;

    public String screenName;

    public String profileImageUrl;

    public String location;

    public String description;

    public int followersCount;

    public int friendsCount;

    public String createdAt;

    public int favouritesCount;

    public boolean verified;

    public String profileBannerImageUrl;

    public UserProfile() {

    }

    public UserProfile(String id, String name, String screenName, String profileImageUrl, String location,
                       String description, int followersCount, int friendsCount, String createdAt, int favouritesCount,
                       boolean verified, String profileBannerImageUrl) {
        this.id = id;
        this.name = name;
        this.screenName = screenName;
        this.profileImageUrl = profileImageUrl;
        this.location = location;
        this.description = description;
        this.followersCount = followersCount;
        this.friendsCount = friendsCount;
        this.createdAt = createdAt;
        this.favouritesCount = favouritesCount;
        this.verified = verified;
        this.profileBannerImageUrl = profileBannerImageUrl;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getFavouritesCount() {
        return favouritesCount;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getProfileBannerImageUrl() {
        return profileBannerImageUrl;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", screenName='" + screenName + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", followersCount=" + followersCount +
                ", friendsCount=" + friendsCount +
                ", createdAt='" + createdAt + '\'' +
                ", favouritesCount=" + favouritesCount +
                ", verified=" + verified +
                ", profileBannerImageUrl='" + profileBannerImageUrl + '\'' +
                '}';
    }

    public void setUserId(String userId) {
        this.id = userId;
    }
}
