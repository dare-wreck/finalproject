package com.travelit.models;

/**
 * Created by sm032858 on 4/24/17.
 */

public class LocationInfo {
    /**
     * Locality is mainly city such as San Francisco
     */
    public String locality;
    /**
     * Location such as Alcatraz, GoldenGate, etc
     */
    public String location;
    /**
     * San Francisco, CA
     */
    public String cityState;
    /**
     * USA or such
     */
    public String country;
}
