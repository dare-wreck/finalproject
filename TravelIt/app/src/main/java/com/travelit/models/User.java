package com.travelit.models;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sai Muppa
 */
@Parcel(analyze={User.class})
public class User {
    public UserProfile userProfile;

    public Map<String, Boolean> travelLogs;

    public Map<String, Boolean> followers;

    public Map<String, Boolean> following;

    public Map<String, TravelLogEntryAssociation> selectedTravelEntries;

    public User(){
        this.selectedTravelEntries = new HashMap<>();
        this.travelLogs = new HashMap<>();
        this.followers = new HashMap<>();
        this.following = new HashMap<>();
    }

    /*
    public User(UserProfile userProfile, Map<String, Boolean> travelLogs, Map<String, Boolean> followers, Map<String, Boolean> following) {
        this.userProfile = userProfile;
        this.travelLogs = travelLogs;
        this.followers = followers;
        this.following = following;
    }
    */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public Map<String, Boolean> getTravelLogs() {
        return travelLogs;
    }

    public Map<String, Boolean> getFollowers() {
        return followers;
    }

    public Map<String, Boolean> getFollowing() {
        return following;
    }

    public Map<String, TravelLogEntryAssociation> getSelectedTravelEntries() {
        return selectedTravelEntries;
    }
}
