package com.travelit.models;

/**
 *
 */
//this is a sample for using and understanding firebase
public class SampleItem {

    private String title;

    public SampleItem() {}

    public SampleItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
