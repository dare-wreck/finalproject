package com.travelit.models;

/**
 * Created by darewreck_PC on 4/24/2017.
 */

public enum PlaceCategory {
    UNKNOWN,
    NIGHT_LIFE,
    RESTURANTS,
    SIGHTS,
    ACTIVE
}
