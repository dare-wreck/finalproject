package com.travelit.models;

/**
 * Created by darewreck on 4/8/17.
 */

public enum PrivacyEnum {
    PUBLIC,
    PRIVATE
}
