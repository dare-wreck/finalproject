package com.travelit.models;

import android.net.Uri;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

@Parcel(analyze={TravelLatLong.class})
public class TravelLatLong {
    public double latitude;
    public double longitude;
    public String name;
    public String address;
    public String placeId;
    public float rating;
    public Uri webSiteUrl;
    public String id;
    public String phoneNumber;
    public List<Integer> placeTypes;

    public TravelLatLong(){

    }
    public TravelLatLong(String placeId, String name, String address, double latitude, double longitude) {
        this.placeId = placeId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.address = address;
    }

    public TravelLatLong(Place place) {
        this.name = place.getName().toString();
        this.rating = place.getRating();
        this.address = place.getAddress().toString();
        LatLng latLng = place.getLatLng();
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;

        this.webSiteUrl = place.getWebsiteUri();
        this.placeTypes = place.getPlaceTypes();
        this.phoneNumber = place.getPhoneNumber().toString();
        this.id = place.getId();
    }
}
