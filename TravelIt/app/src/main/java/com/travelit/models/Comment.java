package com.travelit.models;

import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

/**
 * @author Sai Muppa
 */
@IgnoreExtraProperties
@Parcel(analyze={Comment.class})
public class Comment {
    public String commentId;

    public String userId;

    public String userDisplayName;

    public String commentText;

    public Comment(){

    }
}
