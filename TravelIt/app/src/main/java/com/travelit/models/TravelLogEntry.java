package com.travelit.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.parceler.Parcel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@IgnoreExtraProperties
@Parcel(analyze={TravelLogEntry.class})
public class TravelLogEntry {
    public String id;
    public String title;
    public String description;
    public PrivacyEnum privacy;
    public boolean notify;
    public long favoritesCount;
    public long commentsCount;
    public boolean favorited;
    public List<Media> images;
    public List<Media> videos;
    public String travelLogId;
    public int status;
    public TravelLatLong travelLatLong;
    public String notes;

    public String date;
    public String dateCreated;

    public boolean checkedIn;

    public int categoryType;

    public String surpriseMsg;

    //USED by UI and set dynamically only.  Not used by DB
    public boolean isSelected;

    public TravelLogEntry() {
        this.checkedIn = false;
        this.isSelected = false;
    }
    /*
    public TravelLogEntry(String title, String description, String date, TravelStatus status, String locationName, double latitude, double longitude, String address) {
        super();
        this.description = description;
        this.date = date;
        this.status = status;
        this.travelLatLong = new TravelLatLong(locationName,address, latitude, longitude);
        this.title = title;
    }
    */
    public TravelLogEntry(String id, String title, String description, PrivacyEnum privacy, boolean notify, double latitude, double longitude,
                          long favoritesCount, long commentsCount, boolean favorited, List<Media> images, List<Media> videos,
                          String travelLogId) {
        super();
        this.id = id;
        this.title = title;
        this.description = description;
        this.privacy = privacy;
        this.notify = notify;
        this.favoritesCount = favoritesCount;
        this.commentsCount = commentsCount;
        this.favorited = favorited;
        this.images = images;
        this.videos = videos;
        this.travelLogId = travelLogId;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Exclude
    public Map<String,Object> toMap() {
        Map<String, Object> result = new HashMap<>();

        result.put("description", description);
        result.put("commentsCount", commentsCount);
        result.put("date", date);
        result.put("dateCreated", dateCreated);
        result.put("favoritesCount", 0);
        result.put("images", images);
        result.put("notes", notes);
        result.put("notify", notify);
        result.put("privacy", privacy);
        result.put("title", title);
        result.put("travelLatLong", travelLatLong);
        result.put("checkedIn", checkedIn);
        result.put("status", status);

        return result;
    }
}
