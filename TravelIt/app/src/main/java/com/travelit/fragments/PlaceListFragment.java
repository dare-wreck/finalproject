package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.TravelLogEntryRecycleAdapter;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.TravelLogEntryAssociation;
import com.travelit.models.User;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceListFragment extends Fragment {
    private static final String TAG = TripListFragment.class.getName();

    protected TravelLogEntryRecycleAdapter mAdapter;
    protected List<TravelLogEntry> mTravelLogEntries;
    protected FirebaseApiHelper mFbApiHelper;
    protected String mTravelLogId;
    private Context mContext;

    @BindView(R.id.rvTravelLogs)
    protected RecyclerView rvTravelLogs;

    /*****************************************************************
     * Interfaces
     *****************************************************************/
    private PlaceListFragment.OnFragmentListener mListener;

    public PlaceListFragment() {
        mTravelLogEntries = new ArrayList<>();
    }

    public static PlaceListFragment newInstance(String travelLogId) {
        PlaceListFragment fragment = new PlaceListFragment();
        Bundle args = new Bundle();

        args.putString("travelLogId", travelLogId);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mFbApiHelper = FirebaseApiHelper.getInstance();
            mTravelLogId = getArguments().getString("travelLogId");
            mContext = getContext();
            populateTravelLogEntries(mTravelLogId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlaceListFragment.OnFragmentListener) {
            mListener = (PlaceListFragment.OnFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement TravelLogListFragment.TravelLogListFragmentListener");
        }
    }

    public interface OnFragmentListener {
        void onTravelLogEntryClicked(TravelLogEntry entry);
    }

    /*****************************************************************
     * Interfaces to implement
     *****************************************************************/
    protected void populateTravelLogEntries(String travelLogId) {
        final String loggedInUserId = mFbApiHelper.getFirebaseUser().getUid();
        mFbApiHelper.getTravelLogEntriesByTravelLog(mTravelLogId, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLogEntry>>() {
            @Override
            public void callback(List<TravelLogEntry> entries) {
                mAdapter.clear();
                mFbApiHelper.getUser(loggedInUserId, new FirebaseApiHelper.FirebaseRetrieverCallback<User>() {
                    @Override
                    public void callback(User user) {
                        Map<String, TravelLogEntryAssociation> selectedUsers  = user.selectedTravelEntries;

                        for(TravelLogEntry entry:entries) {
                            if(selectedUsers.containsKey(entry.id)) {
                                entry.isSelected = true;
                            } else {
                                entry.isSelected = false;
                            }

                            mAdapter.add(entry);
                        }
                    }
                });
            }
        });
    }

    /********************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_travel_list, container, false);
        ButterKnife.bind(this, view);

        if (savedInstanceState == null) {
            mAdapter = new TravelLogEntryRecycleAdapter(getContext(), mTravelLogEntries, true);
            rvTravelLogs.setAdapter(mAdapter);
            //gridLayoutManager = new GridLayoutManager(getActivity(), 3);
            LinearLayoutManager linerLayoutManager = new LinearLayoutManager(getContext());

            DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvTravelLogs.getContext(), linerLayoutManager.getOrientation());
            rvTravelLogs.addItemDecoration(mDividerItemDecoration);
            rvTravelLogs.setLayoutManager(linerLayoutManager);
            rvTravelLogs.setHasFixedSize(true);
            ItemClickSupport.addTo(rvTravelLogs).setOnItemClickListener(
                    new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            TravelLogEntry log = mTravelLogEntries.get(position);
                            mListener.onTravelLogEntryClicked(log);
                        }
                    }
            );
        }
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            case android.R.id.home: {
                if(getChildFragmentManager().getBackStackEntryCount() >= 1) {
                    getChildFragmentManager().popBackStack();
                } else {
                    getActivity().finish();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}