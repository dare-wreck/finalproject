package com.travelit.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.activities.AddTravelLogActivity;
import com.travelit.activities.AddTravelLogEntryActivity;
import com.travelit.adapters.IconTabsAdapter;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.User;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.views.DynamicHeightImageView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class HomeFragment extends Fragment {

    @BindView(R.id.vpTravelLogEntries)
    ViewPager mVpTravelLogEntries;

    @BindView(R.id.tlTravelLogEntryView)
    TabLayout mTlTravelLogEntryView;

    @BindView(R.id.tvTripName)
    TextView mTvTripName;

    @BindView(R.id.tvTripDescription)
    TextView mTvTripDescription;

    @BindView(R.id.ivProfilePicture)
    DynamicHeightImageView mIvProfilePicture;

    @BindView(R.id.ivTravelPic)
    ImageView mIvTravelPic;

    @BindView(R.id.tvUserName)
    TextView mTvUserName;

    @BindView(R.id.ivVerified)
    ImageView mIvVerified;

    @BindView(R.id.tvScreenName)
    TextView mTvScreenName;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    TravelLog mTravelLog;

    UserProfile mUserProfile;

    IconTabsAdapter mTravelLogEntryTabsAdapter;
    private List<Fragment> mFragments = new ArrayList<>();


    private FirebaseApiHelper mFbApiHelper;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(TravelLog travelLog, UserProfile userProfile) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putParcelable("travelLog", Parcels.wrap(travelLog));
        args.putParcelable("userProfile", Parcels.wrap(userProfile));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFbApiHelper = FirebaseApiHelper.getInstance();

        if (getArguments() != null) {
            mTravelLog = Parcels.unwrap(getArguments().getParcelable("travelLog"));
            mUserProfile = Parcels.unwrap(getArguments().getParcelable("userProfile"));
        }
    }
    /*
    @OnClick(R.id.fab_add_travel_log_menu_item)
    public void onFabAddTravelLogMenuClicked() {
        Intent intent = new Intent(getActivity(), AddTravelLogActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.fab_add_travel_log_entry_menu_item)
    public void onFabAddTravelLogEntryClicked() {
        Intent intent = new Intent(getActivity(), AddTravelLogEntryActivity.class);
        startActivity(intent);
    }
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);


        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(mToolbar);

        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        prepareFragments();
        bindTabsToFragments();
        setTabIcons();
        populateTravelLogProfile();

        return view;
    }

    private void bindTabsToFragments() {
        mTravelLogEntryTabsAdapter = new IconTabsAdapter(getChildFragmentManager(), mFragments);

        //Bind the Adapter to the View Pager
        mVpTravelLogEntries.setAdapter(mTravelLogEntryTabsAdapter);

        final int width = mVpTravelLogEntries.getMeasuredWidth();
        final int height = mVpTravelLogEntries.getMeasuredHeight();

        //Link View Pager and Tab Layout
        mTlTravelLogEntryView.setupWithViewPager(mVpTravelLogEntries);
        mTlTravelLogEntryView.setTabGravity(TabLayout.GRAVITY_CENTER);
        mTlTravelLogEntryView.setTabMode(TabLayout.MODE_FIXED);
    }

    private void setTabIcons() {
        if(mTlTravelLogEntryView != null) {
         //   mTlTravelLogEntryView.getTabAt(0).setIcon(R.drawable.ic_view_list);
            mTlTravelLogEntryView.getTabAt(0).setIcon(R.drawable.ic_timeline);
            mTlTravelLogEntryView.getTabAt(1).setIcon(R.drawable.ic_maps);
            mTlTravelLogEntryView.getTabAt(2).setIcon(R.drawable.ic_view_grid);
        }
    }

    private void prepareFragments() {

        List<TravelLogEntry> travelLogEntries = new ArrayList<TravelLogEntry>();
        if(mTravelLog != null && mTravelLog.getTravelLogEntries() != null) {
            travelLogEntries.addAll(mTravelLog.getTravelLogEntries());
        }

        mFragments.clear();

        //MapTimelineFragment mapTimelineFragment = MapTimelineFragment.newInstance(mTravelLog, false, true, false);
       // PlaceListFragment placeListFragment = PlaceListFragment.newInstance(mTravelLog.id);
        //mFragments.add(placeListFragment);

        TimelineFragment timeLineFragment = TimelineFragment.newInstance(mTravelLog.travelLogEntries, false, true, mTravelLog.isComplete);
        mFragments.add(timeLineFragment);

        MapFragment mapFragment = MapFragment.newInstance(mTravelLog.travelLogEntries);
        mFragments.add(mapFragment);

        //mFragments.add(TravelLogEntriesFragment.newInstance(travelLogEntries, false));
        mFragments.add(TravelLogEntriesMediaFragment.newInstance(mTravelLog.id));
        //mFragments.add(TravelLogEntriesFragment.newInstance(travelLogEntries, true));
    }

    private void populateTravelLogProfile() {
        if(mTravelLog != null) {
            Picasso.with(getActivity())
                    .load(mTravelLog.userProfile.getProfileImageUrl())
                    .transform(new RoundedCornersTransformation(2,2))
                    .resize(48, 48)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(mIvProfilePicture);

            Picasso.with(getActivity())
                    .load(mTravelLog.getDefaultImageUrl())
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(mIvTravelPic);

            mTvTripName.setText(mTravelLog.getTitle());
            mTvTripDescription.setText(mTravelLog.getDescription());

            mTvUserName.setText(mTravelLog.userProfile.getName());
            mTvScreenName.setText("@"+mTravelLog.userProfile.getScreenName());

            if(new UserProfile().isVerified()){
                mIvVerified.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            case android.R.id.home: {
                if(getChildFragmentManager().getBackStackEntryCount() >= 1) {
                    getChildFragmentManager().popBackStack();
                } else {
                    getActivity().finish();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
