package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.TravelLogRecycleAdapter;
import com.travelit.models.TravelLog;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.ItemClickSupport;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
public abstract class TripListFragment extends Fragment {
    private static final String TAG = TripListFragment.class.getName();

    protected TravelLogRecycleAdapter mAdapter;
    protected List<TravelLog> mTravelLog;
    protected FirebaseApiHelper mFbApiHelper;
    protected UserProfile mUserProfile;
    private Context mContext;

    @BindView(R.id.rvTravelLogs)
    protected RecyclerView rvTravelLogs;

    /*****************************************************************
     * Interfaces
     *****************************************************************/
    private TripListFragmentListener mListener;


    public TripListFragment(){
        this.mTravelLog = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mFbApiHelper = FirebaseApiHelper.getInstance();
            mUserProfile = Parcels.unwrap(getArguments().getParcelable("userProfile"));
            mContext = getContext();
            this.populateTravelLogs(mUserProfile);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TripListFragment.TripListFragmentListener) {
            mListener = (TripListFragment.TripListFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement TravelLogListFragment.TravelLogListFragmentListener");
        }
    }

    public interface TripListFragmentListener {
        void onTravelLogClicked(TravelLog entry);
    }

    /*****************************************************************
     * Interfaces to implement
     *****************************************************************/
    protected abstract void populateTravelLogs(UserProfile userProfile);

    /********************************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_travel_list, container, false);
        ButterKnife.bind(this, view);

        if (savedInstanceState == null) {
            mAdapter = new TravelLogRecycleAdapter(getContext(), mTravelLog);
            rvTravelLogs.setAdapter(mAdapter);
            //gridLayoutManager = new GridLayoutManager(getActivity(), 3);
            LinearLayoutManager linerLayoutManager = new LinearLayoutManager(getContext());

            DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvTravelLogs.getContext(), linerLayoutManager.getOrientation());
            rvTravelLogs.addItemDecoration(mDividerItemDecoration);
            rvTravelLogs.setLayoutManager(linerLayoutManager);
            rvTravelLogs.setHasFixedSize(true);
            ItemClickSupport.addTo(rvTravelLogs).setOnItemClickListener(
                    new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            TravelLog log = mTravelLog.get(position);
                            mListener.onTravelLogClicked(log);
                        }
                    }
            );
        }
        return view;
    }

    public void clearItems() {
        mTravelLog.clear();
        mAdapter.notifyDataSetChanged();
    }

    public void addItem(TravelLog travelLog){
        mTravelLog.add(0, travelLog);
        mAdapter.notifyItemChanged(0);
    }

}
