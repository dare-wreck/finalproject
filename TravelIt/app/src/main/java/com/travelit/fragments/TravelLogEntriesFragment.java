package com.travelit.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.TravelLogEntryAdapter;
import com.travelit.eventlisteners.EndlessRecyclerViewScrollListener;
import com.travelit.models.TravelLogEntry;
import com.travelit.util.LogUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Sai Muppa
 */
public class TravelLogEntriesFragment extends TravelLogEntriesBaseFragment {
    static final String ARG_FAVORITES_ONLY = "favoritesOnly";
    private static final String ARG_LOG_ENTRIES = "logEntries";

    @BindView(R.id.rvTravelLogEntry)
    RecyclerView mRvTravelLogEntry;

    TravelLogEntryAdapter mTravelLogEntryRecyclerViewAdapter;
    EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;
    boolean mFavoritesOnly;
    LinearLayoutManager mLinearLayoutManager;

    /**
     *
     * @return
     */
    public static TravelLogEntriesFragment newInstance(List<TravelLogEntry> travelLogEntries, boolean favoritesOnly){
        TravelLogEntriesFragment travelLogEntriesFragment = new TravelLogEntriesFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_LOG_ENTRIES, Parcels.wrap(travelLogEntries));

        bundle.putBoolean(ARG_FAVORITES_ONLY, favoritesOnly);
        travelLogEntriesFragment.setArguments(bundle);
        return travelLogEntriesFragment;
    }

    public TravelLogEntriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTraveLogEntries = Parcels.unwrap(getArguments().getParcelable(ARG_LOG_ENTRIES));
        mFavoritesOnly = getArguments().getBoolean(ARG_FAVORITES_ONLY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_travel_log_entries, container, false);
        mCurrentView = view;

        super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }

    /**
     * Initializes the travel log recycler view
     */
    @Override
    void setTravelLogEntriesRecyclerView() {
        if(mTraveLogEntries == null) {
            mTraveLogEntries = new ArrayList<>();
        }

        // Create adapter passing in the initial travel log data
        mTravelLogEntryRecyclerViewAdapter = new TravelLogEntryAdapter(getActivity(), mTraveLogEntries);

        // Attach the adapter to the recyclerview to populate items
        mRvTravelLogEntry.setAdapter(mTravelLogEntryRecyclerViewAdapter);

        mLinearLayoutManager =
                new LinearLayoutManager(getActivity());

        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        // Set layout manager to position the items
        mRvTravelLogEntry.setLayoutManager(mLinearLayoutManager);

        // Retain an instance so that you can call `resetState()` for fresh searches
        mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
            }
        };

        // Adds the scroll listener to RecyclerView
        mRvTravelLogEntry.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    }

    @Override
    void populateTravelLogEntries(){

        //TODO: depending whether mFavoritesOnly is true or not make different queries to get travel entries

        //TODO: replace the following logic with Firebase data pull
        //List<TravelLogEntry> travelLogEntries = TestServiceUtil.getTravelLogEntries();

        //if (travelLogEntries != null) {
        //    int currSize = mTravelLogEntryRecyclerViewAdapter.getItemCount();
        //    mTraveLogEntries.addAll(travelLogEntries);
        //    mTravelLogEntryRecyclerViewAdapter.notifyItemRangeInserted(currSize, mTraveLogEntries.size() - 1);
        //}

        LogUtil.logD(TAG, mTraveLogEntries.toString());
    }

    void appendTravelLogEntries(List<TravelLogEntry> travelLogEntries) {
        mTraveLogEntries.addAll(0, travelLogEntries);
        mTravelLogEntryRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
        mLinearLayoutManager.scrollToPosition(0);
    }
}
