package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.travelit.R;
import com.travelit.models.TravelLogEntry;
import com.travelit.networks.FirebaseApiHelper;

import org.parceler.Parcels;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapFragment extends Fragment implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    private GoogleMapOptions options;
    private SupportMapFragment mapFragment;
    private List<TravelLogEntry> mTravelLogEntries;
    private static final String TAG = MapFragment.class.getSimpleName();
    @BindView(R.id.flContainer)
    FrameLayout flContainer;

    private FirebaseApiHelper mFireBaseApiHelper;

    private MapFragment.OnFragmentListener mListener;
    public interface OnFragmentListener {
        void onMarkerClick(TravelLogEntry entry) throws Exception;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MapFragment.OnFragmentListener) {
            mListener = (MapFragment.OnFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MapFragment.OnFragmentListener");
        }
    }

    public MapFragment() {
        mTravelLogEntries = new ArrayList<>();
    }

    public static MapFragment newInstance(final List<TravelLogEntry> mTravelLogEntry) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putParcelable("travelLogEntries", Parcels.wrap(mTravelLogEntry));
        fragment.setArguments(args);
        return fragment;
    }

       @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFireBaseApiHelper = FirebaseApiHelper.getInstance();

        if (TextUtils.isEmpty(getResources().getString(R.string.google_maps_api_key))) {
            throw new IllegalStateException("You forgot to supply a Google Maps API key");
        }
        if (getArguments() != null) {
            mTravelLogEntries = (List<TravelLogEntry>) Parcels.unwrap(getArguments().getParcelable("travelLogEntries"));
        }

        options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL);
        options.compassEnabled(false);

        mapFragment = SupportMapFragment.newInstance(options);
        mapFragment.getMapAsync(this);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            Toast.makeText(getContext(), "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.flContainer, mapFragment);
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        // Retrieve the data from the marker.
        Integer markerIndex = (Integer) marker.getTag();
        if (markerIndex != null) {
            if(getParentFragment() instanceof  MapTimelineFragment) {
                MapTimelineFragment mapTimeLineFragment = (MapTimelineFragment) getParentFragment();
                mapTimeLineFragment.onMarkerClick(markerIndex);
            } else {
                TravelLogEntry entry = mTravelLogEntries.get(markerIndex);
                try {
                    mListener.onMarkerClick(entry);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        List<MarkerOptions> markers = new ArrayList<MarkerOptions>();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<LatLng> latLongList = new ArrayList<>();

        boolean success = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.mapa_sara_2_style));
        if(success) {
            Log.e(TAG, "Issue with loading map style.");
        }
        if(mTravelLogEntries.size() > 0) {
            for(int i = 0; i <  mTravelLogEntries.size(); i++) {
                TravelLogEntry entry = mTravelLogEntries.get(i);
                LatLng latLng = new LatLng(entry.travelLatLong.latitude, entry.travelLatLong.longitude);
                MarkerOptions marker = new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                        .title(entry.title);
                markers.add(marker);
                map.addMarker(marker).setTag(i);
                latLongList.add(latLng);
                builder.include(marker.getPosition());
            }

            final LatLngBounds bounds = builder.build();
            Polyline polyline = map.addPolyline(new PolylineOptions()
                                                    .clickable(true)
                                                    .color(R.color.colorAccent)
                                                    .width(5)
                                                    .addAll(latLongList));

            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    final int width = flContainer.getMeasuredWidth();
                    final int height = flContainer.getMeasuredHeight();
                    map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, 100));
                    map.getUiSettings().setZoomControlsEnabled(false);
                    map.getUiSettings().setZoomGesturesEnabled(false);
                    map.getUiSettings().setScrollGesturesEnabled(false);
                    map.getUiSettings().setAllGesturesEnabled(false);
                }
            });

            map.setOnMarkerClickListener(this);
        }
    }
}