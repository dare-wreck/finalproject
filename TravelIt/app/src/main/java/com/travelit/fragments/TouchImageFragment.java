package com.travelit.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.Media;
import com.travelit.util.TouchImageView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Sai Muppa
 */
public class TouchImageFragment extends Fragment {

    public static final String ARG_MEDIA = "mediaParam";

    private Media mMedia;
    private ShareActionProvider miShareAction;
    private Uri mLocalImageUri;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.ivImageResult)
    TouchImageView mIvImageResult;


    public TouchImageFragment(){
        // Required empty public constructor
    }

    public static TouchImageFragment newInstance(Media media) {
        TouchImageFragment fragment = new TouchImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MEDIA, Parcels.wrap(media));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMedia = (Media) Parcels.unwrap(getArguments().getParcelable(ARG_MEDIA));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_touch_image, container, false);
        ButterKnife.bind(this,view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (mMedia!=null && mMedia.url != null)
            Picasso.with(view.getContext())
                    .load(mMedia.url )
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(mIvImageResult);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
