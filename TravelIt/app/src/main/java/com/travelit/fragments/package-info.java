/**
 * This package contains the fragments of the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.fragments;