package com.travelit.fragments;

import android.os.Bundle;
import android.os.Parcelable;

import com.travelit.models.TravelLog;
import com.travelit.models.User;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class PastTripListFragment extends TripListFragment {
    private static final String TAG = PastTripListFragment.class.getName();

    public PastTripListFragment() {

    }
    public static PastTripListFragment newInstance(UserProfile userProfile) {
        PastTripListFragment fragment = new PastTripListFragment();
        Bundle args = new Bundle();

        Parcelable listParcelable = Parcels.wrap(userProfile);
        args.putParcelable("userProfile", listParcelable);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected void populateTravelLogs(UserProfile userProfile) {
        mFbApiHelper.getTravelLogsByUser(userProfile.id, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
            @Override
            public void callback(List<TravelLog> travelLogs) {
                clearItems();
                for(TravelLog travelLog:travelLogs) {
                    if(travelLog.isComplete){
                        addItem(travelLog);
                    }
                }
            }
        });
    }
}
