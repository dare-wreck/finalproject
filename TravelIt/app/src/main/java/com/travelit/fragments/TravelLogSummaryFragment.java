package com.travelit.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.models.UserProfile;

import org.parceler.Parcels;

/**
 * A simple {@link TravelLogSummaryBaseFragment} subclass.
 */
public class TravelLogSummaryFragment extends TravelLogSummaryBaseFragment {
    /**
     *
     * @return
     */
    public static TravelLogSummaryFragment newInstance(UserProfile currentUserProfile){
        TravelLogSummaryFragment travelLogFragment = new TravelLogSummaryFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_CURRENT_USER, Parcels.wrap(currentUserProfile));

        travelLogFragment.setArguments(bundle);
        return travelLogFragment;
    }

    public TravelLogSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentUserProfile = Parcels.unwrap(getArguments().getParcelable(ARG_CURRENT_USER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_travel_log_summary, container, false);
        mCurrentView = view;

        super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }
}
