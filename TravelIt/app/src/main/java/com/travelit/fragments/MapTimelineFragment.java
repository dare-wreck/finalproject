package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.networks.FirebaseApiHelper;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapTimelineFragment extends Fragment {
    private static final String TAG = MapTimelineFragment.class.getName();
    private TimelineFragment timeLineFragment;
    private MapFragment mapFragment;
    private TravelLog mTravelLog;
    private boolean mEnableStartAdventure;
    private boolean mEnableFavoriteLocation;
    private boolean mEnableToolbar;
    private FirebaseApiHelper mFireBaseApiHelper;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MapTimelineFragment.MapTimeLineFragmentListener mListener;

    public MapTimelineFragment() {
    }

    public static MapTimelineFragment newInstance(TravelLog travelLog, boolean enableStartAdventure, boolean enableFavoriteLocation, boolean enableToolbar) {
        MapTimelineFragment fragment = new MapTimelineFragment();
        Bundle args = new Bundle();
        args.putParcelable("travelLog", Parcels.wrap(travelLog));
        args.putBoolean("enableStartAdventure", enableStartAdventure);
        args.putBoolean("enableFavoriteLocation", enableFavoriteLocation);
        args.putBoolean("enableToolbar", enableToolbar);
        fragment.setArguments(args);
        return fragment;
    }

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MapTimelineFragment.MapTimeLineFragmentListener) {
            mListener = (MapTimelineFragment.MapTimeLineFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MapTimelineFragment.OnItemSelectedListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            mFireBaseApiHelper = FirebaseApiHelper.getInstance();
        }
    }

    public interface MapTimeLineFragmentListener {
        void onStartAdventureClick(TravelLog entry) throws Exception;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            mTravelLog = (TravelLog) Parcels.unwrap(getArguments().getParcelable("travelLog"));
            mEnableStartAdventure = getArguments().getBoolean("enableStartAdventure");
            mEnableFavoriteLocation = getArguments().getBoolean("enableFavoriteLocation");
            mEnableToolbar = getArguments().getBoolean("enableToolbar");
        }

        View view = inflater.inflate(R.layout.fragment_map_timeline, container, false);
        ButterKnife.bind(this, view);

        if(mEnableToolbar) {
            final AppCompatActivity _activity = (AppCompatActivity) getActivity();
            _activity.setSupportActionBar(toolbar);
            final ActionBar actionBar = _activity.getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            setHasOptionsMenu(true);
        } else {
            toolbar.setVisibility(View.GONE);
        }
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        if(mFireBaseApiHelper == null){
            mFireBaseApiHelper = FirebaseApiHelper.getInstance();
        }

        mFireBaseApiHelper.getTravelLogEntriesByTravelLog(mTravelLog.id, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLogEntry>>() {
            @Override
            public void callback(List<TravelLogEntry> entries) {
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();


                mapFragment = MapFragment.newInstance(entries);
                ft.replace(R.id.flContainer1, mapFragment);

                timeLineFragment = TimelineFragment.newInstance(entries, false, mEnableFavoriteLocation, mTravelLog.isComplete);
                ft.replace(R.id.flContainer2, timeLineFragment);

                ft.commit();
            }
        });
    }

    public void onMarkerClick(int markerIndex) {
        timeLineFragment.scrollToPosition(markerIndex);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(mEnableStartAdventure) {
            getActivity().getMenuInflater().inflate(R.menu.travellog_menu, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_start_adventrue: {
                if(mEnableStartAdventure) {
                    try {
                        mListener.onStartAdventureClick(mTravelLog);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d(TAG,"Start Adventure should not be enabled.");
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
