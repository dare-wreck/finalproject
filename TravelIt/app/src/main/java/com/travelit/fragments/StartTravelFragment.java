package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.StartTravelFragmentPagerAdapter;
import com.travelit.models.TravelLog;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartTravelFragment extends Fragment {
    private final String TAG = StartTravelFragment.class.getName();
    private static final String ARG_TRAVEL_LOG_ENTRY= "travelLogEntryParam";
    private TravelLog mTravelLog;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.viewpager)
    ViewPager viewpager;


    public StartTravelFragment() {
        // Required empty public constructor
    }

    public static StartTravelFragment newInstance(TravelLog travelLog) {
        StartTravelFragment fragment = new StartTravelFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TRAVEL_LOG_ENTRY, Parcels.wrap(travelLog));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTravelLog = (TravelLog) Parcels.unwrap(getArguments().getParcelable(ARG_TRAVEL_LOG_ENTRY));
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_start_travel, container, false);
        ButterKnife.bind(this,view);

        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        StartTravelFragmentPagerAdapter customPageAdapter = new StartTravelFragmentPagerAdapter(getChildFragmentManager(), mTravelLog);
        viewpager.setAdapter(customPageAdapter);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(12);
        // Attach the page change listener inside the activity
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                mListener.onViewPagerScroll(position);
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StartTravelFragment.OnFragmentInteractionListener) {
            mListener = (StartTravelFragment.OnFragmentInteractionListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement StartTravelLogEntryDetailFragment.OnFragmentInteractionListener");
        }
    }

    public void scrollToPage(int position)
    {
        viewpager.setCurrentItem(position);
    }
    public interface OnFragmentInteractionListener {
        void onViewPagerScroll(int position);
    }

}
