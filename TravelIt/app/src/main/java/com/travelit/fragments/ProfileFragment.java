package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.adapters.TripCategoryPagerAdapter;
import com.travelit.models.TravelLog;
import com.travelit.models.UserProfile;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class ProfileFragment  extends Fragment {
    private UserProfile mUserProfile;
    private Context mContext;
    private static final String TAG = ProfileFragment.class.getName();
    private static final int PICK_IMAGE_ID = 234; // the number doesn't matter

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.psTripCategory)
    PagerSlidingTabStrip tabsStrip;

    @BindView(R.id.vpTripCategory)
    ViewPager vpPager;

    @BindView(R.id.ivProfilePic)
    ImageView ivProfilePic;

    @BindView(R.id.tvFollowersCount)
    TextView tvFollowersCount;

    @BindView(R.id.tvFolloweringCount)
    TextView tvFolloweringCount;

    @BindView(R.id.tvDestinationCount)
    TextView tvDestinationCount;

    @BindView(R.id.etUserName)
    TextView tvUserName;

    @BindView(R.id.etDescription)
    TextView tvDescription;

    @BindView(R.id.btEdit)
    Button mBtnEdit;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvUserName;

        @BindView(R.id.ivProfilePic)
        CircleImageView ivProfilePic;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private ProfileFragment.ProfileFragmentListener mListener;

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileFragment.ProfileFragmentListener) {
            mListener = (ProfileFragment.ProfileFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ProfileFragment.ProfileFragmentListener");
        }
    }

    public interface ProfileFragmentListener {
        void onTravelLogClicked(TravelLog entry);
        void onEditUserProfile(UserProfile userProfile);
    }

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(UserProfile userProfile) {

        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();

        Parcelable userProfileParcel = Parcels.wrap(userProfile);
        args.putParcelable("userProfile", userProfileParcel);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mUserProfile = Parcels.unwrap(getArguments().getParcelable("userProfile"));
            mContext = getContext();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(toolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        TripCategoryPagerAdapter pageAdapter = new TripCategoryPagerAdapter(mUserProfile, getChildFragmentManager(),getContext());
        vpPager.setAdapter(pageAdapter);
        tabsStrip.setViewPager(vpPager);

        //Update view data
        tvUserName.setText(mUserProfile.name);
        tvFolloweringCount.setText(String.valueOf(mUserProfile.followersCount));
        tvFollowersCount.setText(String.valueOf(mUserProfile.followersCount));
        tvDescription.setText(mUserProfile.description);
        tvDestinationCount.setText("3");

        if(mUserProfile.getProfileImageUrl() != null){
            Picasso.with(getContext())
                    .load(mUserProfile.getProfileImageUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(ivProfilePic);
        }


        return view;
    }

    @OnClick({R.id.btEdit, R.id.ivProfilePic})
    void OnClick(View view) {
        mListener.onEditUserProfile(mUserProfile);
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home: {
                getFragmentManager().popBackStack();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    */
}