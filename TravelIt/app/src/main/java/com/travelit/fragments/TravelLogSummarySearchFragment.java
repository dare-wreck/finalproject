package com.travelit.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.models.UserProfile;

import org.parceler.Parcels;

/**
 * @author Sai Muppa
 */
public class TravelLogSummarySearchFragment extends TravelLogSummaryBaseFragment {
    private static final String ARG_LOCATION_NAME = "location_name";

    /**
     *
     * @return
     */
    public static TravelLogSummarySearchFragment newInstance(UserProfile currentUserProfile, String locationName){
        TravelLogSummarySearchFragment travelLogSummarySearchFragment = new TravelLogSummarySearchFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_CURRENT_USER, Parcels.wrap(currentUserProfile));
        bundle.putString(ARG_LOCATION_NAME, locationName);

        travelLogSummarySearchFragment.setArguments(bundle);
        return travelLogSummarySearchFragment;
    }

    public TravelLogSummarySearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentUserProfile = Parcels.unwrap(getArguments().getParcelable(ARG_CURRENT_USER));
        mSearchQuery = getArguments().getString(ARG_LOCATION_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_travel_log_summary, container, false);
        mCurrentView = view;

        super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }
}
