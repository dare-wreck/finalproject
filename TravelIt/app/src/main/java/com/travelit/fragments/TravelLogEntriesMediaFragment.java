package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.MediaRecyclerAdapter;
import com.travelit.eventlisteners.EndlessRecyclerViewScrollListener;
import com.travelit.models.LayoutType;
import com.travelit.models.Media;
import com.travelit.models.MediaRecyclerViewModel;
import com.travelit.models.TravelLogEntry;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.ItemClickSupport;
import com.travelit.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Sai Muppa
 */
public class TravelLogEntriesMediaFragment extends TravelLogEntriesBaseFragment {

    @BindView(R.id.rvTravelMedia)
    RecyclerView mRvTravelMedia;

    List<MediaRecyclerViewModel> mMedias;

    MediaRecyclerAdapter mMediaRecyclerAdapter;
    EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;

    StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private FirebaseApiHelper mFbApiHelper;

    private String mTravelLogId;

    private TravelLogEntriesMediaFragment.OnFragmentListener mListener;

    public interface OnFragmentListener {
        void onMediaClicked(TravelLogEntry travelLogEntry);
    }

    /**
     *
     * @return
     */
    public static TravelLogEntriesMediaFragment newInstance(String travelLogId){
        TravelLogEntriesMediaFragment travelLogEntriesMediaFragment = new TravelLogEntriesMediaFragment();

        Bundle bundle = new Bundle();
        bundle.putString("travelLogId", travelLogId);

        travelLogEntriesMediaFragment.setArguments(bundle);
        return travelLogEntriesMediaFragment;
    }

    public TravelLogEntriesMediaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFbApiHelper = FirebaseApiHelper.getInstance();
        mTravelLogId = getArguments().getString("travelLogId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.item_travel_log_entry_media, container, false);
        mCurrentView = view;

        super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlaceListFragment.OnFragmentListener) {
            mListener = (TravelLogEntriesMediaFragment.OnFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement TravelLogEntriesMediaFragment.OnFragmentListener");
        }
    }

    /**
     * Initializes the travel log recycler view
     */
    @Override
    void setTravelLogEntriesRecyclerView() {
        mTraveLogEntries = new ArrayList<>();

        mMedias = new ArrayList<>();

        // Create adapter passing in the initial travel log data
        mMediaRecyclerAdapter = new MediaRecyclerAdapter(getActivity(), LayoutType.STAGGERED_GRID, mMedias);

        // Attach the adapter to the recyclerview to populate items
        mRvTravelMedia.setAdapter(mMediaRecyclerAdapter);

        mStaggeredGridLayoutManager =
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);

        // Set layout manager to position the items
        mRvTravelMedia.setLayoutManager(mStaggeredGridLayoutManager);

        mRvTravelMedia.setHasFixedSize(true);

        ItemClickSupport.addTo(mRvTravelMedia).setOnItemClickListener(
            new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    MediaRecyclerViewModel media = mMedias.get(position);
                    mListener.onMediaClicked(media.travelLogEntry);
                }
            }
        );

        // Retain an instance so that you can call `resetState()` for fresh searches
        mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mStaggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
            }
        };

        // Adds the scroll listener to RecyclerView
        //mRvTravelMedia.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    }

    @Override
    void populateTravelLogEntries(){

        //ist<TravelLogEntry> travelLogEntries = TestServiceUtil.getTravelLogEntries();
        mFbApiHelper.getTravelLogEntriesByTravelLog(mTravelLogId, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLogEntry>>() {
            @Override
            public void callback(List<TravelLogEntry> entries) {
                mTraveLogEntries.clear();
                mMediaRecyclerAdapter.notifyDataSetChanged();

                MediaRecyclerViewModel mediaRecyclerViewModel;
                for(TravelLogEntry entry:entries){
                    if(entry!= null && entry.images != null){
                        for(Media media: entry.images) {
                            mediaRecyclerViewModel = new MediaRecyclerViewModel(media.url, media.title, media.description, entry);
                            mMedias.add(mediaRecyclerViewModel);
                            mMediaRecyclerAdapter.notifyItemInserted(0);
                        }
                    }
                }
            }
        });

        LogUtil.logD(TAG, mTraveLogEntries.toString());
    }
}
