package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.UserProfile;
import com.travelit.views.DynamicHeightImageView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * @author Sai Muppa
 */
public class ProfileEditFragment extends Fragment {
    private UserProfile mUserProfile;
    private Context mContext;
    private static final String TAG = ProfileFragment.class.getName();
    private static final int PICK_IMAGE_ID = 234; // the number doesn't matter

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.ivProfilePicture)
    DynamicHeightImageView mIvProfilePicture;

    @BindView(R.id.ivAddProfileUrl)
    ImageView mIvAddProfileUrl;

    @BindView(R.id.ivDefaultPicture)
    ImageView mIvDefaultPicture;

    @BindView(R.id.ivAddDefaultUrl)
    ImageView mIvAddDefaultUrl;

    @BindView(R.id.etName)
    TextView mEtName;

    @BindView(R.id.etScreenName)
    TextView mEtScreenName;

    @BindView(R.id.etBio)
    TextView mEtBio;

    @BindView(R.id.btLocation)
    Button mBtLocation;

    public interface ProfileEditFragmentListener {
        void onLocationClicked();
    }


    private ProfileEditFragment.ProfileEditFragmentListener mListener;

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileFragment.ProfileFragmentListener) {
            mListener = (ProfileEditFragment.ProfileEditFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ProfileEditFragment.ProfileEditFragmentListener");
        }
    }

    public ProfileEditFragment() {
    }

    public static ProfileEditFragment newInstance(UserProfile userProfile) {

        ProfileEditFragment fragment = new ProfileEditFragment();
        Bundle args = new Bundle();

        Parcelable listParcelable = Parcels.wrap(userProfile);
        args.putParcelable("userProfile", listParcelable);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mUserProfile = Parcels.unwrap(getArguments().getParcelable("userProfile"));
            mContext = getContext();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_edit, container, false);
        ButterKnife.bind(this, view);

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        //Update view data
        mEtName.setText(mUserProfile.name);
        mEtScreenName.setText(mUserProfile.screenName);
        mEtBio.setText(mUserProfile.description);
        mBtLocation.setText(mUserProfile.location);

        addProfilePicture(mUserProfile.getProfileImageUrl());
        addDefaultPicture(mUserProfile.getProfileBannerImageUrl());

        return view;
    }

    @OnClick({R.id.ivProfilePicture, R.id.ivAddProfileUrl, R.id.ivDefaultPicture, R.id.ivAddDefaultUrl})
    public void onClick(View view){
        String imageUrl = "";

        switch (view.getId()){
            case R.id.ivProfilePicture:
            case R.id.ivAddProfileUrl:
                imageUrl = "";
                updateProfilePicture(imageUrl);
            break;

            case R.id.ivDefaultPicture:
            case R.id.ivAddDefaultUrl:
                imageUrl = "";
                updateDefaultPicture(imageUrl);
            break;

            case R.id.btLocation:
                mListener.onLocationClicked();
            break;

            default:
                // TODO: do nothing
            break;

        }
    }

    @Nullable
    @OnClick(R.id.btLocation)
    public void onLocationClick() {
        mListener.onLocationClicked();
    }

    public void updateProfilePicture(String imageUrl) {
        //TODO: update profile picture
        Toast.makeText(getContext(), "Update picture triggered", Toast.LENGTH_LONG).show();

        addProfilePicture(imageUrl);
    }

    private void addProfilePicture(String imageUrl) {
        if(imageUrl != null  && !imageUrl.trim().equals("")) {
            Picasso.with(getContext())
                    .load(imageUrl)
                    .transform(new RoundedCornersTransformation(0, 0))
                    .resize(50, 50)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(mIvProfilePicture);
        }
    }

    public void updateDefaultPicture(String imageUrl) {
        //TODO: update banner picture
        Toast.makeText(getContext(), "Update banner picture triggered", Toast.LENGTH_LONG).show();

        addDefaultPicture(imageUrl);
    }

    private void addDefaultPicture(String imageUrl) {
        if(imageUrl != null  && !imageUrl.trim().equals("")) {
            Picasso.with(getContext())
                    .load(imageUrl)
                    .transform(new RoundedCornersTransformation(0, 0))
                    .resize(1100, 850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(mIvDefaultPicture);
        }
    }

    public void addLocation(String location){
        if(location != null  && !location.trim().equals("")) {
            mBtLocation.setText(location);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.save_user_profile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.action_home: {
                getFragmentManager().popBackStack();
                return true;
            }

            case R.id.action_save_user_profile: {
                //TODO: save user profile to the database and go back to the profile screen
                Toast.makeText(getContext(), "Save user profile triggered", Toast.LENGTH_LONG).show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
