package com.travelit.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.TravelItApplication;
import com.travelit.models.GooglePlace;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.User;
import com.travelit.util.ImagePickerUtil;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartTravelLogEntryFragment extends Fragment {
    private final static String TAG = StartTravelLogEntryFragment.class.getSimpleName();
    private TravelLogEntry mTravelEntry;
    private StartTravelLogEntryFragment.OnFragmentListner mListener;

    @BindView(R.id.ivTravelPic)
    ImageView ivTravelPic;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvDescription)
    TextView tvDescription;

    @BindView(R.id.tvNotes)
    TextView tvNotes;

    @BindView(R.id.tvCheckin)
    TextView tvCheckin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StartTravelLogEntryFragment.OnFragmentListner) {
            mListener = (StartTravelLogEntryFragment.OnFragmentListner) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ProfileFragment.ProfileFragmentListener");
        }
    }

    public interface OnFragmentListner {
        void onAddPhotoClicked(TravelLogEntry mTravelEntry);
        void onCheckin(TravelLogEntry mTravelEntry);
    }

    public StartTravelLogEntryFragment() {
        // Required empty public constructor
    }

    public static StartTravelLogEntryFragment newInstance(TravelLogEntry entry) {
        StartTravelLogEntryFragment fragment = new StartTravelLogEntryFragment();
        Bundle args = new Bundle();
        args.putParcelable("entry", Parcels.wrap(entry));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTravelEntry = (TravelLogEntry) Parcels.unwrap(getArguments().getParcelable("entry"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_start_travel_log_entry, container, false);
        ButterKnife.bind(this,view);

        tvNotes.setText(mTravelEntry.notes);
        tvDescription.setText(mTravelEntry.description);
        tvTitle.setText(mTravelEntry.title);

        if(mTravelEntry.images != null && mTravelEntry.images.size() > 0) {
            Picasso.with(getContext())
                    .load(mTravelEntry.images.get(0).getUrl())
                    .transform(new RoundedCornersTransformation(0,0))
                    .resize(1100,850)
                    .placeholder(R.drawable.ic_photo)
                    .error(R.drawable.ic_camera)
                    .into(ivTravelPic);
        }
        return view;
    }

    @OnClick(R.id.btAddPhoto)
    public void addPhotoClick() {
        mListener.onAddPhotoClicked(mTravelEntry);
    }

    @OnClick(R.id.btCheckin)
    public void onCheckInClick() {
        //View view = getLayoutInflater().inflate(R.layout.custom_snackbar_view, null);
        String msg = mTravelEntry.surpriseMsg;
        if(msg != null) {
            Snackbar.make(ivTravelPic,msg, Snackbar.LENGTH_LONG)
                    .setAction("Details", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            FragmentManager fm = getChildFragmentManager();
                            SurpriseFragment editNameDialogFragment = SurpriseFragment.newInstance(msg);
                            editNameDialogFragment.show(fm, "fragment_surprise");
                        }
                    })
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setDuration(3000).show();
        }

        //TODO: Sai update db with checked in status;
        mTravelEntry.checkedIn = true;
        tvCheckin.setText("Checked-in");
        mListener.onCheckin(mTravelEntry);


    }

    @OnClick(R.id.btAddSurprise)
    public void addSurpriseClick() {
        FragmentManager fm = getChildFragmentManager();
        AddSurpriseFragment addSurpriseFragment = AddSurpriseFragment.newInstance(mTravelEntry);
        addSurpriseFragment.show(fm, "fragment_edit_name");
    }

    @OnClick(R.id.btDirection)
    public void getDirectionClick() {
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", mTravelEntry.travelLatLong.latitude, mTravelEntry.travelLatLong.longitude, mTravelEntry.title);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(getContext(), "Please install a maps application", Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.btReview)
    public void getReviewClick() {
        FragmentManager fm = getChildFragmentManager();
        ReviewFragment reviewFragment = ReviewFragment.newInstance(mTravelEntry);
        reviewFragment.show(fm, "fragment_edit_name");
    }
}
