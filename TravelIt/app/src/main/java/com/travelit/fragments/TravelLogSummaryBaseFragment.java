package com.travelit.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.adapters.TravelLogAdapter;
import com.travelit.eventlisteners.EndlessRecyclerViewScrollListener;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.TravelLogEntryAssociation;
import com.travelit.models.User;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.LogUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Sai Muppa
 */
public abstract class TravelLogSummaryBaseFragment extends Fragment {

    @BindView(R.id.rvTravelLog)
    RecyclerView mRvTravelLog;

    @BindView(R.id.srContainer)
    SwipeRefreshLayout mSwipeContainer;

    TravelLogAdapter mTravelLogRecyclerViewAdapter;
    EndlessRecyclerViewScrollListener mEndlessRecyclerViewScrollListener;
    LinearLayoutManager mLinearLayoutManager;
    List<TravelLog> mTravelogs;

    static final String TAG = "TravelLogBaseFragment";
    static final short START_PAGE = 1;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    static final String ARG_CURRENT_USER = "currentUser";

    UserProfile mCurrentUserProfile;
    View mCurrentView;

    ProgressDialog pd;

    Unbinder unbinder;
    OnFragmentInteractionListener mListener;

    String mSearchQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        unbinder = ButterKnife.bind(this, mCurrentView);

        setTravelLogRecyclerView();

        setSwipeRefreshContainer();

        pd = new ProgressDialog(this.getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        pd.show();

        populateTravelLogs(START_PAGE);

        return mCurrentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Reset Endless Scroller
     */
    private void resetEndlessScroller() {
        if(mTravelLogRecyclerViewAdapter!= null) {
            mTravelLogRecyclerViewAdapter.clear();
        }

        if(mEndlessRecyclerViewScrollListener != null) {
            // 3. Reset endless scroll listener when performing a new search
            mEndlessRecyclerViewScrollListener.resetState();
        }
    }

    /**
     * Refreshes and populates the latest travel logs
     */
    private void refreshTravellogs() {
        resetEndlessScroller();

        populateTravelLogs(START_PAGE);
    }

    /**
     * Set's up the swipe refresh layout for the travel logs time line
     */
    void setSwipeRefreshContainer() {
        // Setup refresh listener which triggers new data loading
        mSwipeContainer.setOnRefreshListener(() -> {
            // Your code to refresh the list here.
            // Make sure you call swipeContainer.setRefreshing(false)
            // once the network request has completed successfully.
            refreshTravellogs();
        });

        // Configure the refreshing colors
        mSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    /**
     * Initializes the travel log recycler view
     */
    void setTravelLogRecyclerView() {
        mTravelogs = new ArrayList<>();

        // Create adapter passing in the initial travel log data
        mTravelLogRecyclerViewAdapter = new TravelLogAdapter(getActivity(), mCurrentUserProfile, mTravelogs);

        // Attach the adapter to the recyclerview to populate items
        mRvTravelLog.setAdapter(mTravelLogRecyclerViewAdapter);

        mLinearLayoutManager =
                new LinearLayoutManager(getActivity());

        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        // Set layout manager to position the items
        mRvTravelLog.setLayoutManager(mLinearLayoutManager);

        // Retain an instance so that you can call `resetState()` for fresh searches
        mEndlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //populateTravelLogs(page);
            }
        };

        // Adds the scroll listener to RecyclerView
        mRvTravelLog.addOnScrollListener(mEndlessRecyclerViewScrollListener);
    }

    void appendTravelLogs(List<TravelLog> travelLogs) {
        mTravelogs.addAll(0, travelLogs);
        mTravelLogRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
        mLinearLayoutManager.scrollToPosition(0);
    }

    /**
     * get the key from the firebase based on the last query to use as an anchor for the subsequent query
     * @param pageId
     * @return
     */
    String getFirebaseAnchorKey(long pageId) {
        String anchorKey = "";
        if(pageId >= 1){
            int currSize = mTravelLogRecyclerViewAdapter.getItemCount();
            if(currSize >= 1){
                //anchorKey = mTravelogs.get(currSize - 1).getId(); //TODO: revisit the logic once (probable reference: http://stackoverflow.com/questions/35098894/implementing-infinite-scrolling-with-firebase)
            }
        }

        return anchorKey;
    }

    void populateTravelLogs(long maxId){
        if(mSearchQuery != null && !mSearchQuery.trim().isEmpty()) {
            searchTravelLogs();
        }
        else {
            onRefreshTravelLogs();
        }

        LogUtil.logD(TAG, mTravelogs.toString());
    }

    void searchTravelLogs(){
        FirebaseApiHelper.getInstance().getTravelLogsByLexicalSearchExcludingCurrentUser(mSearchQuery, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
            @Override
            public void callback(List<TravelLog> data) {
                List<TravelLog> logs = new ArrayList<TravelLog>();
                List<TravelLog> curatedLogs = new ArrayList<TravelLog>();

                //only display complete logs
                for(TravelLog log:data) {
                    if(log != null && log.isComplete) {
                        logs.add(log);
                    }
                }

                final String loggedInUserId =  FirebaseApiHelper.getInstance().getFirebaseUser().getUid();

                FirebaseApiHelper.getInstance().getUser(loggedInUserId, new FirebaseApiHelper.FirebaseRetrieverCallback<User>() {
                    @Override
                    public void callback(User user) {
                        final List<TravelLog> curatedLogs = curateTravelLogs(user, logs);

                        if(curatedLogs!= null && curatedLogs.size() > 0) {
                            mTravelogs.addAll(curatedLogs);
                            mTravelLogRecyclerViewAdapter.notifyItemRangeInserted(0, curatedLogs.size());
                        }

                        if(mSwipeContainer !=  null) {
                            // Now we call setRefreshing(false) to signal refresh has finished
                            mSwipeContainer.setRefreshing(false);
                        }

                       if(pd != null) {
                           pd.hide();
                       }
                    }
                });
            }
        });
    }

    private List<TravelLog> curateTravelLogs(User user, List<TravelLog> logs) {
        Map<String, TravelLogEntryAssociation> selectedUsers = user.selectedTravelEntries;
        for(TravelLog log: logs){
            if(log.travelLogEntries == null){
                log.travelLogEntries = new ArrayList<TravelLogEntry>();
            }

            for (TravelLogEntry entry : log.travelLogEntries) {
                if(selectedUsers == null){
                    selectedUsers = new HashMap<String, TravelLogEntryAssociation>();
                }

                if(entry == null || entry.id == null){
                    log.travelLogEntries.remove(entry);
                }

                if (selectedUsers.containsKey(entry.id)) {
                    entry.isSelected = true;
                } else {
                    entry.isSelected = false;
                }
            }
        }

        return logs;
    }

    void onRefreshTravelLogs(){
        FirebaseApiHelper.getInstance().getAllTravelLogsExcludingCurrentUser(new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
            @Override
            public void callback(List<TravelLog> travelLogs) {


                if (travelLogs != null) {
                    List<TravelLog> logs = new ArrayList<TravelLog>();

                    //only display complete logs
                    for(TravelLog log:travelLogs) {
                        if(log != null && log.isComplete) {
                            logs.add(log);
                        }
                    }


                    final String loggedInUserId =  FirebaseApiHelper.getInstance().getFirebaseUser().getUid();

                    FirebaseApiHelper.getInstance().getUser(loggedInUserId, new FirebaseApiHelper.FirebaseRetrieverCallback<User>() {
                        @Override
                        public void callback(User user) {
                            final List<TravelLog> curatedLogs = curateTravelLogs(user, logs);

                            if(curatedLogs != null && curatedLogs.size() > 0) {
                                int currSize = mTravelLogRecyclerViewAdapter.getItemCount();
                                mTravelogs.addAll(curatedLogs);
                                mTravelLogRecyclerViewAdapter.notifyItemRangeInserted(currSize, curatedLogs.size());
                                LogUtil.logD("MainActivity", curatedLogs.toString());
                            }

                            if(mSwipeContainer != null) {
                                // Now we call setRefreshing(false) to signal refresh has finished
                                mSwipeContainer.setRefreshing(false);
                            }

                            if(pd != null) {
                                pd.hide();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();

        pd.dismiss();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        pd.dismiss();
    }

    public void clearTravelLogs() {
        mTravelLogRecyclerViewAdapter.clear();
    }
}
