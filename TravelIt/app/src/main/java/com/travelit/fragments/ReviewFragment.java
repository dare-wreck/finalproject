package com.travelit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.travelit.R;
import com.travelit.TravelItApplication;
import com.travelit.adapters.ReviewRecyclerAdapter;
import com.travelit.models.GooglePlace;
import com.travelit.models.Review;
import com.travelit.models.TravelLogEntry;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends DialogFragment {
    private final static String TAG = ReviewFragment.class.getSimpleName();
    private TravelLogEntry mEntry;

    @BindView(R.id.rvReviews)
    RecyclerView rvReviews;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private List<Review> mReviews;
    private ReviewRecyclerAdapter adapter;
    public ReviewFragment(){}

    public static ReviewFragment newInstance(TravelLogEntry entry) {
        ReviewFragment frag = new ReviewFragment();
        Bundle args = new Bundle();
        args.putParcelable("travelLogEntry", Parcels.wrap(entry));
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mEntry = (TravelLogEntry) Parcels.unwrap(getArguments().getParcelable("travelLogEntry"));
        View view =  inflater.inflate(R.layout.fragment_review, container);
        ButterKnife.bind(this, view);
        mReviews = new ArrayList<>();
        ReviewRecyclerAdapter adapter = new ReviewRecyclerAdapter(getContext(), mReviews);
        rvReviews.setAdapter(adapter);
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));

        Call<GooglePlace> call =   TravelItApplication.getGoogleApiClient().getClient().getReviews(TravelItApplication.getGoogleApiClient().API_KEY, mEntry.travelLatLong.placeId);
        call.enqueue(new Callback<GooglePlace>() {
            @Override
            public void onResponse(Call<GooglePlace> call, Response<GooglePlace> response) {
                if (response.isSuccessful()) {
                    GooglePlace place = response.body();
                    if(place != null && !place.getStatus().equalsIgnoreCase("INVALID_REQUEST")
                            && place.getResult() != null && place.getResult().getReviews() != null){
                        adapter.add(place.getResult().getReviews());
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<GooglePlace> call, Throwable t) {
                Log.d(TAG, "Search Request Failed: " + t.getMessage());
            }
        });

        final AppCompatActivity _activity = (AppCompatActivity)getActivity();
        _activity.setSupportActionBar(mToolbar);
        final ActionBar actionBar = _activity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.dialog_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.action_close: {
                dismiss();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


}
