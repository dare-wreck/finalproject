package com.travelit.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.travelit.R;
import com.travelit.adapters.TimelineAdapter;
import com.travelit.models.Orientation;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.util.ItemClickSupport;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimelineFragment  extends Fragment {

    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;

    private TimelineAdapter mTimeLineAdapter;
    private List<TravelLogEntry> mTravelLogEntries;
    private LinearLayoutManager linearLayoutManager;
    private TimelineFragmentListener mListener;
    private boolean isHorizontal;
    private boolean mEnableCheckBox;
    private boolean mIsTravelLogComplete;
    public TimelineFragment() {
        mTravelLogEntries = new ArrayList<>();
    }

    public static TimelineFragment newInstance(final List<TravelLogEntry> mTravelLogEntry, boolean isHorizontal, boolean enableCheckbox, boolean isTravelLogComplete) {
        TimelineFragment fragment = new TimelineFragment();
        Bundle args = new Bundle();
        args.putParcelable("travelLogEntries", Parcels.wrap(mTravelLogEntry));
        args.putBoolean("isHorizontal", isHorizontal);
        args.putBoolean("enableCheckBox", enableCheckbox);
        args.putBoolean("isTravelLogComplete", isTravelLogComplete);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTravelLogEntries = (List<TravelLogEntry>) Parcels.unwrap(getArguments().getParcelable("travelLogEntries"));
            isHorizontal = getArguments().getBoolean("isHorizontal");
            mEnableCheckBox = getArguments().getBoolean("enableCheckBox");
            mIsTravelLogComplete = getArguments().getBoolean("isTravelLogComplete");
        }
    }

    public void onCheckinClicked(TravelLogEntry entry) {
        mTimeLineAdapter.updateValue(entry);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, view);


        if(isHorizontal) {
            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        } else {
            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        }
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        TravelLogEntry timelineModel = mTravelLogEntries.get(position);
                        mListener.onTimelineDetailClicked(timelineModel, position);
                    }
                }
        );
        initView();

        return view;
    }

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TimelineFragmentListener) {
            mListener = (TimelineFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement TimelineFragment.TimelineFragmentListener");
        }
    }

    private void initView() {
        if(isHorizontal) {
            mTimeLineAdapter = new TimelineAdapter(mTravelLogEntries, Orientation.HORIZONTAL, mEnableCheckBox, mIsTravelLogComplete);
        } else {
            mTimeLineAdapter = new TimelineAdapter(mTravelLogEntries, Orientation.VERTICAL, mEnableCheckBox, mIsTravelLogComplete);
        }
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    public void scrollToPosition(int index) {
        linearLayoutManager.scrollToPositionWithOffset(index, 10);

        if(mTimeLineAdapter.getSelectedIndex() != index) {
            mTimeLineAdapter.setSelectedIndex(index);
            mTimeLineAdapter.notifyItemChanged(index);

            for (int i = mTimeLineAdapter.getMinViewableIndex(); i <= mTimeLineAdapter.getMaxViewableIndex(); i++) {
                mTimeLineAdapter.notifyItemChanged(i);
            }
        }

    }

    public interface TimelineFragmentListener {
        void onTimelineDetailClicked(TravelLogEntry entry, int position);
    }
}