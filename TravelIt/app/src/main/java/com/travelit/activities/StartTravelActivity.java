package com.travelit.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.travelit.R;
import com.travelit.fragments.StartTravelFragment;
import com.travelit.fragments.StartTravelLogEntryFragment;
import com.travelit.fragments.TimelineFragment;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.ImagePickerUtil;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by darewreck_PC on 4/16/2017.
 */

public class StartTravelActivity  extends AppCompatActivity implements TimelineFragment.TimelineFragmentListener, StartTravelFragment.OnFragmentInteractionListener, StartTravelLogEntryFragment.OnFragmentListner{
    private static String TAG = StartTravelActivity.class.getName();
    private TravelLog mTravelLog;
    private TimelineFragment timelineFragment;
    private StartTravelFragment startTravelFragment;
    private static final int PICK_IMAGE_ID = 2222;

    private Uri mImageCaptureUri;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_travel);
        ButterKnife.bind(this);
        mTravelLog = (TravelLog) Parcels.unwrap(getIntent().getParcelableExtra("travelLog"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        timelineFragment = TimelineFragment.newInstance(mTravelLog.getTravelLogEntries(), true, false, mTravelLog.isComplete);
        ft.replace(R.id.flContainer1, timelineFragment);

        startTravelFragment = StartTravelFragment.newInstance(mTravelLog);
        ft.replace(R.id.flContainer2, startTravelFragment);

        ft.addToBackStack(TAG); // name can be null
        ft.commit();
    }


    @Override
    public void onTimelineDetailClicked(TravelLogEntry entry, int position) {
        startTravelFragment.scrollToPage(position);
    }

    @Override
    public void onViewPagerScroll(int position) {
        timelineFragment.scrollToPosition(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    private TravelLogEntry mCurrentSelected = null;

    @Override
    public void onAddPhotoClicked(TravelLogEntry mTravelEntry) {
        mCurrentSelected = mTravelEntry;
        Intent chooseImageIntent = ImagePickerUtil.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @Override
    public void onCheckin(TravelLogEntry mTravelEntry) {
        timelineFragment.onCheckinClicked(mTravelEntry);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                if(resultCode == -1) {
                    if(data != null && data.getData() != null && data.getData().getScheme() != null
                            && data.getData().getAuthority() != null && data.getData().getPath() != null) {
                        mImageCaptureUri = Uri.parse(data.getData().getScheme() + "://" + data.getData().getAuthority() + data.getData().getPath());

                        if(mTravelLog != null && mCurrentSelected != null && mCurrentSelected.id != null && mImageCaptureUri != null){
                            FirebaseApiHelper firebaseApiHelper = FirebaseApiHelper.getInstance();
                            firebaseApiHelper.updateTravelLogEntryWithImageAddition(mTravelLog.getId(), mCurrentSelected.id, mImageCaptureUri);
                        }
                    }

                    //mCurrentSelected is the current selected log where the click actionis triggered
                    //user the current user and log entry (mCurrentSelected) to store to the db.  Might need to store the travelEntry from onAddPhotoClicked
                    //TODO: Add photo to collection to database
                } else {
                    //cancel is triggered so do nothing
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}