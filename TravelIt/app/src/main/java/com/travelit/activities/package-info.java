/**
 * This package contains various activities of the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.activities;