package com.travelit.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.travelit.R;
import com.travelit.fragments.HomeFragment;
import com.travelit.fragments.MapFragment;
import com.travelit.fragments.PlaceListFragment;
import com.travelit.fragments.TimelineFragment;
import com.travelit.fragments.TravelLogEntriesBaseFragment;
import com.travelit.fragments.TravelLogEntriesMediaFragment;
import com.travelit.fragments.TravelLogEntryDetailFragment;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.UserProfile;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TravelEntriesActivity extends BaseActivity implements  TravelLogEntriesBaseFragment.OnFragmentInteractionListener,
                                                                    MapFragment.OnFragmentListener,
                                                                    TimelineFragment.TimelineFragmentListener,
                                                                    PlaceListFragment.OnFragmentListener,
        TravelLogEntriesMediaFragment.OnFragmentListener{

    public static final String EXTRA_TRAVEL_LOG = "travelLog";

    private static final String TAG = "UserProfileActivity";
    public static final String EXTRA_USER_PROFILE = "userProfile";

    private UserProfile mUserProfile;


    @BindView(R.id.fam_action_menu)
    FloatingActionMenu fam;

    @OnClick(R.id.fab_add_travel_log_menu_item)
    public void onFabAddTravelLogMenuClicked() {
        Intent intent = new Intent(this, AddTravelLogActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.fab_add_travel_log_entry_menu_item)
    public void onFabAddTravelLogEntryClicked() {
        Intent intent = new Intent(this, AddTravelLogEntryActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_entries);

        ButterKnife.bind(TravelEntriesActivity.this);

        TravelLog mTravelLog = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_TRAVEL_LOG));

        mUserProfile = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_USER_PROFILE));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HomeFragment tavelLogEntriesFragment = HomeFragment.newInstance(mTravelLog, mUserProfile);
        ft.replace(R.id.flContainer, tavelLogEntriesFragment);

        // ADD THIS LINE
        ft.addToBackStack("TravelEntriesActivity"); // name can be null
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onTimelineDetailClicked(TravelLogEntry entry, int position) {
        Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        TravelLogEntryDetailFragment timelineDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
        ft.replace(R.id.flContainer, timelineDetailFragment);

        // ADD THIS LINE
        ft.addToBackStack("TravelLogEntryDetailFragment"); // name can be null
        ft.commit();
    }

    @Override
    public void onTravelLogEntryClicked(TravelLogEntry entry) {
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        TravelLogEntryDetailFragment travelLogEntryDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
        ft.replace(R.id.flContainer, travelLogEntryDetailFragment);

        // ADD THIS LINE
        ft.addToBackStack("TravelLogEntryDetailFragment"); // name can be null
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            case android.R.id.home: {
                if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    finish();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed()
    {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
    @Override
    public void onMarkerClick(TravelLogEntry entry) {
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        TravelLogEntryDetailFragment travelLogEntryDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
        ft.replace(R.id.flContainer, travelLogEntryDetailFragment);

        // ADD THIS LINE
        ft.addToBackStack("TravelLogEntryDetailFragment"); // name can be null
        ft.commit();
    }

    @Override
    public void onMediaClicked(TravelLogEntry entry) {
        if(entry!= null && entry.id != null) {
            // Begin the transaction
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            TravelLogEntryDetailFragment travelLogEntryDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
            ft.replace(R.id.flContainer, travelLogEntryDetailFragment);

            // ADD THIS LINE
            ft.addToBackStack("TravelLogEntryDetailFragment"); // name can be null
            ft.commit();
        }
    }
}
