package com.travelit.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.adapters.SpinTravelLogEntryAdapter;
import com.travelit.models.Media;
import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLatLong;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.DateTimeUtils;
import com.travelit.util.ImagePickerUtil;
import com.travelit.util.LogUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

import static android.support.design.widget.Snackbar.make;

public class AddTravelLogEntryActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.btAddLocation)
    Button btAddLocation;

    @BindView(R.id.ivPreview)
    ImageView ivTripConverPic;

    /*@BindView(R.id.etTitle)
    EditText mEtTitle;*/

    @BindView(R.id.spPrivacy)
    Spinner mSpPrivacy;

    @BindView(R.id.cbNotification)
    CheckBox mCbNotification;

    @BindView(R.id.etDescription)
    EditText mEtDescription;

    @BindView(R.id.etNotes)
    EditText mEtNotes;

    @BindView(R.id.spAssociateTrip)
    Spinner spAssociateTrip;

    @BindView(R.id.rlTravelLogEntry)
    RelativeLayout mRlTravelLogEntry;

    private Context mContext;
    private Uri mImageCaptureUri;

    private TravelLatLong mSelectedTravelLatLong;

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 132;
    private static final int PICK_IMAGE_ID = 2334;
    private FirebaseApiHelper mFbApiHelper;

    private TravelLatLong travelInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_travel_log_entry);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        String loggedInUserId = getIntent().getStringExtra("user");
        mContext = this;
        mFbApiHelper = FirebaseApiHelper.getInstance();
        mFbApiHelper.getTravelLogsByUser(mFbApiHelper.getFirebaseUser().getUid(), new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLog>>() {
            @Override
            public void callback(List<TravelLog> travelLogs) {
                TravelLog[] travelLogArray = (TravelLog[])travelLogs.toArray(new TravelLog[travelLogs.size()]);

                SpinTravelLogEntryAdapter spinAdapter = new SpinTravelLogEntryAdapter(mContext, android.R.layout.simple_dropdown_item_1line,  travelLogArray );
                spAssociateTrip.setAdapter(spinAdapter);
            }
        });

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_trip_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //When home is clicked
            case R.id.action_close: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btAddLocation)
    public void onAddLocationFocus() {
         Intent intent = new Intent(this, PlacesAutoCompleteActivity.class);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

    }

    @OnClick(R.id.ivPreview)
    public void onAddPhotoClick() {
        Intent chooseImageIntent = ImagePickerUtil.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @OnClick(R.id.btAddTrip)
    public void onSubmit(){
        TravelLogEntry travelLogEntry = new TravelLogEntry();
        Snackbar snackbar;
        TravelLog travelLog = null;

        /*if(mEtTitle.getText()!=null && !mEtTitle.getText().toString().equals("")){
            travelLogEntry.title = mEtTitle.getText().toString();
        }
        else{
            snackbar = make(findViewById(R.id.rlTravelLog), "Need to add Title.",Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }*/

        if(btAddLocation.getText() !=null && ! btAddLocation.getText().toString().equalsIgnoreCase("Add Location")){
            travelLogEntry.title = mSelectedTravelLatLong.name;
        }
        else{
            snackbar = Snackbar.make(mRlTravelLogEntry,
                    "Need to add Location.", Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        try {
            travelLog = (TravelLog) spAssociateTrip.getSelectedItem();
            if(travelLog == null || travelLog.getId() == null){
                snackbar = make(mRlTravelLogEntry, "Selected trip is invalid.",Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
        }
        catch (Exception e){
            LogUtil.logE("AddTripEntryActivity", e.getMessage(), e);
        }

        travelLogEntry.date = DateTimeUtils.getUTCMillisAsString();
        travelLogEntry.dateCreated = DateTimeUtils.getUTCMillisAsString();
        travelLogEntry.privacy= PrivacyEnum.valueOf(mSpPrivacy.getSelectedItem().toString().toUpperCase());
        travelLogEntry.notify = mCbNotification.isChecked();

        if(mEtDescription.getText() != null && !mEtDescription.getText().toString().equals("")){
            travelLogEntry.description = mEtDescription.getText().toString();
        }
        else{
            snackbar= make(mRlTravelLogEntry, "Need to add Description.",Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        travelLogEntry.notes = mEtNotes.getText().toString();

        travelLogEntry.travelLatLong = mSelectedTravelLatLong;

        travelLogEntry.images = new ArrayList<>();
        if(mImageCaptureUri != null) {
            travelLogEntry.images.add(new Media(null, mImageCaptureUri.toString(), null, null));
        }

        FirebaseApiHelper.getInstance().addTravelLogEntry(travelLog.getId(), travelLogEntry, mImageCaptureUri, new FirebaseApiHelper.FirebaseRetrieverCallback<String>() {
            @Override
            public void callback(String callbackMessage) {
                Snackbar snackbar = make(mRlTravelLogEntry, callbackMessage, Snackbar.LENGTH_SHORT);
                snackbar.show();
                finish();
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case PICK_IMAGE_ID:
                if(resultCode == -1) {

                    if(data != null && data.getData() != null && data.getData().getScheme() != null
                            && data.getData().getAuthority() != null && data.getData().getPath() != null) {
                        mImageCaptureUri = Uri.parse(data.getData().getScheme() + "://" + data.getData().getAuthority() + data.getData().getPath());

                        if(mImageCaptureUri != null) {
                            Picasso.with(getApplicationContext())
                                .load(mImageCaptureUri)
                                .transform(new RoundedCornersTransformation(0,0))
                                .resize(1100,850)
                                .placeholder(R.drawable.ic_photo)
                                .error(R.drawable.ic_camera)
                                .into(ivTripConverPic);
                        }
                    }

                } else if( resultCode == 0) {
                    //Cancel triggered
                }
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if(data!=null) {
                    mSelectedTravelLatLong = Parcels.unwrap(data.getParcelableExtra("travelLatLong"));
                    if(mSelectedTravelLatLong!= null && mSelectedTravelLatLong.address != null) {
                        btAddLocation.setText(mSelectedTravelLatLong.address);
                    }
                }
            }
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
