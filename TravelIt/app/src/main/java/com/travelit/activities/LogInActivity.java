package com.travelit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.travelit.R;
import com.travelit.networks.FirebaseApiHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Login Activity for TravelIt app using FireBase authentication
 * @author Sai Muppa
 */
public class LogInActivity extends BaseActivity {

    @BindView(R.id.etEmailField)
    EditText mEtEmailText;

    @BindView(R.id.etPasswordField)
    EditText mEtPasswordText;

    @BindView(R.id.btnLogin)
    Button mBtnLogin;

    @BindView(R.id.btnSignUp)
    TextView mBtnSignUp;

    FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        ButterKnife.bind(this);

        mFirebaseAuth = FirebaseApiHelper.getInstance().getFireBaseAuth();
    }

    @OnClick(R.id.btnSignUp)
    void onSignUpClick(View v){
        Intent intent = new Intent(LogInActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnLogin)
    void onLoginClick(View v){
        String email = mEtEmailText.getText().toString();
        String password = mEtPasswordText.getText().toString();

        if(email.isEmpty() && password.isEmpty()) {
            email = "demo1@travelit.com";//email.trim();
            password = "travelitdemo";//password.trim();
        }

        if (email.isEmpty() || password.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
            builder.setMessage(R.string.login_error_message)
                    .setTitle(R.string.login_error_title)
                    .setPositiveButton(android.R.string.ok, null);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LogInActivity.this, task -> {
                    if (task.isSuccessful()) {
                        Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                        builder.setMessage(task.getException().getMessage())
                                .setTitle(R.string.login_error_title)
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
        }
    }
}
