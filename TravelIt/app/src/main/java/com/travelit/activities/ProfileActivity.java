package com.travelit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.github.clans.fab.FloatingActionMenu;
import com.travelit.R;
import com.travelit.fragments.MapFragment;
import com.travelit.fragments.MapTimelineFragment;
import com.travelit.fragments.ProfileEditFragment;
import com.travelit.fragments.ProfileFragment;
import com.travelit.fragments.TimelineFragment;
import com.travelit.fragments.TravelLogEntryDetailFragment;
import com.travelit.fragments.TripListFragment;
import com.travelit.models.TravelLatLong;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity  extends AppCompatActivity implements TimelineFragment.TimelineFragmentListener,
        MapTimelineFragment.MapTimeLineFragmentListener,
        ProfileFragment.ProfileFragmentListener,
        TripListFragment.TripListFragmentListener,
        MapFragment.OnFragmentListener,
        ProfileEditFragment.ProfileEditFragmentListener{

    private ProfileFragment profileFragment;

    private ProfileEditFragment mProfileEditFragment;

    private static final int PICK_IMAGE_ID = 234;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 132;

    private FirebaseApiHelper mFbApiHelper;
    private static final String TAG = ProfileActivity.class.getName();

    @BindView(R.id.fam_action_menu)
    FloatingActionMenu fam;

    @OnClick(R.id.fab_add_travel_log_menu_item)
    public void onFabAddTravelLogMenuClicked() {
        Intent intent = new Intent(this, AddTravelLogActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.fab_add_travel_log_entry_menu_item)
    public void onFabAddTravelLogEntryClicked() {
        Intent intent = new Intent(this, AddTravelLogEntryActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            setContentView(R.layout.activity_profile);
            ButterKnife.bind(this);

            mFbApiHelper = FirebaseApiHelper.getInstance();

            UserProfile userProfile = (UserProfile) Parcels.unwrap(getIntent().getParcelableExtra("userProfile"));
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            profileFragment = ProfileFragment.newInstance(userProfile);
            ft.replace(R.id.flContainer, profileFragment);
            ft.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                if(getSupportFragmentManager().getBackStackEntryCount() >= 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
    @Override
    public void onTimelineDetailClicked(TravelLogEntry entry, int position) {
        //Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        TravelLogEntryDetailFragment timelineDetailFragment = TravelLogEntryDetailFragment.newInstance(entry);
        ft.replace(R.id.flContainer, timelineDetailFragment);

        // ADD THIS LINE
        ft.addToBackStack("mapTimeLineFragment"); // name can be null
        ft.commit();

    }

    @Override
    public void onStartAdventureClick(TravelLog entry) {
        Intent intent = new Intent(this, StartTravelActivity.class);
        intent.putExtra("travelLog", Parcels.wrap(entry));
        startActivity(intent);
    }

    @Override
    public void onTravelLogClicked(TravelLog entry) {
         mFbApiHelper.getTravelLogEntriesByTravelLog(entry.id, new FirebaseApiHelper.FirebaseRetrieverCallback<List<TravelLogEntry>>() {
            @Override
            public void callback(List<TravelLogEntry> entries) {
                entry.travelLogEntries = entries;
                // Begin the transaction
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                MapTimelineFragment mapTimelineFragment = MapTimelineFragment.newInstance(entry, true, false, true);
                ft.replace(R.id.flContainer, mapTimelineFragment);

                ft.addToBackStack("profileFragment"); // name can be null
                ft.commit();
            }
        });
    }

    @Override
    public void onEditUserProfile(UserProfile userProfile) {
        //Intent chooseImageIntent = ImagePickerUtil.getPickImageIntent(this);
        //startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);

        if(userProfile!= null && userProfile.id != null) {
            // Begin the transaction
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            mProfileEditFragment = ProfileEditFragment.newInstance(userProfile);
            ft.replace(R.id.flContainer, mProfileEditFragment);

            // ADD THIS LINE
            ft.addToBackStack("ProfileFragment"); // name can be null
            ft.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                if(resultCode == -1) {
                    String imageUrl = "";
                    //profileFragment.updateProfilePicture(imageUrl);
                } else {
                    //cancel is triggered so do nothing
                }
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if(data!=null && mProfileEditFragment != null) {
                    TravelLatLong travelLatLong = Parcels.unwrap(data.getParcelableExtra("travelLatLong"));

                    if(travelLatLong!= null && travelLatLong.name != null) {
                        mProfileEditFragment.addLocation(travelLatLong.name);
                    }
                }
            }
            default:
                super.onActivityResult(requestCode, resultCode, data);
            break;
        }
    }

    @Override
    public void onMarkerClick(TravelLogEntry entry) throws Exception {
        // TODO: do something -
        //throw new Exception("Should not be implemented");
    }

    @Override
    public void onLocationClicked() {
        Intent intent = new Intent(this, PlacesAutoCompleteActivity.class);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }
}
