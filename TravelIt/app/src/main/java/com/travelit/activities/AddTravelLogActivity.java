package com.travelit.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelit.R;
import com.travelit.models.PrivacyEnum;
import com.travelit.models.TravelLatLong;
import com.travelit.models.TravelLog;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.DateTimeUtils;
import com.travelit.util.ImagePickerUtil;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class AddTravelLogActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String NEED_TO_ADD_TITLE = "Need to add title.";
    public static final String NEED_TO_ADD_LOCATION = "Need to add location.";
    public static final String NEED_TO_ADD_DESCRIPTION = "Need to add description.";
    public static final String ADDED_NEW_TRIP = "Added new trip!";
    public static final String NEED_TO_PICK_A_DATE = "Need to pick a date.";
    public static final String NEED_TO_SELECT_A_PRIVACY = "Need to select a privacy.";

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.btAddLocation)
    Button btAddLocation;

    @BindView(R.id.btAddDate)
    Button btAddDate;

    @BindView(R.id.ivPreview)
    ImageView ivTripConverPic;

    @BindView(R.id.etTitle)
    EditText mEtTitle;

    @BindView(R.id.spPrivacy)
    MaterialBetterSpinner mSpPrivacy;

    @BindView(R.id.etDescription)
    EditText mEtDescription;

    @BindView(R.id.rlTravelLog)
    RelativeLayout mRlTravelLog;

    private String mSelectedDate;

    private Uri mImageCaptureUri;

    FirebaseApiHelper mFbApiHelper;
    UserProfile mUserProfile;

    private  DatePickerDialog datePickerDialog;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 132;
    private static final int PICK_IMAGE_ID = 2334;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_travel_log);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //toolbar.setNavigationIcon(R.drawable.ic_back_arrow);

        mFbApiHelper = FirebaseApiHelper.getInstance();

        mFbApiHelper.getCurrentUserProfile(new FirebaseApiHelper.FirebaseRetrieverCallback<UserProfile>() {
            @Override
            public void callback(UserProfile userProfile) {
                mUserProfile = userProfile;
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(AddTravelLogActivity.this,
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.privacy_options));

        mSpPrivacy.setAdapter(adapter);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(
                this, AddTravelLogActivity.this, year, month, day);

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_trip_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //When home is clicked
            case R.id.action_close: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";

    @OnClick(R.id.btAddDate)
    public void addDate() {
        datePickerDialog.show();
    }
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;


    @OnClick(R.id.btAddLocation)
    public void onAddLocationFocus() {
        Intent intent = new Intent(this, PlacesAutoCompleteActivity.class);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @OnClick(R.id.ivPreview)
    public void onAddPhotoClick() {
        Intent chooseImageIntent = ImagePickerUtil.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @OnClick(R.id.btAddTrip)
    public void onSubmit(){
        TravelLog travelLog = new TravelLog();
        Snackbar snackbar;

        if(mEtTitle.getText()!=null && !mEtTitle.getText().toString().equals("")) {
            travelLog.title = mEtTitle.getText().toString();
        }
        else {
           snackbar = Snackbar.make(mRlTravelLog, NEED_TO_ADD_TITLE, Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        if(btAddLocation.getText() !=null && !btAddLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.add_location))){
            travelLog.destination = btAddLocation.getText().toString();
        }
        else{
            snackbar = Snackbar.make(mRlTravelLog, NEED_TO_ADD_LOCATION, Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        if(mSelectedDate!= null && !mSelectedDate.trim().equals("")){
            travelLog.date = mSelectedDate;
        }
        else{
            snackbar = Snackbar.make(mRlTravelLog, NEED_TO_PICK_A_DATE, Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        travelLog.dateCreated = DateTimeUtils.getUTCMillisAsString();

        if(mSpPrivacy.getText() !=null && !mSpPrivacy.getText().toString().trim().equals("")
                && !mSpPrivacy.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_privacy_prompt))){
            travelLog.privacy = PrivacyEnum.valueOf(mSpPrivacy.getText().toString().toUpperCase());
        }
        else{
            snackbar = Snackbar.make(mRlTravelLog, NEED_TO_SELECT_A_PRIVACY, Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        travelLog.notify = false;

        travelLog.userId = mFbApiHelper.getFirebaseUser().getUid();

        travelLog.userProfile = mUserProfile;

        if(mImageCaptureUri != null) {
            travelLog.defaultImageUrl = mImageCaptureUri.toString();
        }

        if(mEtDescription.getText()!=null && !mEtDescription.getText().toString().equals("")) {
            travelLog.description = mEtDescription.getText().toString();
        }
        else {
            snackbar = Snackbar.make(mRlTravelLog, NEED_TO_ADD_DESCRIPTION, Snackbar.LENGTH_SHORT);
            snackbar.show();

            return;
        }

        mFbApiHelper.addTravelLog(travelLog, mImageCaptureUri);

        finish();
        snackbar = Snackbar.make(mRlTravelLog, ADDED_NEW_TRIP, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case PICK_IMAGE_ID:
                if(resultCode == 0) {
                    //do nothing
                } else if(resultCode == -1){
                    if(data != null && data.getData() != null && data.getData().getScheme() != null
                            && data.getData().getAuthority() != null && data.getData().getPath() != null) {
                        mImageCaptureUri = Uri.parse(data.getData().getScheme() + "://" + data.getData().getAuthority() + data.getData().getPath());

                        if(mImageCaptureUri != null) {
                            Picasso.with(getApplicationContext())
                                .load(mImageCaptureUri)
                                .transform(new RoundedCornersTransformation(0, 0))
                                .resize(1100, 850)
                                .placeholder(R.drawable.ic_photo)
                                .error(R.drawable.ic_camera)
                                .into(ivTripConverPic);
                        }
                    }
                }

                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if(data!=null) {
                    TravelLatLong travelLatLong = Parcels.unwrap(data.getParcelableExtra("travelLatLong"));
                    if(travelLatLong!= null && travelLatLong.address != null) {
                        btAddLocation.setText(travelLatLong.address);
                    }
                }
            }
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mSelectedDate = DateTimeUtils.getUTCMillisAsString(year, month, dayOfMonth);
        btAddDate.setText(DateTimeUtils.parseDateTimeFromMillisToDate(mSelectedDate));
    }
}
