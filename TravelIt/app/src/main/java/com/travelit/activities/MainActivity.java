package com.travelit.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.github.clans.fab.FloatingActionMenu;
import com.travelit.R;
import com.travelit.fragments.TravelLogSummaryBaseFragment;
import com.travelit.fragments.TravelLogSummaryFragment;
import com.travelit.fragments.TravelLogSummarySearchFragment;
import com.travelit.models.TravelLatLong;
import com.travelit.models.User;
import com.travelit.models.UserProfile;
import com.travelit.networks.FirebaseApiHelper;
import com.travelit.util.LogUtil;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends BaseActivity implements TravelLogSummaryBaseFragment.OnFragmentInteractionListener {
    private final String TAG = MainActivity.class.getName();
    private static FirebaseApiHelper mFbApiHelper;
    private static String loggedInuserId;
    private User mUser;

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 132;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fam_action_menu)
    FloatingActionMenu fam;

    @OnClick(R.id.fab_add_travel_log_menu_item)
    public void onFabAddTravelLogMenuClicked() {
        Intent intent = new Intent(MainActivity.this, AddTravelLogActivity.class);
        intent.putExtra("user", loggedInuserId);
        startActivity(intent);
    }

    @OnClick(R.id.fab_add_travel_log_entry_menu_item)
    public void onFabAddTravelLogEntryClicked() {
        Intent intent = new Intent(MainActivity.this, AddTravelLogEntryActivity.class);
        intent.putExtra("user", loggedInuserId);
        startActivity(intent);
    }

    UserProfile mUserProfile;

    Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mFbApiHelper = FirebaseApiHelper.getInstance();
        loggedInuserId = mFbApiHelper.getFirebaseUser().getUid();
        loadInitialFragment();
    }

    private void loadInitialFragment() {
        mFbApiHelper.getCurrentUserProfile(new FirebaseApiHelper.FirebaseRetrieverCallback<UserProfile>() {
            @Override
            public void callback(UserProfile userProfile) {

                mUserProfile = userProfile;

                try {
                    // Replace the contents of the container with the new fragment
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.flPlaceholder, TravelLogSummaryFragment.newInstance(userProfile));
                    ft.commit();
                }
                catch (Exception e){
                    LogUtil.logE("MainActivity", e.getMessage(), e);
                }
            }
        });

        /*mFbApiHelper.getCurrentUser(new FirebaseApiHelper.FirebaseRetrieverCallback<User>() {
            @Override
            public void callback(User user) {

                mUserProfile = user.getCurrentUserProfile();
                // Replace the contents of the container with the new fragment
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flPlaceholder, TravelLogSummaryFragment.newInstance(mUserProfile));
                ft.commit();
            }
        });*/
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu.findItem(R.id.action_clear_search).setVisible(false);

        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home: {
                getFragmentManager().popBackStack();
                return true;
            }
            case R.id.action_search:{
                Intent intent = new Intent(this, PlacesAutoCompleteActivity.class);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                return true;
            }
            case R.id.action_clear_search:{
                MenuItem menuItem = mMenu.findItem(R.id.action_clear_search);
                menuItem.setVisible(false);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                // Replace the contents of the container with the new fragment
                ft.replace(R.id.flPlaceholder, TravelLogSummaryFragment.newInstance(mUserProfile));
                // Complete the changes added above
                ft.commit();
                return true;
            }
            case R.id.action_logout:{
                mFbApiHelper.signOut();
                Intent intent = new Intent(MainActivity.this, LogInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return true;
            }
            case R.id.action_profile: {
                /*
                mFbApiHelper.getCurrentUserProfile(loggedInuserId, new FirebaseApiHelper.FirebaseRetrieverCallback<UserProfile>() {
                    @Override
                    public void callback(UserProfile userProfile) {
                        mUser = user;

                        //mUser.getCurrentUserProfile().setUserId(loggedInuserId);
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        intent.putExtra("user", Parcels.wrap(user));
                        startActivity(intent);
                    }
                });
                */

                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra("userProfile", Parcels.wrap(mUserProfile));
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if(data!=null) {
                    MenuItem menuItem = mMenu.findItem(R.id.action_clear_search);
                    menuItem.setVisible(true);

                    TravelLatLong travelLatLong = Parcels.unwrap(data.getParcelableExtra("travelLatLong"));

                    if(travelLatLong!= null && travelLatLong.name != null) {
                        // Begin the transaction
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.flPlaceholder, TravelLogSummarySearchFragment.newInstance(mUserProfile, travelLatLong.name));
                        // Complete the changes added above
                        ft.commit();
                    }


                    Log.d(TAG, "Search Nav Clicked");
                }
                break;
            }
            default:
                return;
        }
    }
}
