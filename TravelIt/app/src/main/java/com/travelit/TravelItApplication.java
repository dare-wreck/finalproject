package com.travelit;

import android.app.Application;
import android.content.Context;

import com.travelit.networks.GoogleApiClient;
import com.travelit.networks.GoogleApiHelper;
import com.travelit.views.CustomViewWithTypefaceSupport;
import com.travelit.views.TextField;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * This is the Android application itself and is used to configure various settings
 * including the image cache in memory and on disk. This also adds a singleton
 * for accessing the relevant rest client.
 *
 * @author Sai Muppa
 */

public class TravelItApplication extends Application {

    private GoogleApiHelper googleApiHelper;
    private static TravelItApplication mInstance;
    public static Context mContext;
    private static GoogleApiClient googleApiClient;

    public TravelItApplication() {
        googleApiClient = new GoogleApiClient();
    }

    public static GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public GoogleApiHelper getGoogleApiHelperInstance() {
        return this.googleApiHelper;
    }

    public static GoogleApiHelper getGoogleApiHelper() {
        return mInstance.getGoogleApiHelperInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-ThinItalic.ttf")
                .setFontAttrId(R.attr.fontPath)
                .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport.class)
                .addCustomStyle(TextField.class, R.attr.textFieldStyle)
                .build()
        );
        TravelItApplication.mContext = this;
        googleApiHelper = new GoogleApiHelper(TravelItApplication.mContext);

    }
}
