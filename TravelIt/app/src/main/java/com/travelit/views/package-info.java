/**
 * This package contains custom views of the travel-it project.
 * @author Sai Muppa
 *
 */
package com.travelit.views;