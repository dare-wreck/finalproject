package com.travelit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

import com.travelit.R;

/**
 * Created by chris on 17/03/15.
 * For Calligraphy.
 */
public class TextField extends AppCompatTextView {

    public TextField(final Context context, final AttributeSet attrs) {
        super(context, attrs, R.attr.textFieldStyle);
    }

}