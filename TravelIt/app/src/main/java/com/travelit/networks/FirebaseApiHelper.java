package com.travelit.networks;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.travelit.models.Media;
import com.travelit.models.TravelLog;
import com.travelit.models.TravelLogEntry;
import com.travelit.models.TravelLogEntryAssociation;
import com.travelit.models.User;
import com.travelit.models.UserProfile;
import com.travelit.models.dto.TravelLogDto;
import com.travelit.util.LogUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class FirebaseApiHelper {
    private static final String TAG = "FirebaseApiHelper";
    private static final String PATH_IMAGES = "images";

    public static String PATH_USERS = "users";
    public static String PATH_TRAVEL_LOGS = "travelLogs";
    public static String PATH_TRAVEL_ENTRIES = "travelLogEntries";
    public static String PATH_USER_PROFILE = "userProfile";
    public static String PATH_SELECTED_TRAVEL_ENTRIES = "selectedTravelEntries";
    public static String PATH_DEFAULT_IMAGE_URL = "defaultImageUrl";

    private static FirebaseAuth mFirebaseAuth;
    private static FirebaseUser mFirebaseUser;
    private static DatabaseReference mDatabase;

    private static FirebaseApiHelper mInstance;

    public static FirebaseApiHelper getInstance(){
        return new FirebaseApiHelper();
    }

    private FirebaseApiHelper(){
        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mInstance = this;
    }

    @NonNull
    private TravelLog getTravelLogFromDTO(String travelLogId, TravelLogDto dto, UserProfile userProfile) {
        TravelLog convertedTravelLog;
        convertedTravelLog = new TravelLog();
        convertedTravelLog.id = travelLogId;
        convertedTravelLog.title = dto.getTitle();
        convertedTravelLog.description = dto.getDescription();
        convertedTravelLog.destination = dto.getDestination();
        convertedTravelLog.commentsCount = dto.getCommentsCount();
        convertedTravelLog.favoritesCount = dto.getFavoritesCount();
        convertedTravelLog.defaultImageUrl = dto.getDefaultImageUrl();
        convertedTravelLog.userId = dto.userId;
        convertedTravelLog.userProfile = userProfile;
        convertedTravelLog.favorited = (dto.favorites != null && dto.favorites.containsKey(mFirebaseUser.getUid()));
        convertedTravelLog.travelLogEntries = new ArrayList<TravelLogEntry>();
        convertedTravelLog.isComplete = dto.isComplete;
        convertedTravelLog.date = dto.date;
        if(dto != null && dto.getTravelLogEntries()!= null){
            for (Map.Entry<String, TravelLogEntry> dtoEntry: dto.getTravelLogEntries().entrySet()) {
                TravelLogEntry entry = dtoEntry.getValue();
                entry.id = dtoEntry.getKey();
                convertedTravelLog.travelLogEntries.add(entry);
            }
        }
        return convertedTravelLog;
    }

    public FirebaseAuth getFireBaseAuth(){
        return mFirebaseAuth;
    }

    /**
     * get's the database reference
     * @return
     */
    public DatabaseReference getFireBaseDatabase(){
        return mDatabase;
    }

    /**
     * get's the currently logged in user
     * @return
     */
    public FirebaseUser getFirebaseUser(){
        return mFirebaseUser;
    }

    /**
     * signsout the User
     */
    public void signOut(){
        mFirebaseAuth.signOut();
    }

    /**
     * get's the currently logged in User
     * @param finishedCallback
     */
    public void getCurrentUserProfile(@NonNull FirebaseRetrieverCallback<UserProfile> finishedCallback){
        mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).child(PATH_USER_PROFILE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {;
                UserProfile userProfile = dataSnapshot.getValue(UserProfile.class);

                mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).child(PATH_USER_PROFILE).removeEventListener(this);
                finishedCallback.callback(userProfile);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finishedCallback.callback(null);
            }
        });
    }

    /************************
        Mocked out API
     */
    public void getUser(String userId, @NonNull FirebaseRetrieverCallback<User> finishedCallback){
        mDatabase.child(PATH_USERS).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {;
                User user = dataSnapshot.getValue(User.class);
                mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).removeEventListener(this);
                finishedCallback.callback(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).removeEventListener(this);
                finishedCallback.callback(null);
            }
        });
    }

    /**
     * Get's the user object for the currect user.
     * @param finishedCallback
     */
    public void getCurrentUser(@NonNull FirebaseRetrieverCallback<User> finishedCallback) {
        mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).removeEventListener(this);
                finishedCallback.callback(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).removeEventListener(this);
                finishedCallback.callback(null);
            }
        });
    }

    /**
     * Gets all users in the system with their UUIDs are Keys and corresponding UserProfile objects objects as Values
     * @param finishedCallback
     */
    public void getAllUserProfiles(@NonNull FirebaseRetrieverCallback<Map<String, UserProfile>> finishedCallback){
        mDatabase.child(PATH_USERS).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                Map<String, UserProfile> userProfileMap = new HashMap<String, UserProfile>();
                for (DataSnapshot data: children) {

                    User user  = data.getValue(User.class);

                    if(user != null && user.userProfile != null) {
                        UserProfile userProfile = user.userProfile;
                        userProfile.id = data.getKey();
                        userProfileMap.put(data.getKey(), userProfile);
                    }
                }

                mDatabase.child(PATH_USERS).removeEventListener(this);
                finishedCallback.callback(userProfileMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * For a giver userId, gets all the user's Travel Logs
     * @param userId
     * @param finishedCallback
     */
    public void getTravelLogsByUser(String userId, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getUser(userId, new FirebaseRetrieverCallback<User>() {
            @Override
            public void callback(User user) {
                getTravelLogsByUser(userId, user.userProfile, finishedCallback);
            }
        });
    }

    private void getTravelLogsByUser(String userId, UserProfile userProfile, @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {
        Query query = mDatabase.child(PATH_TRAVEL_LOGS).orderByChild("userId").equalTo(userId);
        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                for (DataSnapshot data: children) {
                    try {
                        TravelLogDto dto = data.getValue(TravelLogDto.class);

                        if(userProfile != null) {
                            TravelLog convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);
                            travelLogs.add(convertedTravelLog);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                query.removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.d(TAG, "Error in database call to get UserById");
            }
        });
    }

    /**
     * Get's all travel logs except for the logs that are related to the passed in user
     * @param userId
     * @param finishedCallback
     */
    public void getAllTravelLogsExcludingUser(String userId, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getAllUserProfiles(new FirebaseRetrieverCallback<Map<String, UserProfile>>() {
            @Override
            public void callback(Map<String, UserProfile> userProfileMap) {
                getAllTravelLogsExcludingUser(userId, userProfileMap, finishedCallback);
            }
        });
    }

    private void getAllTravelLogsExcludingUser(final String userId, Map<String, UserProfile> userProfileMap, @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {
        Query query = mDatabase.child(PATH_TRAVEL_LOGS);
        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                TravelLog convertedTravelLog;
                for (DataSnapshot data: children) {
                    try{
                        TravelLogDto dto = data.getValue(TravelLogDto.class);

                        UserProfile userProfile = null;
                        if(dto != null && dto.userId != null && !dto.getUserId().equals(userId) && userProfileMap.containsKey(dto.userId)) {
                            userProfile = userProfileMap.get(dto.userId);
                            convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);

                            travelLogs.add(convertedTravelLog);
                            LogUtil.logD(TAG, convertedTravelLog.toString());
                        }
                    } catch(Exception e){
                        Log.d(TAG, "Incorrect data format in the database for travel log entry: " + e);
                    }

                }

                query.removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.d(TAG, "Error in database call to get UserById");
            }
        });
    }

    /**
     * Get's all travel logs except for the logs that are related to the current user
     * @param finishedCallback
     */
    public void getAllTravelLogsExcludingCurrentUser(@NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getAllUserProfiles(new FirebaseRetrieverCallback<Map<String, UserProfile>>() {
            @Override
            public void callback(Map<String, UserProfile> userProfileMap) {
                getAllTravelLogsExcludingUser((String) getFirebaseUser().getUid(), userProfileMap, finishedCallback);
            }
        });
    }

    /**
     * get's travel log based on the travel log id
     * @param travelLogId
     * @param finishedCallback
     */
    public void getTravelLogByTravelLogId(String travelLogId, @NonNull FirebaseRetrieverCallback<TravelLog> finishedCallback) {
        mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TravelLogDto dto = dataSnapshot.getValue(TravelLogDto.class);

                if(dto!= null && dto.userId != null){
                    getUser(dto.userId, new FirebaseRetrieverCallback<User>() {
                        @Override
                        public void callback(User user) {
                            if(user != null && user.userProfile != null){
                                TravelLog convertedTravelLog = getTravelLogFromDTO(travelLogId, dto, user.userProfile);
                                finishedCallback.callback(convertedTravelLog);
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finishedCallback.callback(null);
            }
        });
    }

    /***
     * Get's all the travel logs
     * @param finishedCallback
     */
    public void getAllTravelLogs(@NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getAllUserProfiles(new FirebaseRetrieverCallback<Map<String, UserProfile>>() {
            @Override
            public void callback(Map<String, UserProfile> userProfileMap) {
                getAllTravelLogs(userProfileMap, finishedCallback);
            }
        });
    }

    private void getAllTravelLogs(Map<String, UserProfile> userProfileMap, @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {
        mDatabase.child(PATH_TRAVEL_LOGS).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                for (DataSnapshot data: children) {

                    TravelLogDto dto  = data.getValue(TravelLogDto.class);

                    UserProfile userProfile = null;
                    if(dto != null && dto.userId != null && userProfileMap.containsKey(dto.userId)) {
                        userProfile = userProfileMap.get(dto.userId);
                        TravelLog convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);

                        travelLogs.add(convertedTravelLog);
                        LogUtil.logD(TAG, convertedTravelLog.toString());
                    }
                }

                mDatabase.child(PATH_TRAVEL_LOGS).removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Get's travel log entries of a travel log
     * @param travelLogId
     * @param finishedCallback
     */
    public void getTravelLogEntriesByTravelLog(String travelLogId, @NonNull FirebaseRetrieverCallback<List<TravelLogEntry>> finishedCallback){
        mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLogEntry> travelLogEntries = new ArrayList<>();
                for (DataSnapshot data: children) {

                    TravelLogEntry travelLogEntry = data.getValue(TravelLogEntry.class);
                    travelLogEntry.setId(data.getKey());
                    travelLogEntries.add(travelLogEntry);
                    LogUtil.logD(TAG, travelLogEntry.toString());
                }

                mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).removeEventListener(this);
                finishedCallback.callback(travelLogEntries);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finishedCallback.callback(null);
            }
        });
    }

    /**
     * Adds a travel log
     * @param travelLog
     */
    public void addTravelLog(TravelLog travelLog, Uri defaultImageUri){
        if(travelLog != null) {
            final String key = mDatabase.child(PATH_TRAVEL_LOGS).push().getKey();

            Map<String, Object> map = new HashMap<>();

            map.put(key, true);

            Map<String, Object> childUpdates = new HashMap<>();

            childUpdates.put("/" + PATH_TRAVEL_LOGS + "/" + key, travelLog.toMap());

            childUpdates.put("/" + PATH_USERS + "/" + mFirebaseUser.getUid() + "/" + "travelLogs" + "/" + key, true);

            mDatabase.updateChildren(childUpdates, (databaseError, databaseReference) -> {
                if (travelLog != null) {
                    if(travelLog.getTravelLogEntries() != null) {
                        for (TravelLogEntry entry : travelLog.getTravelLogEntries()) {
                            mDatabase.child(PATH_TRAVEL_LOGS).child(key).child(PATH_TRAVEL_ENTRIES).push().setValue(entry);
                        }
                    }

                    if (databaseError == null) {
                        if(defaultImageUri != null) {
                            try {

                                String imageUid = mDatabase.push().getKey() + "-" + defaultImageUri.getLastPathSegment();

                                StorageReference storageReference =
                                        FirebaseStorage.getInstance()
                                                .getReference(mFirebaseUser.getUid())
                                                .child(key)
                                                .child(imageUid);



                                putTravelLogDefaultImageInStorage(storageReference, defaultImageUri, key);
                            }
                            catch (Exception e){
                                LogUtil.logE(TAG, "Unable to write image to database.");
                            }
                        }
                    } else {
                        LogUtil.logE(TAG, "Unable to write message to database.");
                    }
                }
            });
        }
    }

    private void putTravelLogDefaultImageInStorage(StorageReference storageReference, final Uri uri, final String key) {
        storageReference.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    @SuppressWarnings("VisibleForTests")String downloadUrl = task.getResult().getMetadata().getDownloadUrl()
                            .toString();
                    mDatabase.child(PATH_TRAVEL_LOGS).child(key).child(PATH_DEFAULT_IMAGE_URL).setValue(downloadUrl);
                } else {
                    LogUtil.logE(TAG, "Image upload task was not successful.");
                }
            }
        });
    }

    /**
     * Adds a travel log
     * returns back newly created TravelLog Id
     * @param travelLog
     */
    public void addTravelLogWithAnEntry(TravelLog travelLog, @NonNull FirebaseRetrieverCallback<TravelLogEntryAssociation> finisheCallBack){
        if(travelLog != null) {
            final String newTravelLogId = mDatabase.child(PATH_TRAVEL_LOGS).push().getKey();

            Map<String, Object> map = new HashMap<>();

            map.put(newTravelLogId, true);

            Map<String, Object> childUpdates = new HashMap<>();

            childUpdates.put("/" + PATH_TRAVEL_LOGS + "/" + newTravelLogId, travelLog.toMap());

            childUpdates.put("/" + PATH_USERS + "/" + mFirebaseUser.getUid() + "/" + "travelLogs" + "/" + newTravelLogId, true);

            mDatabase.updateChildren(childUpdates, (databaseError, databaseReference) -> {
                if(databaseError == null && travelLog != null && travelLog.getTravelLogEntries() != null) {
                    DatabaseReference dbReference = mDatabase.child(PATH_TRAVEL_LOGS).child(newTravelLogId).child(PATH_TRAVEL_ENTRIES).push();
                    String travelLogEntryId = dbReference.getKey();
                    mDatabase.child(PATH_TRAVEL_LOGS).child(newTravelLogId).child(PATH_TRAVEL_ENTRIES).child(travelLogEntryId).setValue(travelLog.getTravelLogEntries().get(0), new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            //TODO: update the storage;

                            TravelLogEntryAssociation travelLogEntryAssociation = new TravelLogEntryAssociation();
                            travelLogEntryAssociation.travelLogId = newTravelLogId;
                            travelLogEntryAssociation.travelEntryId = travelLogEntryId;

                            finisheCallBack.callback(travelLogEntryAssociation);
                        }
                    });
                }
                else {
                    finisheCallBack.callback(null);
                }
            });
        }
    }

    /**
     * Adds a travel log entry to a travel log
     * @param travelLogId
     */
    public void addTravelLogEntry(String travelLogId, TravelLogEntry travelLogEntry, @NonNull FirebaseRetrieverCallback<String> finishedCallback){
        if(travelLogId != null && !travelLogId.trim().equals("") && travelLogEntry != null) {

            Map<String, Object> map = new HashMap<>();

            map = travelLogEntry.toMap();
            mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).push().setValue(map, (databaseError, databaseReference) -> {
                if(databaseError == null) {
                    finishedCallback.callback("Trip entry added!");
                }
                else{
                    finishedCallback.callback("Failed to add entry.");
                }
            });
        }
    }

    /**
     * Adds a travel log entry to a travel log
     * @param travelLogId
     * @param travelLogEntry
     * @param imageUri
     * @param finishedCallback
     */
    public void addTravelLogEntry(String travelLogId, TravelLogEntry travelLogEntry, Uri imageUri, @NonNull FirebaseRetrieverCallback<String> finishedCallback){
        if(travelLogId != null && !travelLogId.trim().equals("") && travelLogEntry != null) {

            Map<String, Object> map = new HashMap<>();

            map = travelLogEntry.toMap();

            DatabaseReference databaseReference = mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).push();
            String travelLogEntryKey  = databaseReference.getKey();

            databaseReference.setValue(map, (databaseError, dbReference) -> {
                if(databaseError == null) {
                    try {
                        StorageReference storageReference = null;
                        if (imageUri != null) {

                            String imageUid = mDatabase.push().getKey() + "-" + imageUri.getLastPathSegment();

                            storageReference =
                                    FirebaseStorage.getInstance()
                                            .getReference(mFirebaseUser.getUid())
                                            .child(travelLogId)
                                            .child(travelLogEntryKey)
                                            .child(imageUid);
                            putTravelLogEntryImageInStorage(storageReference, imageUri, travelLogId, travelLogEntryKey);
                        }
                        finishedCallback.callback("Trip entry added!");
                    }
                    catch (Exception e){
                        finishedCallback.callback("Failed to add entry.");
                        LogUtil.logE(TAG, "Unable to write message to database.", e);
                    }
                }
                else{
                    LogUtil.logE(TAG, "Unable to write message to database.", databaseError.toException());
                    finishedCallback.callback("Failed to add entry.");
                }
            });
        }
    }

    private void putTravelLogEntryImageInStorage(StorageReference storageReference, Uri imageUri,
                                                 String travelLogId, String travelLogEntryKey) {
        storageReference.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    @SuppressWarnings("VisibleForTests")String downloadUrl = task.getResult().getMetadata().getDownloadUrl()
                            .toString();

                    Map<String, Object> map = new HashMap<>();
                    map.put("url", downloadUrl);
                    mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES)
                            .child(travelLogEntryKey).child(PATH_IMAGES).child("0").setValue(map);
                } else {
                    LogUtil.logE(TAG, "Image upload task was not successful.");
                }
            }
        });
    }

    /**
     * Deletes a travel log
     */
    public void deleteTravelLog(){

    }

    /**
     * Adds a travel log
     */
    public void deleteTravelLogEntry(){

    }

    /**
     * Updates a travel log when a new image is added to TarvelLogEntry, this is a Hack due to the database structure will have to
     * revisit the database structure to make Media a Map<String, Object> rather than a list <Objects>'s
     * @param travelLogId
     * @param travelLogEntryId
     * @param imageUri
     */
    public void updateTravelLogEntryWithImageAddition(String travelLogId, String travelLogEntryId, Uri imageUri){
        if(travelLogId != null && travelLogEntryId != null && imageUri != null) {
            try {
                StorageReference storageReference =
                        FirebaseStorage.getInstance()
                                .getReference(mFirebaseUser.getUid())
                                .child(travelLogId)
                                .child(travelLogEntryId)
                                .child(imageUri.getLastPathSegment());

                storageReference.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            @SuppressWarnings("VisibleForTests") String downloadUrl = task.getResult().getMetadata().getDownloadUrl()
                                    .toString();

                            // [START post travel log entry transaction]
                            mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).child(travelLogEntryId)
                                    .runTransaction(new Transaction.Handler() {
                                        @Override
                                        public Transaction.Result doTransaction(MutableData mutableData) {
                                            TravelLogEntry travelLogEntry = mutableData.getValue(TravelLogEntry.class);
                                            if (travelLogEntry == null) {
                                                return Transaction.success(mutableData);
                                            }

                                            if (travelLogEntry != null) {
                                                Media media = new Media(null, downloadUrl, null, null);
                                                if (travelLogEntry.images == null) {
                                                    travelLogEntry.images = new ArrayList<Media>();
                                                }

                                                travelLogEntry.images.add(media);
                                            }

                                            // Set value and report transaction success
                                            mutableData.setValue(travelLogEntry);
                                            return Transaction.success(mutableData);
                                        }

                                        @Override
                                        public void onComplete(DatabaseError databaseError, boolean b,
                                                               DataSnapshot dataSnapshot) {
                                            if (databaseError == null) {
                                                // Transaction completed
                                                Log.d(TAG, "postTransaction:onComplete: Travel log entry updated with image.");
                                            } else {
                                                // Transaction completed
                                                Log.d(TAG, "postTransaction:onComplete:" + databaseError);

                                            }
                                        }
                                    });
                            // [END post favorites transaction]
                        } else {
                            LogUtil.logE(TAG, "Image upload task was not successful.", task.getException());
                        }
                    }
                });
            }
            catch (Exception e){
                LogUtil.logE(TAG, "Image upload task was not successful.");
            }
        }
    }

    /**
     * updates an user profile
     */
    public void updateUserProfile(){

    }

    /**
     * Follow an user
     */
    public void userFollow(){

    }

    /**
     * Unfollow an user
     */
    public void userUnFollow(){

    }

    /**
     * Searches destinations that start with queryText
     * @param queryText
     * @param finishedCallback
     */
    public void getTravelLogsByLexicalSearch(String queryText, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getAllUserProfiles(new FirebaseRetrieverCallback<Map<String, UserProfile>>() {
            @Override
            public void callback(Map<String, UserProfile> userProfileMap) {
                getTravelLogsByLexicalSearch(queryText, userProfileMap, finishedCallback);
            }
        });
    }

    private void getTravelLogsByLexicalSearch(String queryText, Map<String, UserProfile> userProfileMap, @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {
        Query query = mDatabase.child(PATH_TRAVEL_LOGS).orderByChild("destination").startAt(queryText)
                .endAt(queryText + "\uf8ff");

        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                for (DataSnapshot data: children) {
                    TravelLogDto dto  = data.getValue(TravelLogDto.class);

                    UserProfile userProfile = null;
                    if(dto != null && dto.userId != null && userProfileMap.containsKey(dto.userId)) {
                        userProfile = userProfileMap.get(dto.userId);
                        TravelLog convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);

                        travelLogs.add(convertedTravelLog);
                        LogUtil.logD(TAG, convertedTravelLog.toString());
                    }
                }

                query.removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    /**
     * Searches all destinations that start with queryText that doesn't belong to passed in user
     * @param userId
     * @param queryText
     * @param finishedCallback
     */
    public void getTravelLogsByLexicalSearchExcludingAnUser(String userId, String queryText, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getAllUserProfiles(new FirebaseRetrieverCallback<Map<String, UserProfile>>() {
            @Override
            public void callback(Map<String, UserProfile> userProfileMap) {
                getTravelLogsByLexicalSearchExcludingAnUser(userId, queryText, userProfileMap, finishedCallback);
            }
        });
    }

    private void getTravelLogsByLexicalSearchExcludingAnUser(final String userId, String queryText, Map<String, UserProfile> userProfileMap,
                                         @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {

        Query query = mDatabase.child(PATH_TRAVEL_LOGS).orderByChild("destination").startAt(queryText).endAt(queryText+"\uf8ff");

        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                TravelLog convertedTravelLog;
                for (DataSnapshot data: children) {
                    TravelLogDto dto  = data.getValue(TravelLogDto.class);

                    UserProfile userProfile = null;
                    if(dto != null && dto.userId != null && !dto.userId.equals(userId) && userProfileMap.containsKey(dto.userId)) {
                        userProfile = userProfileMap.get(dto.userId);
                        convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);

                        travelLogs.add(convertedTravelLog);
                        LogUtil.logD(TAG, convertedTravelLog.toString());
                    }
                }

                query.removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    /**
     * Searches all destinations that start with queryText that belongs to passed in user
     * @param queryText
     * @param finishedCallback
     */
    public void getTravelLogsByLexicalSearchForCurrentUser(String queryText, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){

        getCurrentUserProfile(new FirebaseRetrieverCallback<UserProfile>() {
            @Override
            public void callback(UserProfile userProfile) {
                getTravelLogsByLexicalSearchForCurrentUser(queryText, userProfile, finishedCallback);
            }
        });
    }

    private void getTravelLogsByLexicalSearchForCurrentUser(String queryText, UserProfile userProfile, @NonNull final FirebaseRetrieverCallback<List<TravelLog>> finishedCallback) {
        Query query = mDatabase.child(PATH_TRAVEL_LOGS).orderByChild("destination").startAt(queryText)
                .endAt(queryText+"\uf8ff");

        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                List<TravelLog> travelLogs = new ArrayList<>();
                TravelLog convertedTravelLog;
                for (DataSnapshot data: children) {
                    TravelLogDto dto  = data.getValue(TravelLogDto.class);

                    UserProfile userProfile = null;
                    if(dto != null && dto.getUserId() != null && dto.getUserId().equals(mFirebaseUser.getUid()) && userProfile != null) {
                        convertedTravelLog = getTravelLogFromDTO(data.getKey(), dto, userProfile);

                        travelLogs.add(convertedTravelLog);
                        LogUtil.logD(TAG, convertedTravelLog.toString());
                    }
                }

                query.removeEventListener(this);
                finishedCallback.callback(travelLogs);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    /**
     * Searches all destinations that start with queryText that doesn't belong to logged (current) in user
     * @param queryText
     * @param finishedCallback
     */
    public void getTravelLogsByLexicalSearchExcludingCurrentUser(String queryText, @NonNull FirebaseRetrieverCallback<List<TravelLog>> finishedCallback){
        getTravelLogsByLexicalSearchExcludingAnUser(getFirebaseUser().getUid(), queryText, finishedCallback);
    }

    /**
     * Update the favorites of a travel log
     * @param travelLog
     */
    public void updateFavorite(final TravelLog travelLog, @NonNull FirebaseRetrieverCallback<TravelLog> finishedCallback){
        if(travelLog != null && travelLog.getId() != null) {
            // [START post favorites transaction]
            mDatabase.child(PATH_TRAVEL_LOGS).child(travelLog.getId())
                    .runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {
                            TravelLogDto travelLog = mutableData.getValue(TravelLogDto.class);
                            if (travelLog == null) {
                                return Transaction.success(mutableData);
                            }

                            if (travelLog.favorites != null && travelLog.favorites.containsKey(mFirebaseUser.getUid())) {
                                // unfavorite the post and remove self from favorites
                                travelLog.favoritesCount = travelLog.favoritesCount - 1;
                                travelLog.favorites.remove(mFirebaseUser.getUid());
                            } else {
                                // favorite the post and add self to favorites
                                travelLog.favoritesCount = travelLog.favoritesCount + 1;
                                travelLog.favorites = new HashMap<String, Boolean>();
                                travelLog.favorites.put(mFirebaseUser.getUid(), true);
                            }

                            // Set value and report transaction success
                            mutableData.setValue(travelLog);
                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(DatabaseError databaseError, boolean b,
                                               DataSnapshot dataSnapshot) {
                            if (databaseError == null) {

                                TravelLogDto dto = dataSnapshot.getValue(TravelLogDto.class);
                                TravelLog convertedTravelLog = getTravelLogFromDTO(dataSnapshot.getKey(), dto, travelLog.userProfile);

                                finishedCallback.callback(convertedTravelLog);
                            } else {
                                // Transaction completed
                                Log.d(TAG, "postTransaction:onComplete:" + databaseError);

                                finishedCallback.callback(travelLog);
                            }
                        }
                    });
            // [END post favorites transaction]
        }
        else{
            finishedCallback.callback(travelLog);
        }
    }

    /**
     *
     * @param parentTravelLogEntryId
     * @param travelLogId
     * @param travelLogEntry
     */
    public void associateTravelLogEntry(String parentTravelLogEntryId, String travelLogId, TravelLogEntry travelLogEntry, @NonNull FirebaseRetrieverCallback<Boolean> finishedCallback) {
        if(parentTravelLogEntryId != null && travelLogId != null && travelLogEntry != null) {
            final String key = mDatabase.child(PATH_TRAVEL_LOGS).child(travelLogId).child(PATH_TRAVEL_ENTRIES).push().getKey();

            TravelLogEntryAssociation travelLogEntryAssociation = new TravelLogEntryAssociation();
            travelLogEntryAssociation.travelEntryId = key;
            travelLogEntryAssociation.travelLogId = travelLogId;

            Map<String, Object> childUpdates = new HashMap<>();

            childUpdates.put("/" + PATH_TRAVEL_LOGS + "/" + travelLogId + "/" + PATH_TRAVEL_ENTRIES + "/" + key, travelLogEntry.toMap());

            childUpdates.put("/" + PATH_USERS + "/" + mFirebaseUser.getUid() + "/" + PATH_SELECTED_TRAVEL_ENTRIES + "/" + parentTravelLogEntryId, travelLogEntryAssociation);

            mDatabase.updateChildren(childUpdates, (databaseError, databaseReference) -> {
                if(databaseError == null)
                {
                    finishedCallback.callback(true);
                }
                else{
                    finishedCallback.callback(false);
                    LogUtil.logE(TAG, databaseError.getMessage(), databaseError.toException());
                }
            });
        }
    }

    public void associateTravelLogEntry(final String parentTravelLogEntryId, final TravelLogEntryAssociation travelLogEntryAssociation, FirebaseRetrieverCallback<Boolean> finishedCallback) {
        mDatabase.child(PATH_USERS).child(mFirebaseUser.getUid()).child(PATH_SELECTED_TRAVEL_ENTRIES).child(parentTravelLogEntryId).setValue(travelLogEntryAssociation, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError == null)
                {
                    finishedCallback.callback(true);
                }
                else{
                    finishedCallback.callback(false);
                    LogUtil.logE(TAG, databaseError.getMessage(), databaseError.toException());
                }
            }
        });
    }

    /**
     * Callback interface for firebase result set
     * @param <T>
     */
    public interface FirebaseRetrieverCallback<T>{
        void callback(T data);
    }
}
