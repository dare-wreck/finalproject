package com.travelit.networks;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;

/**
 * Created by darewreck_PC on 5/4/2017.
 */

public class GoogleApiRequestInterceptor implements Interceptor {
    public static final String API_KEY = "AIzaSyAJFO8tjs4391GzgU7YeOXdWLSKXjJq_jM";

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder().addHeader("api-key", API_KEY).build();
        return chain.proceed(newRequest);
    }
}