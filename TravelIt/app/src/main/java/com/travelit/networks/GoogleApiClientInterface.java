package com.travelit.networks;

import com.travelit.models.GooglePlace;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleApiClientInterface {
    @GET("maps/api/place/details/json")
    Call<GooglePlace> getReviews(@Query("key") String key, @Query("placeid") String placeId);
}
