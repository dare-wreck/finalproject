package com.travelit.networks;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by darewreck_PC on 5/4/2017.
 */

public class GoogleApiClient {
    public static final String BASE_URL = "https://maps.googleapis.com/";
    public static final String API_KEY = "AIzaSyAJFO8tjs4391GzgU7YeOXdWLSKXjJq_jM";

    public static String getBaseUrl(){
        return BASE_URL;
    }
    private OkHttpClient client;
    private GoogleApiClientInterface googleApiClientInterface;

    public GoogleApiClient(){
        init();
    }

    public GoogleApiClientInterface getClient(){
        return this.googleApiClientInterface;
    }

    public void init(){

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(new GoogleApiRequestInterceptor());

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        // Can be Level.BASIC, Level.HEADERS, or Level.BODY
        // See http://square.github.io/okhttp/3.x/logging-interceptor/ to see the options.
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.networkInterceptors().add(httpLoggingInterceptor);
        builder.build();

        this.client = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        googleApiClientInterface = retrofit.create(GoogleApiClientInterface.class);

    }
}
